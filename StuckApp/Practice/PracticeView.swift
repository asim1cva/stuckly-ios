//
//  PracticeView.swift
//  StuckApp
//
//  Created by AR Computers on 24/12/2021.
//

import SwiftUI


struct FirstView: View {
    
    @State var showDatePicker: Bool = false
    @State var savedDate: Date? = nil
    
    var body: some View {
        ZStack {
            HStack {
                Text("Selected date: ")
                Button(action: {
                    showDatePicker.toggle()
                }, label: {
                    Text(savedDate?.description ?? "SELECT DATE")
                })
            }
            

            if showDatePicker {
                DatePickerWithButtons(showDatePicker: $showDatePicker, savedDate: $savedDate, selectedDate: savedDate ?? Date())
                    .animation(.linear)
                    .transition(.opacity)
            }
        }
        
    }
}

struct DatePickerWithButtons: View {
    
    @Binding var showDatePicker: Bool
    @Binding var savedDate: Date?
    @State var selectedDate: Date = Date()
    
    var body: some View {
        ZStack {
            
            Color.black.opacity(0.3)
                .edgesIgnoringSafeArea(.all)
            
            
            VStack {
                DatePicker("Test", selection: $selectedDate, displayedComponents: [.date])
                    .datePickerStyle(GraphicalDatePickerStyle())
                
                Divider()
                HStack {
                    
                    Button(action: {
                        showDatePicker = false
                    }, label: {
                        Text("Cancel")
                    })
                    
                    Spacer()
                    
                    Button(action: {
                        savedDate = selectedDate
                        showDatePicker = false
                    }, label: {
                        Text("Save".uppercased())
                            .bold()
                    })
                    
                }
                .padding(.horizontal)

            }
            .padding()
            .background(
                Color.white
                    .cornerRadius(30)
            )

            
        }

    }
}



struct DatePickerWithButtons_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}
