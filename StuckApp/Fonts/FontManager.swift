//
//  FontManager.swift
//  Carrera-Corp-App
//
//  Created by Asim on 9/29/21.
//

import Foundation
import SwiftUI

enum Nunito {
    case light
    case regular
    case black
    case medium
    case semiBold
    case bold
  
    
    func font(size: CGFloat) -> Font {
        switch self {
        case .light:
            return .custom("Nunito-Light", size: size)
        case .regular:
            return .custom("Nunito-Regular", size: size)
        case .black:
            return .custom("Nunito-Black", size: size)
        case .medium:
            return .custom("Nunito-Medium", size: size)
        case .semiBold:
            return .custom("Nunito-SemiBold", size: size)
        case .bold:
            return .custom("Nunito-Bold", size: size)
            
        }
    }
}
