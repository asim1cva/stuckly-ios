//
//  NotificationViewCell.swift
//  StuckApp
//
//  Created by Asim on 2/22/22.
//

import SwiftUI

struct NotificationViewCell: View {
    
    var notificationData: NotificationData
    @State private var detailViewToggle = false
    
    @StateObject var businessViewModel = BusinessTabsViewModel()

    var body: some View {
        
        
        VStack(alignment: .leading, spacing: 0) {
            
            HStack{
                ZStack {
                    NavigationLink(
                        destination: DetailView()
                            .environmentObject(businessViewModel)
                            .navigationBarHidden(false),
                        isActive: $detailViewToggle){ EmptyView()}
                        .hidden()
                    
                    AsyncImageLoader(imageUrl: notificationData.business?.images[0].imageURL ?? "",
                                     imageWidth: 100,
                                     imageHeight: 100,
                                     isRounded: false,
                                     cornerRadius: imageCorner)
                        .cornerRadius(imageCorner)
                }
                
                .frame(width: 100, height: 100)
                
                VStack {
                    HStack {
                        
                        if Global.shared.currentLoginUser.role == UserType.customer {
                            Text(notificationData.business?.name ?? "")
                                .font(!(notificationData.isOpen ?? false) ?  Nunito.bold.font(size: CustomFontsSize.regular) :  Nunito.regular.font(size: CustomFontsSize.regular))
                        } else {
                            Text(notificationData.business?.name ?? "")
                                .font(Nunito.regular.font(size: CustomFontsSize.regular))
                        }
                        
                        Spacer()
                    }
                    
                  
                    HStack {
                        Text(notificationData.text ?? "")
                            .font(Nunito.regular.font(size: CustomFontsSize.small))
                            .foregroundColor(Color.gray)
                        Spacer()


                    }
                    
                    HStack {
                        Spacer()
                        Text(notificationData.createdAt ?? "")
                            .font(Nunito.regular.font(size: CustomFontsSize.exSmall))
                            .foregroundColor(appColor)
                        


                    }
                }
                .padding(.leading,1)
              
                
            }
           
            Divider().padding(.vertical)
            
        }
        .padding(.horizontal)
        .onTapGesture {
            
            if Global.shared.currentLoginUser.role == UserType.customer {
                
                if !(notificationData.isOpen ?? false) {
                    
                    businessViewModel.isOpenApiRequest(notificationId: notificationData.id)
                }
                
                businessViewModel.businessData = self.notificationData.business
                detailViewToggle = true
            }

            
           
            
        }
        
    }
}

