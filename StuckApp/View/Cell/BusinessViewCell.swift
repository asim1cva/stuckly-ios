//
//  BusinessViewCell.swift
//  StuckApp
//
//  Created by Admin Admin on 1/12/22.
//


import SwiftUI

struct BusinessViewCell: View {
    
    var businessData: BusinessListData
    @State private var detailViewToggle = false
    
    @StateObject var businessViewModel = BusinessTabsViewModel()

    var body: some View {
        
        
        VStack {
            
            NavigationLink(
                destination: DetailView()
                    .environmentObject(businessViewModel)
                    .navigationBarHidden(false),
                isActive: $detailViewToggle){ EmptyView()}
                .hidden()
            
            
            ZStack {
                AsyncImageLoader(imageUrl: businessData.images[0].imageURL ?? "",
                                 imageWidth: deviceWidth-40,
                                 imageHeight: 250,
                                 isRounded: false,
                                 cornerRadius: imageCorner)
                    .cornerRadius(imageCorner)
            }
            
            .frame(width: deviceWidth-20, height: 250)
           
            
            
            
            
            
            
            HStack {
                
                Text(businessData.name ?? "")
                    .font(Nunito.bold.font(size: CustomFontsSize.regular))

                
                Spacer()
                
                Text("\(businessData.followersCount ?? 0) Followers")
                    .font(Nunito.regular.font(size: CustomFontsSize.small))
                    .foregroundColor(bgColor)
                
            }
            .padding(.horizontal)
            .padding(.horizontal)
            
            
            Divider().padding(.vertical)
        }
        .onTapGesture {
            businessViewModel.businessData = self.businessData
            detailViewToggle = true
        }
        .frame(width: deviceWidth)
        
        
        
        
    }
}
