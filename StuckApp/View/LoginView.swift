//
//  LoginView.swift
//  StuckApp
//
//  Created by Asim on 16/12/2021.
//

import SwiftUI

struct LoginView: View {
    
    @State var signUpToggle = false
    //    @State var mainViewToggle = false
    @State var isSheetOpen = false
    
    @StateObject var authVM = AuthViewModel()
    
    @EnvironmentObject var customerViewModel : CustomerViewModel

    @EnvironmentObject var businessViewModel : BusinessTabsViewModel

    var userType: UserType
    
    // MARK: body.......
    
    var body: some View {
        
        LoadingView(isShowing: .constant(authVM.loading)) {
            
            VStack {
                
                ScrollView(.vertical, showsIndicators: false, content: {
                    VStack {
                        ZStack {
                            
                            Image("bgImage")
                                .resizable()
                            
                            // MARK: Image....
                            
                            VStack {
                                
                                Image("stuck_Image")
                                    .resizable()
                                    .frame(width: 130, height: 130)
                                Spacer()
                            }
                            .padding(.top, deviceHeight > 700 ? 50 : 40)
                            
                            
                            //MARK: scroll View..............
                            
                            
                            VStack {
                                //MARK: Info text
                                
                                VStack {
                                    
                                    Text(userType == UserType.customer ? "Customer Sign In" : "Business Sign In")
                                        .font(Nunito.bold.font(size: CustomFontsSize.medium))
                                    
                                        .font(.title2)
                                        .fontWeight(.bold)
                                    
                                    Text("Enter your email & password")
                                        .font(Nunito.regular.font(size: CustomFontsSize.regular))
                                    
                                        .foregroundColor(.gray)
                                        .padding(.top, 3)
                                    
                                    Text("to continue")
                                        .font(Nunito.regular.font(size: CustomFontsSize.regular))
                                    
                                        .foregroundColor(.gray)
                                }
                                .padding(.top, deviceHeight > 700 ? 100: 100)
                                
                                
                                
                                //MARK: Email text Field..........
                                
                                
                                
                                
                                VStack(alignment: .leading) {
                                    
                                    
                                    CustomTextFieldView(title: "Email", text: $authVM.email, iconName: "envelope.fill", placeHolder: "Email")
                                    
                                    
                                    //MARK: Password text Field........
                                    
                                    
                                    
                                    CustomTextFieldView(title: "Password", text: $authVM.password, iconName: "lock.fill", placeHolder: "Password")
                                    
                                    
                                    
                                    
                                    
                                    HStack {
                                        
                                        Spacer()
                                        Text("Forgot Password?")
                                            .font(Nunito.regular.font(size: CustomFontsSize.regular))
                                        
                                            .padding(.top, 5)
                                    }
                                    .sheet(isPresented: $isSheetOpen, content: {
                                        ForgetPasswordView()
                                    })
                                    .onTapGesture {
                                        isSheetOpen.toggle()
                                    }
                                    
                                    
                                    
                                    NavigationLink(
                                        destination: BusinessDashBoardView()
                                            .navigationBarHidden(true)
                                            .navigationBarBackButtonHidden(true),
                                        isActive: $authVM.businessDashboardViewToggle){}
                                        .hidden()
                                    
                                    
                                    
                                    NavigationLink(
                                        destination: CustomerBottomTabsView()
                                            .environmentObject(customerViewModel)
                                            .environmentObject(businessViewModel)
                                            .navigationBarHidden(true)
                                            .navigationBarBackButtonHidden(true),
                                        isActive: $authVM.customerDashboardViewToggle){}
                                        .hidden()
                                    
                                    
                                    //MARK: Sign In Button.............
                                    
                                    
                                    
                                    CustomButton(text: "Sign In")
                                        .onTapGesture {
                                            authVM.loginApiRequest(userType: self.userType)
                                        }
                                        .alert(item: $authVM.alertItem) { alertItem in
                                            Alert(title: alertItem.title,
                                                  message: alertItem.message,
                                                  dismissButton: alertItem.dismissButton)
                                        }
                                    
                                }
                                .padding(.horizontal, 30)
                                //.padding(.top, 50)
                                
                                
                                //MARK: New Member text...............
                                
                                
                                
                                
                                HStack {
                                    
                                    
                                    NavigationLink("", destination: SignUpView(userType: self.userType)
//                                                    .navigationBarHidden(true)
                                                    .navigationBarTitleDisplayMode(.inline),
                                                   isActive: $signUpToggle)
                                    

                                    Text(userType == UserType.customer ? "New Customer" : "New Business")
                                        .font(Nunito.regular.font(size: CustomFontsSize.regular))
                                    
                                    
                                    Text("Sign Up")
                                        .font(Nunito.bold.font(size: CustomFontsSize.regular))
                                    
                                        .foregroundColor(bgColor)
                                        .padding(.leading, -1)
                                    
                                        .onTapGesture {
                                            signUpToggle.toggle()
                                        }
                                }
                                
                                .padding(.top, deviceHeight > 700 ? 30: 10)
                                .padding(.bottom, deviceHeight > 700 ? 20: 100)
                                
                                
                            }
                        }
                    }
                    
                })
                    .edgesIgnoringSafeArea(.top)
//                    .navigationBarHidden(true)
            }
            .onAppear{
                
                print("User Type: \(self.userType)")
            }
        }
        
        
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView(userType: UserType.customer)
    }
}
