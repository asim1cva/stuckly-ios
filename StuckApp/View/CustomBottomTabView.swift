//
//  CustomBottomTabView.swift
//  StuckApp
//
//  Created by Asim on 1/6/22.
//

import SwiftUI

//struct CustomBottomTabView: View {
//
//    // MARK: Properties
//    @State var selectedIndex: Int = 0
//
//
//    // MARK: Body
//
//    var body: some View {
//
//        CustomTabView(tabs: BusinessTabType.allCases.map({ $0.tabItem }),
//                      selectedIndex: $selectedIndex) { index in
//            let type = BusinessTabType(rawValue: index) ?? .home
//
//            getTabView(type: type)
//        }
//    }
    
    
//    @ViewBuilder
//    func getTabView(type: BusinessTabType) -> some View {
//        switch type {
//        case .home:
//            HomeView()
//        case .create:
//            CreateView()
//        case .profile:
//            ProfileView()
//        }
//    }
//}

//struct CustomBottomTabView_Previews: PreviewProvider {
//    static var previews: some View {
//        CustomBottomTabView()
//    }
//}


// MARK: Utils that used to create custom Bottom Nav Tabs

struct TabItemData {
    let image: String
    let selectedImage: String
    let title: String
}

struct TabItemView: View {
    
    let data: TabItemData
    let isSelected: Bool
    
    @EnvironmentObject var customerViewModel: BusinessTabsViewModel
    
    var body: some View {
        VStack {
            /// // if slect and unselect image different
            // Image(isSelected ? data.selectedImage : data.image)
            
            ZStack{
             

              
                
                Image(data.image)
                
                    .renderingMode(.template)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 24, height: 24)
                    .padding(.top, 10)
                
                
                if Global.shared.currentLoginUser.role == UserType.customer {
                    
                    if data.title == "Notifications" {
                        
                        HStack {
                            
                            Text("\(customerViewModel.notificationsListResponse?.notification_count ?? 0)")
                                .font(Nunito.bold.font(size: CustomFontsSize.exSmall))
                                .padding(4)
                                .foregroundColor(.white)
                                .background(LinearGradient(gradient: Gradient(colors: [Color("btnup"),
                                                                                       Color("btndown")]),
                                                           startPoint: .top,
                                                           endPoint: .bottom))
                                .cornerRadius(10)
                            
                        }
                        .padding(.leading)
                    }
                }
              
            }
            
         
            
            Spacer().frame(height: 4)
            
            Text(data.title)
            //                .foregroundColor(isSelected ? .black : .gray)
                .font(.footnote)
        }
        .foregroundColor(isSelected ? tabSelectedColor : tabUnSelectedColor)
    }
}



struct TabBottomView: View {
    
    let tabbarItems: [TabItemData]
    var height: CGFloat = 80
    var width: CGFloat = UIScreen.main.bounds.width - 0
    
    @Binding var selectedIndex: Int
    
    var body: some View {
        HStack {
            Spacer()
            
            ForEach(tabbarItems.indices) { index in
                let item = tabbarItems[index]
                
                ZStack {
                 
                   
                    Button {
                        self.selectedIndex = index
                    } label: {
                        let isSelected = selectedIndex == index
                        TabItemView(data: item, isSelected: isSelected)
                        
                    }
                }
              
                Spacer()
            }
        }
        
        .frame(width: width, height: height)
        .padding(.bottom)
        .background(Color.white)
//        .cornerRadius(0)
        //        .shadow(radius: 5, x: 0, y: 4)
    }
}



struct CustomTabView<Content: View>: View {
    
    let tabs: [TabItemData]
    @Binding var selectedIndex: Int
    @ViewBuilder let content: (Int) -> Content
    
    var body: some View {
        ZStack {
            TabView(selection: $selectedIndex) {
                
                ForEach(tabs.indices) { index in
                    
                    
                    if tabs[index].title == "Notifications" {
                        
                        content(index)
                            .tag(index)
                            .badge(10)
                    } else {
                        content(index)
                            .tag(index)
                            .badge(10)
                    }
                    
                   

                }
                

            }
            
            VStack {
                Spacer()
                TabBottomView(tabbarItems: tabs, selectedIndex: $selectedIndex)
            }
            //            .padding(.bottom, 8)
        }
        .background(Color.white)
        
    }
}


// Tabs for Business

enum BusinessTabType: Int, CaseIterable {
    case home = 0
   
    case profile
    case create

    case notification

    
    var tabItem: TabItemData {
        switch self {
        case .home:
            return TabItemData(image: "ic_home",
                               selectedImage: "ic_home",
                               title: "Home")
            
        case .profile:
            return TabItemData(image: "ic_profile",
                               selectedImage: "ic_profile",
                               title: "Profile")
            
        case .notification:
            return TabItemData(image: "ic_notification",
                               selectedImage: "ic_notification",
                               title: "Notifications")
            
        case .create:
            return TabItemData(image: "ic_create",
                               selectedImage: "ic_create",
                               title: "Add Business")
      
        }
    }
}


// Tabs for Customer

enum CustomerTabType: Int, CaseIterable {
    case home = 0
    case following

    case notification
    case profile

    
    var tabItem: TabItemData {
        
        switch self {
            
        case .home:
            return TabItemData(image: "ic_home",
                               selectedImage: "ic_home_c",
                               title: "Home")
            
        case .following:
            return TabItemData(image: "ic_heart",
                               selectedImage: "ic_notification",
                               title: "Following")
        case .notification:
            return TabItemData(image: "ic_notification",
                               selectedImage: "ic_notification",
                               title: "Notifications")
        
       
        case .profile:
            return TabItemData(image: "ic_profile",
                               selectedImage: "ic_profile",
                               title: "Profile")
        }
    }
}
