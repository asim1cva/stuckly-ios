//
//  ShedualeNotificationSheet.swift
//  StuckApp
//
//  Created by Asim on 2/25/22.
//

import SwiftUI

struct ShedualeNotificationSheet: View {
    
    // MARK: Properties
    
    
    @EnvironmentObject var businessViewModel: BusinessTabsViewModel
    
    @Environment(\.presentationMode) var presentationMode
    
    @State var showGraphical: Bool = false
    
    var dateFormatter: DateFormatter {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        return formatter
    }
    
    @State var showsDatePicker = false
    
    let dateTimeFormatter: DateFormatter = {
        let df = DateFormatter()
        df.timeStyle = .medium
        return df
    }()
    
    
    // MARK: Body
    
    var body: some View {
        
        LoadingView(isShowing: .constant(businessViewModel.loading)) {
            
            VStack {
                VStack {
                    HStack{
                        Text("Cancel")
                            .font(Nunito.bold.font(size: CustomFontsSize.regular))
                            .foregroundColor(.white)
                            .onTapGesture {
                                presentationMode.wrappedValue.dismiss()
                                
                            }
                        Spacer()
                    }
                    
                    
                    Text("Schedule Notification")
                    
                        .font(Nunito.bold.font(size: CustomFontsSize.medium))
                        .foregroundColor(.white)
                }
                
                .padding()
                .background(appColor)
                
                
                VStack(alignment: .leading, spacing: 16) {
                    ScrollView(showsIndicators: false, content: {
                        VStack {
                            
                            ZStack {
                                
                                
                                
                                
                                VStack(alignment: .leading) {
                                    
                                    // MARK: Date
                                    HStack {
                                        
                                        Text("Select Date:")
                                            .font(Nunito.bold.font(size: CustomFontsSize.regular))
                                        Spacer()
                                        
                                    }
                                    .padding(.top)
                                    
                                    
                                    //MARK: WeekDays..............
                                    
                                    HStack {
                                        
                                        
                                        Text("\(businessViewModel.scheduleDate, formatter: dateFormatter)")
                                        Spacer()
                                        
                                        
                                    }
                                    .onTapGesture {
                                        showGraphical.toggle()
                                        
                                    }
                                    .padding(.vertical)
                                    .padding(.horizontal, 8)
                                    .font(Nunito.regular.font(size: CustomFontsSize.regular))
                                    .frame(width: .infinity)
                                    .background(
                                        RoundedRectangle(cornerRadius: buttonCornerRadius, style: .continuous)
                                            .stroke(bgColor ,lineWidth: 0.5)
                                        
                                            .background(Color.white)
                                    )
                                    .sheet(isPresented: $showGraphical, content: {
                                        DatePickerSheet(currentDate: $businessViewModel.scheduleDate)
                                    })
                                    
                                    
                                    
                                    
                                    // MARK: Time
                                    HStack {
                                        
                                        Text("Select Time:")
                                            .font(Nunito.bold.font(size: CustomFontsSize.regular))
                                        Spacer()
                                        
                                    }
                                    .padding(.top)
                                    
                                    
                                    
                                    HStack {
                                        
                                        
                                        Text("\(businessViewModel.scheduleTime, formatter: dateTimeFormatter)")
                                        Spacer()
                                        
                                        
                                    }
                                    .onTapGesture {
                                        self.showsDatePicker.toggle()
                                    }
                                    .padding(.vertical)
                                    .padding(.horizontal, 8)
                                    
                                    .font(Nunito.regular.font(size: CustomFontsSize.regular))
                                    .frame(width: .infinity)
                                    .background(
                                        RoundedRectangle(cornerRadius: buttonCornerRadius, style: .continuous)
                                            .stroke(bgColor ,lineWidth: 0.5)
                                        
                                            .background(Color.white)
                                    )
                                    
                                    if showsDatePicker {
                                        DatePicker("", selection: $businessViewModel.scheduleTime,
                                                   displayedComponents: .hourAndMinute)
                                            .datePickerStyle(WheelDatePickerStyle())
                                    }
                                    
                                    
                                    
                                    
                                    //MARK: Write message textfield..........
                                    
                                    
                                    CustomTextEditor.init(placeholder: "Write Message", text: $businessViewModel.scheduleMessage)
                                        .font(Nunito.regular.font(size: CustomFontsSize.regular))
                                        .frame(width: .infinity, height: messageTextFiledHeight)
                                        .background(
                                            RoundedRectangle(cornerRadius: buttonCornerRadius, style: .continuous)
                                                .stroke(bgColor ,lineWidth: 0.5)
                                            
                                                .background(Color.white)
                                        )
                                        .padding(.top)
                                    
                                    
                                    
                                    //MARK: Confirm Button................
                                    
                                    
                                    HStack {
                                        
                                        CustomButton(text: "Schedule Notification")
                                            .onTapGesture {
                                                businessViewModel.setScheduleNotificationApiRequest()
                                            }
                                            .alert(item: $businessViewModel.alertItem) { alertItem in
                                                Alert(title: alertItem.title,
                                                      message: alertItem.message,
                                                      dismissButton: .default(Text("OK"), action: {
                                                    
                                                    if businessViewModel.setScheduleNotificationApiRequestToggle {
                                                        
                                                        //                                                        editApiToggle.toggle()
                                                        
                                                        presentationMode.wrappedValue.dismiss()
                                                        
                                                        
                                                    }
                                                    
                                                }))
                                                
                                            }
                                            .padding(.bottom, 40)
                                        
                                    }
                                    
                                    
                                }
                                .padding(.horizontal)
                                
                            }
                        }
                    })
                    
                }
                
                
            }
            
            
            
            
            
        }
        
    }
}

struct ShedualeNotificationSheet_Previews: PreviewProvider {
    static var previews: some View {
        ShedualeNotificationSheet()
    }
}
