//
//  SignUpView.swift
//  StuckApp
//
//  Created by Asim on 16/12/2021.
//

import SwiftUI

struct SignUpView: View {
    
    
    // MARK: Properties
    
    
    
    //    @State var mainViewToggle: Bool = false
    @State var loginToggle: Bool = false
    @Environment(\.presentationMode) var presentationMode
    
    var userType: UserType
    
    @StateObject var authVM = AuthViewModel()
    @StateObject var customerViewModel = CustomerViewModel()
    @StateObject var businessViewModel = BusinessTabsViewModel()

    
    
    // MARK: body
    
    var body: some View {
        
        LoadingView(isShowing: .constant(authVM.loading)) {
            
            ZStack {
                ScrollView(.vertical, showsIndicators: false, content: {
                    VStack {
                        ZStack {
                            
                            Image("bgImage")
                                .resizable()
                            
                            // MARK: Image....
                            
                            VStack {
                                
                                Image("stuck_Image")
                                    .resizable()
                                    .frame(width: 130, height: 130)
                                
                                Spacer()
                            }
                            .padding(.top, deviceHeight > 700 ? 50: 40)
                            
                            
                            VStack {
                                
                                
                                VStack {
                                    
                                    Text(userType == UserType.customer ? "Customer Sign Up" : "Business Sign Up")
                                        .font(Nunito.bold.font(size: CustomFontsSize.medium))
                                        .fontWeight(.bold)
                                }
                                .padding(.top, deviceHeight > 700 ? 150: 100)
                                
                               
                                
                                NavigationLink(
                                    destination: VerifyEmailView(userType: self.userType)
//                                                .navigationBarHidden(true)
                                        .navigationBarTitleDisplayMode(.inline)
                                        .environmentObject(authVM)
                                        .environmentObject(customerViewModel)
                                        .environmentObject(businessViewModel)
                                    ,
                                    isActive: $authVM.verifyEmailApiRequestToggle){}
                                    .hidden()
                                
                                
                              
                                
                                
                                //MARK: Username TextField.......
                                
                                VStack(alignment: .leading) {
                                    
                                    
                                    CustomTextFieldView(title: "Full Name",
                                                        text: $authVM.name,
                                                        iconName: "person.fill",
                                                        placeHolder: "Full Name")
                                    
                                    
                                    
                                    
                                    //MARK: Email TextField..........
                                    
                                    
                                    CustomTextFieldView(title: "Email",
                                                        text: $authVM.email,
                                                        iconName: "envelope.fill",
                                                        placeHolder: "Email")
                                    
                                    
                                    
                                    //MARK: Phone Number TextField.......
                                    
                                    
                                    
//                                    CustomTextFieldView(title: "Phone Number", text: $authVM.phone, iconName: "phone.fill", placeHolder: "Phone Number")
                                    
                                    
                                    
                                    //MARK: Password text Field........
                                    
                                    CustomTextFieldView(title: "Password", text: $authVM.password, iconName: "lock.fill", placeHolder: "Password")
                                    
                                    
                                    
                                    
                                    
                                    
                                    VStack {
                                        
                                        
//                                        NavigationLink(
//                                            destination: BusinessDashBoardView()
//                                                .navigationBarHidden(true)
//                                                .navigationBarTitleDisplayMode(.inline),
//                                            isActive: $authVM.businessDashboardViewToggle){}
//                                            .hidden()
                                        
                                        
                                    
                                        
                                        
                                        //MARK: Sign up Button.......
                                        
                                        
                                        CustomButton(text: "Sign Up")
                                            .onTapGesture {
                                                
                                                
                                                authVM.verifyEmailApiRequest()
//                                                authVM.businessDashboardViewToggle = true
//                                                authVM.signUpApiRequest(userType: self.userType)
                                                //                                                mainViewToggle.toggle()
                                            }
                                            .alert(item: $authVM.alertItem) { alertItem in
                                                Alert(title: alertItem.title,
                                                      message: alertItem.message,
                                                      dismissButton: alertItem.dismissButton)
                                            }
                                        
                                    }
                                    
                                    
                                    
                                    //MARK: Have another account text........
                                    
                                    HStack {
                                        
                                        Spacer()
                                        
                                        
                                        
                                        Text("Have another account")
                                        
                                        Text("Sign In")
                                            .foregroundColor(bgColor)
                                            .fontWeight(.semibold)
                                            .padding(.leading, -1)
                                            .onTapGesture {
                                                presentationMode.wrappedValue.dismiss()
                                            }
                                        
                                        Spacer()
                                    }
                                    .padding(.top)
                                    
                                    
                                }
                                .padding(.horizontal, 30)
                                .padding(.top)
                            }
                            
                        }
                        
                    }
                    
                })
                
            }
            
            
        }
        .edgesIgnoringSafeArea(.top)
//        .navigationBarHidden(true)
        
    }
    
}

struct SignUpView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        SignUpView(userType: UserType.customer)
    }
}
