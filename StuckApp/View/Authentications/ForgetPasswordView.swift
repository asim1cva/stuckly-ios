//
//  ForgetPasswordView.swift
//  StuckApp
//
//  Created by Asim on 27/12/2021.
//

import SwiftUI

struct ForgetPasswordView: View {
    
    //    @State var email: String = ""
    @State var openCodeViewToggle = false
    @Environment(\.presentationMode) var presentationMode
    
    @StateObject var authVM = AuthViewModel()
    
    
    var body: some View {
        
        NavigationView {
            
            LoadingView(isShowing: .constant(authVM.loading)) {
                VStack {
                    
                    HStack {
                        Button {
                            presentationMode.wrappedValue.dismiss()
                            
                        } label: {
                            Text("Cancel")
                                .font(Nunito.regular.font(size: CustomFontsSize.regular))
                            
                        }
                        
                        
                        Spacer()
                        Text("Verify Email")
                            .font(Nunito.bold.font(size: CustomFontsSize.medium))
                        
                        Spacer()
                        Spacer()
                    }
                    .padding()
                    Spacer()
                    
                    
                    
                    VStack {
                        
                        Text("You will reciece 6 digit Code")
                            .font(Nunito.regular.font(size: CustomFontsSize.regular))
                        
                        Text("to verify next.")
                            .font(Nunito.regular.font(size: CustomFontsSize.regular))
                        
                        
                        
                        CustomTextFieldView(title: "Email", text: $authVM.email, iconName: "envelope.fill", placeHolder: "Enter Email Address")
                        
                        CustomButton(text: "Verify Email")
                            .onTapGesture {
                                
                                authVM.forgotPasswordApiRequest()
                                
                                //                            openCodeViewToggle.toggle()
                                
                            }
                            .alert(item: $authVM.alertItem) { alertItem in
                                Alert(title: alertItem.title,
                                      message: alertItem.message,
                                      dismissButton: alertItem.dismissButton)
                            }
                        //                        .sheet(isPresented: $openCodeViewToggle, content: {
                        //                            VerifyCodeView()
                        //                        })
                        
                        
                        NavigationLink("", destination: VerifyCodeView(presentationMode: _presentationMode)
                                        .environmentObject(authVM)
                                        .navigationBarHidden(true),
                                       isActive: $authVM.otpViewToggle)
                        
                        
                    }
                    .padding()
                    Spacer()
                }
                .edgesIgnoringSafeArea(.top)
                .navigationBarHidden(true)
            }
        }
        
        
        
        
        
    }
}

struct ForgetPasswordView_Previews: PreviewProvider {
    static var previews: some View {
        ForgetPasswordView()
    }
}


// MARK: VerifyCodeView

struct VerifyCodeView: View {
    
    //    @State var code: String = ""
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var authVM: AuthViewModel
    //    @State var newPasswordViewToggle = false
    
    var body: some View {
        VStack {
            
            HStack {
                
                Button {
                    presentationMode.wrappedValue.dismiss()
                    
                } label: {
                    Text("Cancel")
                }
                
                
                Spacer()
                Text("Verify Code")
                Spacer()
                Spacer()
            }
            .padding()
            
            Spacer()
            
            
            
            
            VStack{
                
                HelperFunctions.highlightText(str: "Code is sent to \(authVM.email)", searched: "\(authVM.email)")
                    .lineLimit(2)
                CustomTextFieldView(title: "Code", text: $authVM.code, iconName: "", placeHolder: "6 digit code.. .")
                
                CustomButton(text: "Next")
                    .onTapGesture {
                        
                        if authVM.code.isEmpty {
                            authVM.alertItem = AlertContext.errorMessage(message: "Please enter 6 OTP.")
                            
                        } else if authVM.code.count < 6 {
                            
                            authVM.alertItem = AlertContext.errorMessage(message: "Invalid OTP.")
                            
                        } else if authVM.forgotPasswordApiResponse?.data.code == HelperFunctions.getIntFromString(str: authVM.code) {
                            
                            //                            authVM.alertItem = AlertContext.errorMessage(message: "Code matched")
                            
                            authVM.newPasswordViewToggle.toggle()
                        } else {
                            
                            authVM.alertItem = AlertContext.errorMessage(message: "Invalid OTP.")
                            
                        }
                        
                    }
                    .alert(item: $authVM.alertItem) { alertItem in
                        Alert(title: alertItem.title, message: alertItem.message, dismissButton: alertItem.dismissButton)
                    }
                
                
                NavigationLink("", destination: NewPasswordView( mode: presentationMode)
                                .environmentObject(authVM)
                                .navigationBarHidden(true),
                               isActive: $authVM.newPasswordViewToggle)
                
            }.padding(.horizontal)
            
            Spacer()
            
        }
        
    }
}

struct VerifyCodeView_Previews: PreviewProvider {
    static var previews: some View {
        VerifyCodeView()
    }
}



// MARK: NewPasswordView

struct NewPasswordView: View {
    
    @EnvironmentObject var authVM: AuthViewModel
    //    @StateObject var authVM = AuthViewModel()
    @Environment(\.presentationMode) var presentationMode
    @Binding var mode: PresentationMode
    
    var body: some View {
        LoadingView(isShowing: .constant(authVM.loading)) {
            ScrollView(.vertical, showsIndicators: false, content: {
                VStack {
                    HStack {
                        Text("Back")
                            .bold()
                            .onTapGesture {
                                authVM.newPassword = ""
                                authVM.confirmPassword = ""
                                presentationMode.wrappedValue.dismiss()
                            }
                            .opacity(authVM.isChangePasswordRequest ? 0:1)
                            .disabled(authVM.isChangePasswordRequest ? true:false)
                        Spacer()
                        Button {
                            authVM.newPassword = ""
                            authVM.confirmPassword = ""
                            mode.dismiss()
                        } label: {
                            Text(authVM.isChangePasswordRequest ? "Done" : "Cancel")
                                .bold()
                                .font(authVM.isChangePasswordRequest ? .title2 : .body)
                                .animation(Animation.easeInOut(duration: 1))
                        }
                        
                    }
                    .padding()
                    
                    Spacer()
                    
                    //MARK: Image..........................
                    
                    Image("appLogo")
                        .padding(.top, 80)
                    
                    //MARK: Info text...........................
                    
                    VStack {
                        
                        Text("New Password")
                            .font(Nunito.bold.font(size: CustomFontsSize.medium))
                        
                        //                        .font(Roboto.bold.font(size: CustomFontsSize.large))
                            .foregroundColor(.black)
                            .opacity(0.8)
                        
                        
                        Text("Reset your password to recover &")
                            .font(Nunito.regular.font(size: CustomFontsSize.regular))
                            .foregroundColor(.gray)
                            .padding(.top, 5)
                        
                        Text("login to your account.")
                            .font(Nunito.regular.font(size: CustomFontsSize.regular))
                            .foregroundColor(.gray)
                    }
                    .padding(.top)
                    
                    //MARK: Enter Old Password TextField......................
                    VStack(alignment: .leading) {
                        
                        
                        CustomTextFieldView(title: "Password", text: $authVM.newPassword, iconName: "key", placeHolder: "Enter new Password")
                        
                        
                        
                        
                        CustomTextFieldView(title: "Password", text: $authVM.confirmPassword, iconName: "key", placeHolder: "Confirm Password")
                        
                        
                        //MARK: Confirm Button...............................
                        
                        
                        
                        CustomButton(text: "Confirm")
                            .onTapGesture {
                                authVM.changePasswordApiRequest()
                                
                            }
                            .alert(item: $authVM.alertItem) { alertItem in
                                Alert(title: alertItem.title,
                                      message: alertItem.message,
                                      dismissButton: .default(Text("Ok")) {
                                    
                                    if authVM.isChangePasswordRequest {
                                        mode.dismiss()
                                    }
                                })
                            }
                        
                        
                        //                    HStack {
                        //                        Spacer()
                        //                        Button(action: {
                        //
                        //
                        //                        }, label: {
                        //
                        //                            CustomButton(text: "Confirm", image: "")
                        //                        })
                        //                        .padding(.top, 20)
                        //                        .alert(item: $authVM.alertItem) { alertItem in
                        //                            Alert(title: alertItem.title,
                        //                                  message: alertItem.message,
                        //                                  dismissButton: .default(Text("Ok")) {
                        //                                if authVM.passChanged {
                        //                                    mode.dismiss()
                        //                                }
                        //                            })
                        //                        }
                        //                        Spacer()
                        //                    }
                        
                    }
                    .padding(.top)
                    .padding(.horizontal)
                }
            })
                .edgesIgnoringSafeArea(.top)
        }
    }
}

