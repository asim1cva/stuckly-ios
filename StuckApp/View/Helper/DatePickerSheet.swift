//
//  DatePickerSheet.swift
//  StuckApp
//
//  Created by Admin Admin on 2/22/22.
//

import SwiftUI

struct DatePickerSheet: View {
    
    @Binding var currentDate: Date
    @Environment(\.presentationMode) var presentationMode

    var body: some View {
        
        VStack {
            
            //https://developer.apple.com/documentation/swiftui/datepicker
            //            DatePicker("Date", selection: $currentDate, displayedComponents: [.date, .hourAndMinute])
            //                //.datePickerStyle(CompactDatePickerStyle())
            //                .datePickerStyle(GraphicalDatePickerStyle())
            DatePicker(selection: $currentDate , displayedComponents: .date) {
                Text("Delivery Date")
            }
            .datePickerStyle(GraphicalDatePickerStyle())
            .font(.body)
            
            Button {
                presentationMode.wrappedValue.dismiss()

            } label: {
                Text("Done")
            }


        }
        
    }
}
