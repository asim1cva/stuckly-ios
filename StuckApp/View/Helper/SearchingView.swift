//
//  SearchingView.swift
//  StuckApp
//
//  Created by Admin Admin on 2/23/22.
//

import SwiftUI


struct SearchingView: View {
    
    @Binding var searchingFor: String
    @Binding var searchingisActive: Bool
    
    var body: some View {
        HStack {
            TextField("Search", text: $searchingFor)
                .padding(.vertical, 5)
                .padding(.horizontal)
                .background(Color.white)
                .frame(width: deviceWidth-130)
                .cornerRadius(100)
//                .placeholder(when: searchingFor.isEmpty) {
//                    Text("Search here...")
//
//                        .padding()
//                }
                .padding(.vertical, 5)
            
            Button {
                withAnimation {
                    searchingFor = ""
                    searchingisActive = false
                }
            } label: {
                Text("Cancel")
                    .font(Nunito.regular.font(size: CustomFontsSize.small))
                    .foregroundColor(Color.white)
            }
            
        }
        .padding(.horizontal)
//        .foregroundColor(Color.black)
    }
}
