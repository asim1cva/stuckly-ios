//
//  CustomTextFieldView.swift
//  Distro
//
//  Created by Asim on 12/24/21.
//

import SwiftUI

struct CustomTextFieldView: View {
    
    var title: String
    @Binding var text: String
    @State var iconName: String = ""
    @State var placeHolder: String = ""
    @State private var showPassword: Bool = false

    @State private var passwordIcon = "eye.fill"

    var body: some View {
        
    
        VStack(alignment: .leading) {
            
            
//            Text("\(title):")
//                .font(.callout)
//                .hidden()
            
            HStack {
                
                Image(systemName: iconName)
                    .foregroundColor(bgColor)
                
                Text("|")
                    .foregroundColor(bgColor)
                
                switch (title) {
                    
                case "Password":
                    
                    if showPassword {
//                        TextField ("Password", text: $authVM.password)
                        TextField(placeHolder, text: $text)

                    } else {
//                        SecureField ("Password", text: $authVM.password)
                        
                        
                        SecureField(placeHolder, text: $text)

                    }
                    
                    Button(action: {
                        showPassword.toggle()
                        if passwordIcon == "eye.fill" {
                            passwordIcon = "eye"
                        } else {
                            passwordIcon = "eye.fill"
                        }
                    }, label: {
                        Image(systemName: passwordIcon)
                            .resizable()
                            .frame(width: 25, height: 15, alignment: .center)
                            .foregroundColor(.gray)
//                            .padding(.trailing)
                    })
                    
                    
                    
                case "Phone Number":
                    TextField(placeHolder, text: $text)
                        .keyboardType(.numberPad)
                    
                case "Email":
                    TextField(placeHolder, text: $text)
                        .keyboardType(.emailAddress)
                    
                default:
                    TextField(placeHolder, text: $text)
                        .keyboardType(.default)
                }
                
//                TextField("Email", text: $email)
//                    .autocapitalization(.none)
                
            }
            .font(Nunito.regular.font(size: CustomFontsSize.regular))

            .padding()
            .background(
                RoundedRectangle(cornerRadius: buttonCornerRadius, style: .continuous)
                    .stroke(bgColor ,lineWidth: 0.5)

                            .background(Color.white)
            )
            
        }
        .padding(.top, 10)
        
        
    }
}


