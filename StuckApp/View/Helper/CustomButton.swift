//
//  CustomButton.swift
//  StuckApp
//
//  Created by AR Computers on 16/12/2021.
//

import SwiftUI

struct CustomButton: View {
    
    var text: String
    
    var body: some View {
        
        VStack {
            
            HStack {
                
                Text(text)
                    .font(Nunito.bold.font(size: CustomFontsSize.regular))
                    .padding()
                    .frame(maxWidth: .infinity, maxHeight: deviceHeight > 700 ? textFieldMaxHeight: textFieldMinHeight)
                    .foregroundColor(.white)
                    .background(LinearGradient(gradient: Gradient(colors: [Color("btnup"),
                                                                           Color("btndown")]),
                                               startPoint: .top,
                                               endPoint: .bottom))
                    .cornerRadius(10)
                    .padding(.top)
            }
        }
    }
}

struct CustomButton_Previews: PreviewProvider {
    static var previews: some View {
        CustomButton(text: "")
    }
}
