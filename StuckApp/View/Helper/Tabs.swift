//
//  Tabs.swift
//  SwiftUiPractice
//
//  Created by Asim on 11/30/20.
//

import SwiftUI

struct Tab {
    var icon: Image?
    var title: String
}

struct Tabs: View {
    var fixed = true
    var tabs: [Tab]
    var geoWidth: CGFloat
    @Binding var selectedTab: Int

    var body: some View {
        
        HStack {
            Spacer()
            HStack {
                
                ScrollViewReader { proxy in
                    VStack(spacing: 0) {
                        HStack(spacing: 0) {
                           
                            ForEach(0 ..< tabs.count, id: \.self) { row in
                                Button(action: {
                                    withAnimation {
                                        selectedTab = row
                                    }
                                }, label: {
                                    VStack(spacing: 0) {
                                        HStack {
   

                                            // Text
                                            Text(tabs[row].title)
                                                .font(Font.system(size: 14, weight: .semibold))
    //                                            .foregroundColor(Color.black)
    //                                            .padding(EdgeInsets(top: 10, leading: 3, bottom: 10, trailing: 3))
                                        }
                                        .frame(width: deviceWidth/3, height: 30, alignment: .center)
    //                                    .frame(width: fixed ? (geoWidth / CGFloat(tabs.count)) : .none, height: 40)
                                        // Bar Indicator
    //                                    Rectangle().fill(selectedTab == row ? Color.black : Color.gray)
    //                                        .frame(height: 3)
                                    }
                                    
                                    .fixedSize()
                                    
                                        .background(
                                        
                                            Rectangle().fill(selectedTab == row ? appColor : Color.gray.opacity(0.5))
                                                .cornerRadius(32)
    //                                            .frame(height: 3)
                                        )
                                        .foregroundColor(selectedTab == row ? Color.white : Color.black)

                                })
                                    .padding(.horizontal, 4)
                                    .padding(.vertical, 4)
                                    .accentColor(Color.white)
                                    .buttonStyle(PlainButtonStyle())
                            }
                            
                        }
                        .onChange(of: selectedTab) { target in
                            withAnimation {
                                proxy.scrollTo(target)
                            }
                        }
                        
                    }
                    .padding(2)
                    .background(
                    
                        Rectangle().fill(Color.gray.opacity(0.3))
                            .cornerRadius(32)
    //                                            .frame(height: 3)
                    )
                    .padding(4)
            }
               

                
            }
            .frame(height: 50)
            .onAppear(perform: {
    //            UIScrollView.appearance().backgroundColor = UIColor(#colorLiteral(red: 0.6196078431, green: 0.1098039216, blue: 0.2509803922, alpha: 1))
                UIScrollView.appearance().bounces = fixed ? false : true
            })
            .onDisappear(perform: {
                UIScrollView.appearance().bounces = true
            })
            .padding(.top, 4)
            
            
            Spacer()
        }
      
    }
}

struct Tabs_Previews: PreviewProvider {
    static var previews: some View {
        Tabs(fixed: true,
             tabs: [.init(icon: Image(systemName: "star.fill"), title: "Tab 1"),
                    .init(icon: Image(systemName: "star.fill"), title: "Tab 2"),
                    .init(icon: Image(systemName: "star.fill"), title: "Tab 3")],
             geoWidth: 375,
             selectedTab: .constant(0))
    }
}
