//
//  ProfileView.swift
//  StuckApp
//
//  Created by AR Computers on 16/12/2021.
//

import SwiftUI
import UIKit

struct ProfileView: View {
    

    // MARK: Properties
    @State var logoutAlertToggle = false

    @State var pickDate = Date()
    
    @EnvironmentObject var authVM : AuthViewModel
    @State var user: DataClass?
    
    
    // MARK: body

    var body: some View {
        
        LoadingView(isShowing: .constant(authVM.loading)) {
            VStack(alignment: .leading, spacing: 8) {
                
                ZStack {
                    Image("topBack")
                        .resizable()
                        .frame(width: deviceWidth, height: deviceHeight/5)
                        .ignoresSafeArea()
                    
                    
                    
                    Text("Profile")
                    
                        .font(Nunito.bold.font(size: CustomFontsSize.medium))
                        .foregroundColor(.white)
                    
                    
                    HStack {

                        Spacer()
                        Image(systemName: "rectangle.portrait.and.arrow.right.fill")
                            .resizable()
                            .frame(width: 24, height: 24)

                            .foregroundColor( .white)
                            .padding(.trailing)
                            .onTapGesture {

                                    self.logoutAlertToggle.toggle()
                            }
                    }
                   
                  
                    
                   
                }
                
                VStack {
                    ScrollView(.vertical, showsIndicators: false) {
                        VStack(alignment: .leading) {
                            
                            // Alerts....
                            
                            ZStack { }
                            .alert(item: $authVM.alertItem, content: { item in
                                Alert(title: item.title,
                                      message: item.message,
                                      primaryButton: .default(Text("Yes")) {
                                    authVM.editProfileApiRequest()
                                    
                                }, secondaryButton: .destructive(Text("No")))
                            })
                            ZStack{}
                            .alert(isPresented: $authVM.profileUpdateAlertToggle) {
                                Alert(title: Text("Updated!"), message:
                                        Text("Profile updated successfully!"),
                                      dismissButton: .default(Text("Ok")))
                            }
                            
                            ZStack{}
                            .alert(isPresented: $logoutAlertToggle) {
                                Alert(title: Text("Logout?"),
                                      message: Text("Are you sure you want to logout?"),
                                      primaryButton: .default(Text("Yes")) {
                                    
                                    
                                    
                                    authVM.signout()
                                    
                                    
                                },
                                      secondaryButton: .destructive(Text("No"))
                                )
                                
                               
                            }
                            
                            VStack(alignment: .leading) {
                                
                                Text("Update Information")
                                    .font(Nunito.bold.font(size: CustomFontsSize.medium))
                                    .padding(.bottom)
                                
                                VStack(alignment: .leading) {
                                    
                                    Text("Name:")
                                        .font(Nunito.bold.font(size: CustomFontsSize.regular))


                                    HStack {
                                        
                                        
                                        TextField("Business Name", text: $authVM.name)
                                            .autocapitalization(.none)
                                        
                                    }
                                    .padding()
                                    
                                    .frame(maxWidth: .infinity)
                                    
                                    .font(Nunito.regular.font(size: CustomFontsSize.regular))
                                    .background(
                                        RoundedRectangle(cornerRadius: buttonCornerRadius, style: .continuous)
                                            .stroke(bgColor ,lineWidth: 0.4)

                                                    .background(Color.white)
                                    )
                                }
                                .onChange(of: authVM.name) { _ in
                                    detectChangesInProfile(name: authVM.name,
                                                           phone: authVM.phone)
                                }
                                
                                
                                //MARK: Email TextField..........
                                
                                
                                VStack(alignment: .leading) {
                                    Text("Email Address:")
                                        .font(Nunito.bold.font(size: CustomFontsSize.regular))

                                    HStack {
                                        
                                        Text(authVM.email)
                                            .font(Nunito.regular.font(size: CustomFontsSize.regular))

                                        Spacer()
        //                                    .autocapitalization(.none)
                                        
                                    }
                                    .padding()
                                    
                                    .frame(maxWidth: .infinity)
                                    
                                    .font(Nunito.regular.font(size: CustomFontsSize.regular))
                                    .background(
                                        RoundedRectangle(cornerRadius: buttonCornerRadius, style: .continuous)
                                            .stroke(bgColor ,lineWidth: 0.4)

                                                    .background(Color.white)
                                    )
                                }
                                .padding(.top)
                                
                                //MARK: Phone Number TextField.......
                                
        //                        VStack(alignment: .leading) {
        //
        //                            Text("Phone Number:")
        //                                .font(Nunito.bold.font(size: CustomFontsSize.regular))
        //
        //
        //                            HStack {
        //
        //
        //                                TextField("+6245677434", text: $authVM.phone)
        //                                    .autocapitalization(.none)
        //                                    .textContentType(.oneTimeCode)
        //                                    .keyboardType(.numberPad)
        //
        //                            }
        //                            .padding()
        //
        //                            .frame(maxWidth: .infinity)
        //
        //                            .font(Nunito.regular.font(size: CustomFontsSize.regular))
        //                            .background(
        //                                RoundedRectangle(cornerRadius: buttonCornerRadius, style: .continuous)
        //                                    .stroke(bgColor ,lineWidth: 0.4)
        //
        //                                            .background(Color.white)
        //                            )
        //                        }
        //                        .padding(.top)
        //                        .onChange(of: authVM.phone) { _ in
        //                            detectChangesInProfile(name: authVM.name,
        //                                                   phone: authVM.phone)
        //                        }
                                
                                //MARK:  Button.......
                                
                                
                                if authVM.showUpdateButton {
                                    CustomButton(text: "Update Changes")
                                        .onTapGesture {
                                            
                                            authVM.alertItem = AlertContext.successMessage(title: "Update",
                                                                                           message: "Are you sure you want to update your profile?")
                                            
        //                                    // for splitting name....
        //                                    let name = authVM.fullName.split(separator: " ", maxSplits: 1)
        //                                    authVM.firstName = String(name[0])
        //                                    authVM.lastName  = String(name[1])
                                        }
                                }

                                

                                
                            }

                        }
                        .padding([.leading, .trailing])
                        .background(RoundedCorners(color: .white, tl: 24, tr: 46, bl: 0, br: 0))
                        .padding(.bottom, 80)
                        .padding(.top, 40)
                    }

                }
                .frame(maxWidth: deviceWidth)

                .background(RoundedCorners(color: .white, tl: 24, tr: 46, bl: 0, br: 0))
                .padding(.top, -49)

                
            

            }
          
            .onAppear {
                getUserProfile()
            }
            .ignoresSafeArea()
            .navigationBarTitleDisplayMode(.inline)
            .edgesIgnoringSafeArea(.bottom)

        }

       
    }
    
    
    // fetch userProfile from userDefaults....
    func getUserProfile() {
        
        user = Global.shared.currentLoginUser

        authVM.name = Global.shared.currentLoginUser.userName ?? ""
        authVM.email = Global.shared.currentLoginUser.email ?? ""
        authVM.phone = Global.shared.currentLoginUser.phoneNumber ?? ""
        
    }
    
    
    //    detecting changes for update profile....
    func detectChangesInProfile(name: String, phone: String) {
        
        let fName = Global.shared.currentLoginUser.userName ?? ""
        
        if name != fName ||
            phone != user?.phoneNumber ?? "" {
            
            withAnimation { authVM.showUpdateButton = true }
            
        } else {  withAnimation { authVM.showUpdateButton = false } }
    }
    
    
}




struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}



