//
//  DetailView.swift
//  StuckApp
//
//  Created by Asim on 16/12/2021.
//

import SwiftUI

struct DetailView: View {
    
    
    // MARK: Properties
    
    @EnvironmentObject var businessViewModel: BusinessTabsViewModel
    
    //    @StateObject var businessViewModel = BusinessTabsViewModel()
    
    var gridItemLayout = [GridItem(.flexible())]
    @State var followToggle = false
    
    @State private var sheetToggle = false
    
    @State private var scheduleSheetToggle = false
    
    @State private var editBusinessToggle = false
    @Environment(\.openURL) var openURL
    
    
    @State var index = 0
    
    
    //MARK: Body
    
    var body: some View {
        
        LoadingView(isShowing: .constant(businessViewModel.loading)) {
            VStack {
                
                ZStack {
                    Image("topBack")
                        .resizable()
                        .frame(width: deviceWidth, height: deviceHeight/5)
                        .ignoresSafeArea()
                    
                    
                    
                    Text("Details")
                    
                        .font(Nunito.bold.font(size: CustomFontsSize.medium))
                        .foregroundColor(.white)
                }
                
                
                
                
                //MARK: horizental scrolled images.....
                VStack(alignment: .leading) {
                    
                    ScrollView(showsIndicators: false) {
                        VStack {
                            
                            
                            ZStack {
                                AsyncImageLoader(imageUrl: businessViewModel.businessData?.images[0].imageURL ?? "",
                                                 imageWidth: deviceWidth-60,
                                                 imageHeight: 250,
                                                 isRounded: false,
                                                 cornerRadius: imageCorner)
                                    .cornerRadius(imageCorner)
                            }
                            
                            //                            .frame(height: 250)
                            .padding(.top)
                            
                            
                            
                            //                            .hidden()
                            
                            
                            
                            
                            
                            VStack(alignment: .leading, spacing: 8) {
                                
                                HStack {
                                    
                                    Text(businessViewModel.businessData!.name ?? "")
                                        .font(Nunito.bold.font(size: CustomFontsSize.medium))
                                        .foregroundColor(tabSelectedColor)
                                    Spacer()
                                }
                                .padding(.top)
                                
                                
                                
                                //MARK: text..............
                                
                                
                                // VStack {
                                HStack {
                                    
                                    Text(businessViewModel.businessData!.datumDescription ?? "")
                                        .font(Nunito.regular.font(size: CustomFontsSize.regular))
                                    
                                        .foregroundColor(.gray)
                                }
                                
                                HStack {
                                    
                                    Button(businessViewModel.businessData!.websiteLink ?? "") {
                                        openURL(URL(string: "https://\(businessViewModel.businessData!.websiteLink ?? "")")!)
                                    }
                                    
                                    //                                    Text(businessViewModel.businessData!.websiteLink ?? "")
                                    //                                        .font(Nunito.regular.font(size: CustomFontsSize.regular))
                                    
                                }
                                .padding(.vertical, 8)
                                
                                
                            }
                            .padding([.leading, .trailing])
                            //.padding(.trailing, 2)
                            
                            
                            relaseNotesView()
                            
                            
                            if Global.shared.currentLoginUser.role == UserType.business {
                                
                                Divider()
                                
                                NavigationLink(destination: EditBusinessView(editBuisnessData: businessViewModel.businessData),
                                               //                                            .environmentObject(businessViewModel),
                                               isActive: $editBusinessToggle){ EmptyView() }
                                
                                
                                
                                //MARK: Button..........
                                
                                
                                
                                CustomButton(text: businessViewModel.businessData!.releaseDate == nil ? "Setup Event Date": "Change Event Date")
                                    .onTapGesture {
                                        
                                        print("Tapped")
                                        self.sheetToggle = true
                                        //                                        releaseDateViewToggle.toggle()
                                        
                                    }
                                    .padding(.top)
                                    .sheet(isPresented: $sheetToggle, content: {
                                        ReleaseDateView()
                                            .environmentObject(businessViewModel)
                                        
                                    })
                                
                                HStack {
                                    
                                    Text("Schedule Notification")
                                        .font(Nunito.bold.font(size: CustomFontsSize.small))
                                        .padding()
                                        .frame(maxWidth: .infinity, maxHeight: deviceHeight > 700 ? textFieldMaxHeight: textFieldMinHeight)
                                        .foregroundColor(Color("btnup"))
                                    
                                        .overlay(
                                            RoundedRectangle(cornerRadius: 10)
                                                .stroke(Color("btnup"), lineWidth: 5)
                                        )
                                    
                                        .cornerRadius(10)
                                        .padding(.top)
                                }
                                //                                CustomButton(text: "Schedule Notification")
                                .onTapGesture {
                                    
                                    print("Tapped")
                                    self.scheduleSheetToggle = true
                                    //                                        releaseDateViewToggle.toggle()
                                    
                                }
                                .sheet(isPresented: $scheduleSheetToggle, content: {
                                    
                                    ShedualeNotificationSheet()
                                        .environmentObject(businessViewModel)
                                    //                                        ReleaseDateView()
                                })
                                
                                if !businessViewModel.isInstantToggle {
                                    
                                    
                                    
                                    HStack {
                                        
                                        Text("Send Instant Notification to Followers")
                                            .font(Nunito.bold.font(size: CustomFontsSize.small))
                                            .padding()
                                            .frame(maxWidth: .infinity, maxHeight: deviceHeight > 700 ? textFieldMaxHeight: textFieldMinHeight)
                                            .foregroundColor(Color("btnup"))
                                        
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 10)
                                                    .stroke(Color("btnup"), lineWidth: 5)
                                            )
                                        
                                            .cornerRadius(10)
                                            .padding(.top)
                                    }
                                    .onTapGesture {
                                        
                                        businessViewModel.isInstantToggle = true
                                    }
                                    //                                    CustomButton(text: "Send Instant Notification to Followers")
                                    
                                    //                                    Text("Send Instant Notification to Followers")
                                    //                                        .font(Nunito.semiBold.font(size: CustomFontsSize.small))
                                    //                                        .padding()
                                    //                                    //                                .frame(maxWidth: .infinity, maxHeight: deviceHeight > 700 ? textFieldMaxHeight: textFieldMinHeight)
                                    //                                        .foregroundColor(buttonBgColor)
                                    
                                    
                                } else {
                                    
                                    Divider()
                                        .padding()
                                    Text("Send Instant Notification to Followers:")
                                        .font(Nunito.bold.font(size: CustomFontsSize.small))
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                        .padding(.top)
                                    
                                    
                                    
                                    
                                    CustomTextEditor.init(placeholder: "Write Message", text: $businessViewModel.instantNotificationMessage)
                                        .font(Nunito.regular.font(size: CustomFontsSize.regular))
                                        .frame(width: .infinity, height: messageTextFiledHeight)
                                        .background(
                                            RoundedRectangle(cornerRadius: buttonCornerRadius, style: .continuous)
                                                .stroke(bgColor ,lineWidth: 0.5)
                                            
                                                .background(Color.white)
                                        )
                                    
                                    CustomButton(text: "Send Notification")
                                        .onTapGesture {
                                            
                                            print("Send Notification")
                                            
                                            businessViewModel.sendInstantNotificationApiRequest()
                                            //                                        isInstantToggle = false
                                            
                                        }
                                    
                                        .padding(.bottom, 40)
                                    
                                }
                                
                            }
                            
                            
                            
                            
                            
                        }
                        .padding([.leading, .trailing])
                        .background(RoundedCorners(color: .white, tl: 24, tr: 46, bl: 0, br: 0))
                        .padding(.bottom, 80)
                        
                    }
                    
                    
                    
                    
                }
                //                .frame(width: deviceWidth)
                .background(RoundedCorners(color: .white, tl: 24, tr: 46, bl: 0, br: 0))
                .padding(.top, -49)
                
            }
            .alert(item: $businessViewModel.alertItem) { alertItem in
                Alert(title: alertItem.title,
                      message: alertItem.message,
                      dismissButton: alertItem.dismissButton)
            }
            .onAppear {
                
                
                businessViewModel.isInstantToggle = false
                businessViewModel.businessName = businessViewModel.businessData?.name ?? ""
                businessViewModel.businessDesc = businessViewModel.businessData?.datumDescription ?? ""
                businessViewModel.businessWebsiteLink = businessViewModel.businessData?.websiteLink ?? ""
                
                
                
                if businessViewModel.businessData?.releaseDate != nil {
                    
                    businessViewModel.releaseDate = HelperFunctions.stringToDate(strDate: businessViewModel.businessData?.releaseDate ?? "")
                    
                    businessViewModel.message = businessViewModel.businessData?.releaseMessage ?? ""
                    businessViewModel.detail = businessViewModel.businessData?.releaseDetail ?? ""
                    
                }
                
                
                if Global.shared.currentLoginUser.role == UserType.customer {
                    
                    self.followToggle = businessViewModel.businessData!.isfollowed ?? false
                    
                }
                
                //            businessViewModel.businessData?.isfollowed = self.followToggle
            }
            .keyboardAware()
            .toolbar(content: {
                
                ToolbarItem(placement: .navigationBarTrailing) {
                    
                    HStack {
                        
                        
                        if Global.shared.currentLoginUser.role == UserType.business {
                            
                            // Edit Business Detail
                            Image("ic_edit")
                                .padding(.trailing)
                                .onTapGesture {
                                    self.editBusinessToggle = true
                                }
                            //                                .hidden()
                        } else {
                            
                            
                            HStack {
                                Text(followToggle ? "Following": "Follow")
                                    .font(Nunito.regular.font(size: CustomFontsSize.small))
                                    .foregroundColor(.white)
                                
                                
                                
                                Image(systemName: "heart.fill")
                                    .resizable()
                                    .frame(width: 20, height: 20)
                                
                                    .foregroundColor(followToggle ? appColorStrokes : .white)
                            }
                            .onTapGesture {
                                
                                
                                businessViewModel.followBusinessApiRequest()
                                
                                followToggle.toggle()
                            }
                            
                            
                        }
                        
                        
                        
                        
                    }
                    
                }
            })
            
            .ignoresSafeArea()
            .navigationBarTitleDisplayMode(.inline)
            .edgesIgnoringSafeArea(.bottom)
        }
        
        
        
    }
    
    private func editJobImages() {
        
        DispatchQueue.global(qos: .background).async {
            
            do {
                
                
                for imageUrl in businessViewModel.businessData?.images ?? [] {
                    
                    print(imageUrl)
                    
                    let imageData = try Data.init(contentsOf: URL.init(string: imageUrl.imageURL ?? "")!)
                    
                    DispatchQueue.main.async { businessViewModel.uploadImagesList.append(UIImage(data: imageData)!) }
                }
                
            } catch {
                // error
                //                return image
                
            }
        }
    }
    
    
    
    
    // MARK: relaseNotesView
    
    @ViewBuilder
    func relaseNotesView() -> some View {
        
        
        if businessViewModel.businessData!.releaseDate != nil {
            
            
            
            
            Divider()
            
            VStack  {
                
                VStack (alignment: .leading, spacing: 8) {
                    
                    HStack {
                        
                        Text("Event Date")
                            .font(Nunito.bold.font(size: CustomFontsSize.regular))
                        Spacer()
                    }
                    .padding(.top)
                    
                    
                    
                    // VStack {
                    HStack {
                        
                        Text(businessViewModel.businessData!.releaseDate ?? "")
                            .font(Nunito.regular.font(size: CustomFontsSize.small))
                        
                            .foregroundColor(.gray)
                    }
                    
                }
                
                VStack (alignment: .leading, spacing: 8) {
                    
                    HStack {
                        
                        Text("Event Name")
                            .font(Nunito.bold.font(size: CustomFontsSize.regular))
                        Spacer()
                    }
                    .padding(.top, 8)
                    
                    
                    
                    // VStack {
                    HStack {
                        
                        Text(businessViewModel.businessData!.releaseMessage ?? "")
                            .font(Nunito.regular.font(size: CustomFontsSize.small))
                        
                            .foregroundColor(.gray)
                    }
                    
                }
                
                
                VStack (alignment: .leading, spacing: 8) {
                    
                    HStack {
                        
                        Text("Event Details")
                            .font(Nunito.bold.font(size: CustomFontsSize.small))
                        Spacer()
                    }
                    .padding(.top, 8)
                    
                    
                    
                    // VStack {
                    HStack {
                        
                        Text(businessViewModel.businessData!.releaseDetail ?? "")
                            .font(Nunito.regular.font(size: CustomFontsSize.regular))
                            .foregroundColor(.gray)
                    }
                    
                }
                
                
                
                // MARK: PagingView
                
                HStack {
                    
                    Text("Event Photos")
                        .font(Nunito.bold.font(size: CustomFontsSize.regular))
                    Spacer()
                }
                .padding(.top, 8)
                
                if businessViewModel.businessData?.releaseImages != [] {
                    
                    HStack {
                        
                        PagingView(index: $index.animation(), maxIndex: businessViewModel.businessData!.releaseImages.count - 1) {
                            
                            ForEach(businessViewModel.businessData!.releaseImages, id: \.self) { imageUrl in
                                
                                ZStack {
                                    AsyncImageLoader(imageUrl: imageUrl ,
                                                     imageWidth: deviceWidth,
                                                     imageHeight: 250,
                                                     isRounded: false,
                                                     cornerRadius: imageCorner)
                                    
                                }
                                .frame(width: deviceWidth, height: 250)
                                
                            }
                        }
                        .frame(height: 250)
                        .cornerRadius(imageCorner)
                        
                        //                Stepper("", value: $index.animation(.easeInOut), in: 0...businessViewModel.businessData!.images.count-1)
                        //                    .font(Font.body.monospacedDigit())
                        //                    .hidden()
                    }
                    //                    .padding()
                    
                } else {
                    
                    Text("No Event Photos found yet")
                        .font(Nunito.bold.font(size: CustomFontsSize.small))
                        .foregroundColor(.gray)
                        .padding()
                }
                
                
                
                
                
            }
            .padding([.leading, .trailing])
            
        }
        
        
    }
}

//struct DetailView_Previews: PreviewProvider {
//    static var previews: some View {
//        DetailView(businessData: nil)
//    }
//}
