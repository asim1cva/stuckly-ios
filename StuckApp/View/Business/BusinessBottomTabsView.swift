////
////  MainView.swift
////  StuckApp
////
////  Created by Asim on 16/12/2021.
////
//
//import SwiftUI
//
//struct BusinessBottomTabsView: View {
//    
//    
//    @State private var title = "Business"
//    @State var selectedIndex: Int = 0
//    @State var logoutAlertToggle = false
//    
////    @StateObject var authVM = AuthViewModel()
//    
//    
//    @StateObject var businessViewModel = BusinessTabsViewModel()
//    
//    
//    
//    var body: some View {
//        
//        LoadingView(isShowing: .constant(businessViewModel.loading)) {
//            
//            VStack {
//                
//                
//                ZStack {
//                    Image("topBack")
//                        .resizable()
//                        .frame(width: deviceWidth, height: deviceHeight/5)
//                        .ignoresSafeArea()
//                    
//                    Text(title.capitalized)
//                    
//                        .font(Nunito.bold.font(size: CustomFontsSize.medium))
//                        .foregroundColor(.white)
//                    
//                    
//                }
//                
//                VStack {
//                    NavigationLink(destination: SelectionView()
//                                    .navigationBarHidden(true)
//                                    .navigationBarBackButtonHidden(true),
//                                   isActive: $businessViewModel.signOutToggle){}
//                                    .hidden()
//                    
//                    Text(" ")
//                        .frame(height: 20, alignment: .center)
//                        .foregroundColor(.white)
//                        .background(Color.white)
//                        .cornerRadius(16)
//                    
//                        .padding(.top)
//                    
//                    ZStack {
//                        CustomTabView(tabs: BusinessTabType.allCases.map({ $0.tabItem }),
//                                      selectedIndex: $selectedIndex) { index in
//                            
//                            let type = BusinessTabType(rawValue: index) ?? .home
//                            
//                            getTabView(type: type)
//                        }
//                    }
//                    
//                    
//                }
//                .frame(width: deviceWidth)
//                .background(RoundedCorners(color: .white, tl: 24, tr: 46, bl: 0, br: 0))
//                .padding(.top, -49)
//                
//                
//            }
//            .alert(isPresented: $logoutAlertToggle) {
//                Alert(title: Text("Logout?"),
//                      message: Text("Are you sure you want to logout?"),
//                      primaryButton: .default(Text("Yes")) {
//                    
//                    
//                    
//                    businessViewModel.signout()
//                    
//                    
//                },
//                      secondaryButton: .destructive(Text("No"))
//                )}
//            
//            .toolbar(content: {
//                
//                ToolbarItem(placement: .navigationBarTrailing) {
//                    HStack {
//                        
//                        
//                        
//                        Image(systemName: "rectangle.portrait.and.arrow.right.fill")
//                            .resizable()
//                            .frame(width: 24, height: 24)
//                            .onTapGesture {
//                                
//                                self.logoutAlertToggle.toggle()
//                            }
//                            .foregroundColor( .white)
//                        
//                    }
//                    
//                }
//            })
//            .ignoresSafeArea()
//            .navigationBarTitleDisplayMode(.inline)
//            .edgesIgnoringSafeArea(.bottom)
//        }
//        
//        
//        
//        
//    }
//    
//    @ViewBuilder
//    func getTabView(type: BusinessTabType) -> some View {
//        
//        switch type {
//        case .home:
//            HomeView()
//                .padding(.bottom)
//                .onAppear {
//                    businessViewModel.getUsersBusinessListApiRequest()
//
//                    print("HomeView - onAppear")
//                    self.title = "Business"
//                    
//                }
//                .environmentObject(businessViewModel)
//            
//        case .create:
//            CreateView()
//                .onAppear {
//                    print("CreateView - onAppear")
//                    
//                    self.title = "Create"
//                    
//                }
//            
//            
//        case .profile:
//            ProfileView()
//                .onAppear {
//                    print("ProfileView - onAppear")
//                    
//                    self.title = "Profile"
//                    
//                }
//            
//            
//        }
//    }
//}
//
//struct MainView_Previews: PreviewProvider {
//    static var previews: some View {
//        BusinessBottomTabsView()
//    }
//}
//
//
//
//
//// MARK: Bottom Tabs Utils ... .
//
//
