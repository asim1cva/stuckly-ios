//
//  NotificationsListView.swift
//  StuckApp
//
//  Created by apple on 13/05/2022.
//

import SwiftUI

struct FutureNotificationView: View {
    
    @EnvironmentObject var customerViewModel : BusinessTabsViewModel

    var body: some View {
        
        
        ScrollView(.vertical, showsIndicators: false) {
            
            VStack(alignment: .leading) {
                
                if customerViewModel.futureNotificationsList.isEmpty {
                    Text("No Notifications Here")
                        .foregroundColor(.gray)
                    
                } else {
                    
                    
                    VStack(alignment: .leading) {
                        
                        ForEach(customerViewModel.futureNotificationsList, id: \.id) { item in
                            

                            NotificationViewCell(notificationData:  item)
//                                .onAppear {
//
//                                    if item.isOpen ?? false {
//
//                                        customerViewModel.newNotificationsList.append(customerViewModel.newNotificationsList.count + 1)
//
//                                    }
////                                    print("timezone: \(localTimeZoneIdentifier)")
//
//                                }

                           
                        }
                    }
                    
                }
            }
            .padding([.leading, .trailing])
            .background(RoundedCorners(color: .white, tl: 24, tr: 46, bl: 0, br: 0))
            .padding(.bottom, 80)
            .padding(.top, 40)

         
        }
    }
}

struct NotificationsListView_Previews: PreviewProvider {
    static var previews: some View {
        FutureNotificationView()
    }
}
