//
//  BusinessDashBoardView.swift
//  StuckApp
//
//  Created by Asim on 1/20/22.
//

import SwiftUI
import Stripe
import Alamofire



struct BusinessDashBoardView: View {
    
    
    // MARK: Properties
    
    
    
    //    @State private var title = "Business"
    @State var logoutAlertToggle = false
    @State var selectedIndex: Int = 0
    
    @StateObject var businessViewModel = BusinessTabsViewModel()
    @StateObject var customerViewModel = CustomerViewModel()

    @StateObject var authVM = AuthViewModel()
    
    // for monthly subscription
    @State var showAlertDialog = false
    @State var freeMonthEnded  = false
    @State var subscribedAPlan = false
    
    @StateObject var subscriptionVM = SubscriptionVM()
    
    @ObservedObject var appState = AppState.shared //<-- note this
    @State var navigate = false
    
    var pushNavigationBinding : Binding<Bool> {
        .init { () -> Bool in
            appState.pageToNavigationTo != nil
        } set: { (newValue) in
            if !newValue { appState.pageToNavigationTo = nil }
        }
    }
    
    var body: some View {
        
        VStack {
            
            if subscriptionVM.firstMonthPlanAvailabe {
                //allow user to use App
                LoadingView(isShowing: .constant(businessViewModel.loading)) {
                    
                    VStack {
                        
                        
                        
                        
                        VStack {
                            NavigationLink(destination: SelectionView()
                                            .navigationBarHidden(true)
                                            .navigationBarBackButtonHidden(true),
                                           isActive: $authVM.signOutToggle){}
                                           .hidden()
                            
                            
                            
                            ZStack {
                                
                                CustomTabView(tabs: BusinessTabType.allCases.map({ $0.tabItem }),
                                              selectedIndex: $selectedIndex) { index in
                                    
                                    let type = BusinessTabType(rawValue: index) ?? .home
                                    
                                    getTabView(type: type)
                                }
                            }
                            .padding(.vertical, 1)
                            
                            
                            
                        }
                        
                        
                        
                    }
                    
                    .ignoresSafeArea()
                    .navigationBarTitleDisplayMode(.inline)
                    .edgesIgnoringSafeArea(.bottom)
                }
                
            }
            else {
                
                // else hide the functionality...
                NavigationView {
                    
                    VStack {
                        
                        Spacer()
                        
                        Image("subscription")
                            .resizable()
                            .scaledToFit()
                            .frame(width: deviceWidth-32, height: 200)
                        
                        Spacer()
                        
                        Text("Trial has Ended!")
                            .tracking(1)
                            .font(Nunito.bold.font(size: CustomFontsSize.medium))
                            .bold()
                            .padding()
                        
                        Text("Your free trial has ended. Subscribe to continue.")
                            .font(Nunito.regular.font(size: CustomFontsSize.regular))
                        
                            .multilineTextAlignment(.center)
                        
                        Spacer()
                        
                        NavigationLink {
                            
                            SubscriptionView()
                            
                        } label: {
                            
                            
                            CustomButton(text: "Subsribe Now - 30$")
                            
                            //                        Text("Subsribe Now")
                            //                            .font(.body)
                            //                            .bold()
                            //                            .padding(.vertical, 12)
                            //                            .padding(.horizontal)
                            //                            .background(Color.yellow)
                            //                            .cornerRadius(10)
                            
                        }
                        
                        Spacer()
                        
                    }
                    .padding()
                    
                    .navigationBarHidden(true)
                }
                
            }
            
        }
        .onAppear {
            
          
            subscriptionVM.validSubscription()
            
            businessViewModel.alreadyLoginApiRequest()
            
            
        }
        .overlay(NavigationLink(destination: Dest(message: appState.pageToNavigationTo ?? ""),
                                isActive: pushNavigationBinding) {
            EmptyView()
        })
        
    }
    
    
    
    
    // MARK: getTabView
    @ViewBuilder
    func getTabView(type: BusinessTabType) -> some View {
        
        switch type {
            
        case .home:
            
            HomeView()
                .environmentObject(businessViewModel)
            
        case .notification:
            
                
            NotificationView()
                    .environmentObject(customerViewModel)
        case .profile:
            
            ProfileView()
                .environmentObject(authVM)
            
        case .create:
            
            CreateView()
            
            
            
        }
    }
}

//struct Dest : View {
//    var message : String
//    var body: some View {
//        Text("\(message)")
//    }
//}

struct BusinessDashBoardView_Previews: PreviewProvider {
    static var previews: some View {
        BusinessDashBoardView()
    }
}




