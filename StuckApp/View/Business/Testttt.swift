//
//  Testttt.swift
//  StuckApp
//
//  Created by Muhammad Salman on 2/11/22.
//

import SwiftUI

struct Testttt: View {
    
    @StateObject var subscriptionVM = SubscriptionVM2()
    
    // for monthly subscription
    @State var showAlertDialog = false
    @State var freeMonthEnded  = false
    @State var subscribedAPlan = false
    
    
    
    
    var body: some View {
        VStack {
            
            Spacer()
            
            Text("\(subscriptionVM.startDate)")
            Text("\(subscriptionVM.endDate)")
            
            Text(subscriptionVM.planStatus)

            Spacer()
            
            Button {
                
                subscriptionVM.subscribeNow()
                
            } label: {
                Text("Reset")
                    .padding()
                    .background(Color.yellow)
                    .cornerRadius(10) }
            
            Button { subscriptionVM.validSubscription() } label: {
                Text("Referesh")
                    .padding()
                    .background(Color.yellow)
                    .cornerRadius(10) }
            
            Spacer()
            
        }
        .font(.title)
        .onAppear() {
            subscriptionVM.validSubscription()
        }
        
    }
}

struct Testttt_Previews: PreviewProvider {
    static var previews: some View {
        Testttt()
    }
}

class SubscriptionVM2: ObservableObject {
    
    // starting and ending dates for subscription plan....
    @Published var firstMonthPlanAvailabe = false
    
    @Published var myCurrentDate = ""
    @Published var startDate = ""
    @Published var endDate = ""
    @Published var planStatus = ""
    
    // userDefaultsKey....
    let startingDateKey = "startingDate"
    
    func validSubscription() {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss" //"yyyy-MM-dd HH:mm:ss"

        let myString = formatter.string(from: Date())
        self.startDate = "Start: " + myString
        let yourDate = formatter.date(from: myString)
        
//        UserDefaults.standard.set(nil, forKey: startingDateKey)
        let start = UserDefaults.standard.object(forKey: startingDateKey) as? Date ?? nil
        
        
        if start == nil {
            
            UserDefaults.standard.set(yourDate, forKey: startingDateKey)
            planStatus = "Was empty Start date Added"
            self.firstMonthPlanAvailabe = true
            
            let startNew = UserDefaults.standard.object(forKey: startingDateKey) as? Date ?? nil
            let calendar = Calendar.current
            let nextDate = calendar.date(byAdding: .second, value: 30, to: startNew!)!
            let myStringDate = formatter.string(from: nextDate)
            self.endDate = "End: " + myStringDate
            
        } else {
            
            let calendar = Calendar.current
            let nextDate = calendar.date(byAdding: .second, value: 30, to: start!)!
            
            let current = Date()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let currentString = formatter.string(from: current)
            let currentDate = formatter.date(from: currentString)
            
            if currentDate! <= nextDate {
                planStatus = "Valid"
                self.firstMonthPlanAvailabe = true
                
            } else if currentDate! > nextDate {
                planStatus = "Expired"
                self.firstMonthPlanAvailabe = false
                
            } else {
                planStatus = "Unknown"
                self.firstMonthPlanAvailabe = false
                
            }
            
            let myStringDate = formatter.string(from: nextDate)
            self.endDate = "End: " + myStringDate
            
        }
        

        
        
    }
    
    
    func subscribeNow() {
        UserDefaults.standard.set(nil, forKey: startingDateKey)
        self.validSubscription() //MARK: IMPORTANT
    }
    
    
    
}
