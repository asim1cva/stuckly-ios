//
//  CreateView.swift
//  StuckApp
//
//  Created by Asim on 16/12/2021.
//

import SwiftUI

struct CreateView: View {

    
    @State private var image = UIImage()
    @State private var showSheet = false
    
    @StateObject var businessViewModel = BusinessTabsViewModel()
    
    @State var showActionSheet = false
    @State var showImagePicker = false
    
    @State var sourceType : UIImagePickerController.SourceType = .camera
    
    @State var pickerImage =  UIImage()

    var body: some View {
        
        
        LoadingView(isShowing: .constant(businessViewModel.loading)) {
            
            VStack {
                
                ZStack {
                    Image("topBack")
                        .resizable()
                        .frame(width: deviceWidth, height: deviceHeight/5)
                        .ignoresSafeArea()
                    
                    
                    
                    Text("Add a Business")
                    
                        .font(Nunito.bold.font(size: CustomFontsSize.medium))
                        .foregroundColor(.white)
                    
                    
                  
                    
                   
                }

                VStack {
                    ScrollView(.vertical, showsIndicators: false) {
                        VStack(alignment: .leading) {
                            
                            Text("Fill this to create business")
                                .font(Nunito.bold.font(size: CustomFontsSize.medium))

                                .padding(.bottom)
                            
                            HStack {
                                
                                
                                TextField("Business Name", text: $businessViewModel.businessName)
                                    .autocapitalization(.none)
                                
                            }
                            .padding()
                            
                            .frame(maxWidth: .infinity)
                            
                            .font(Nunito.regular.font(size: CustomFontsSize.regular))
                            .background(
                                RoundedRectangle(cornerRadius: buttonCornerRadius, style: .continuous)
                                    .stroke(bgColor ,lineWidth: 0.4)

                                            .background(Color.white)
                            )
                            
                            
                            
                            
                            HStack {
                                
                                
                                CustomTextEditor.init(placeholder: "Enter Description", text: $businessViewModel.businessDesc)
    //                                .font(Nunito.regular.font(size: CustomFontsSize.regular))
    //                                .frame(width: .infinity, height: messageTextFiledHeight)
    //                                .background(
    //                                    RoundedRectangle(cornerRadius: buttonCornerRadius, style: .continuous)
    //                                        .stroke(bgColor ,lineWidth: 0.5)
    //
    //                                                .background(Color.white)
    //                                )
    //                                .padding(.top)
                                
                                
    //                            TextField("Enter Description", text: $businessViewModel.businessDesc)
    //                                .autocapitalization(.none)
                                
                            }
    //                        .padding()
                            
                            .padding(.leading, 6)

                            
                            .font(Nunito.regular.font(size: CustomFontsSize.regular))
                            .frame(width: .infinity, height: messageTextFiledHeight)
                            .background(
                                RoundedRectangle(cornerRadius: buttonCornerRadius, style: .continuous)
                                    .stroke(bgColor ,lineWidth: 0.4)

                                            .background(Color.white)
                            )
                            .padding(.top)
                            
                            
                            //MARK: Phone Number TextField.......
                            
                            
                            HStack {
                                
                                
                                TextField("Website Link", text: $businessViewModel.businessWebsiteLink)
                                    .autocapitalization(.none)
                                
                            }
                            .padding()
                            
                            .frame(maxWidth: .infinity)
                            
                            .font(Nunito.regular.font(size: CustomFontsSize.regular))
                            .background(
                                RoundedRectangle(cornerRadius: buttonCornerRadius, style: .continuous)
                                    .stroke(bgColor ,lineWidth: 0.4)

                                            .background(Color.white)
                            )
                            .padding(.top)

                            
                            
                            HStack {
                                
                                Text("Upload Images")
                                    .font(Nunito.bold.font(size: CustomFontsSize.regular))

                                    .padding(.top, 30)
                                
                                Spacer()
                            }
                            
                            
                            
                            VStack {
                               
                                
                                Button(action: {
                                    
                                    if businessViewModel.uploadImagesList.count == 5 {
                                        businessViewModel.alertItem = AlertContext.errorMessage(title: "Failed", message: "Max limit of uploaded images is 5")
                                        
                                    } else {
                                        self.showActionSheet.toggle()
                                        withAnimation {
                                        }
                                    }
                                    
                                    
    //                                showSheet.toggle()
                                },
                                       label: {
                                    
                                    ZStack{
                                        Rectangle()
                                        
                                            .stroke(style: StrokeStyle(lineWidth: 2, dash: [5]))
                                            .frame(height: 120)
                                            .foregroundColor(bgColor)
                                        Image("upload")

                                    }
                                })
                                    .actionSheet(isPresented: $showActionSheet) {
                                        ActionSheet(title: Text("Add a photo from"), message: nil, buttons: [
                                            //Button1
                                            
                                            .default(Text("Camera"), action: {
                                                self.showImagePicker = true
                                                self.sourceType = .camera
                                            }),
                                            //Button2
                                            .default(Text("Photo Library"), action: {
                                                self.showImagePicker = true
                                                self.sourceType = .photoLibrary
                                            }),
                                            
                                            //Button3
                                            .cancel()
                                            
                                        ])
                                    }
                                    .sheet(isPresented: $showImagePicker) {
                                        
                                        ImagePicker(avatarImage: $pickerImage, sourceType: self.sourceType)
                                            .onDisappear {
                                                businessViewModel.uploadImagesList.append(pickerImage)
                                                
                                            }
                                    }
                                
                                ImagesViewList()
                                    .padding()
                                
                                
                            }
                            .padding(.top)
                            
                            
                            
                            
                            CustomButton(text: "Confirm")
                                .onTapGesture {
                                    
                                    businessViewModel.createBusinessAPiRequest()
                                }
                                .alert(item: $businessViewModel.alertItem) { alertItem in
                                    Alert(title: alertItem.title,
                                          message: alertItem.message,
                                          dismissButton: alertItem.dismissButton)
                                }
                                .padding(.bottom, 40)
                            
                        }
                        .padding([.leading, .trailing])
                        .background(RoundedCorners(color: .white, tl: 24, tr: 46, bl: 0, br: 0))
                        .padding(.bottom, 80)
                        .padding(.top, 40)
                        
                        
                    }

                }
                .frame(maxWidth: deviceWidth)

                .background(RoundedCorners(color: .white, tl: 24, tr: 46, bl: 0, br: 0))
                .padding(.top, -49)

                
            }
            .navigationBarHidden(true)
            .ignoresSafeArea()
            .navigationBarTitleDisplayMode(.inline)
            .edgesIgnoringSafeArea(.bottom)
            

        }

        
    }
    
    // MARK: ImagesViewList
    
    @ViewBuilder
    func ImagesViewList() -> some View {
        
        
        ScrollView(.horizontal, showsIndicators: false) {
            
            HStack {
                
                ForEach(Array(businessViewModel.uploadImagesList.enumerated().reversed()), id: \.offset)  { index, item in
                    
                    ZStack {
                        
                        Image(uiImage: item)
                            .resizable()
                            .frame(width: 100, height: 100)
                            .cornerRadius(8)
                        
                        Button(action: {
                            
                            businessViewModel.uploadImagesList.remove(at: index)
                            
                            
                            
                        }, label: {
                            Image(systemName: "xmark.circle.fill")
                                .resizable()
                                .frame(width: 30, height: 30)
                                .foregroundColor(buttonBgColor)
                        })
                        
                    }
                }
                .frame(width: 100, height: 100)
                .padding(.top)
                .padding(.bottom)
            }
            
        }
    }
}

struct CreateView_Previews: PreviewProvider {
    static var previews: some View {
        CreateView()
    }
}
