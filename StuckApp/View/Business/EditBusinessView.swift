//
//  EditBusinessView.swift
//  StuckApp
//
//  Created by Asim on 1/20/22.
//

import SwiftUI

struct EditBusinessView: View {
    
    @State private var image = UIImage()
    @State private var showSheet = false
    
    @StateObject var businessViewModel = BusinessTabsViewModel()
    
    @State var showActionSheet = false
    @State var showImagePicker = false
    
    @State var sourceType : UIImagePickerController.SourceType = .camera
    
    @State var pickerImage =  UIImage()
    
    var editBuisnessData: BusinessListData? = nil

    @State private var editApiToggle = false

    
    var body: some View {
        
        LoadingView(isShowing: .constant(businessViewModel.loading)) {
            
            VStack {
                ZStack {
                    Image("topBack")
                        .resizable()
                        .frame(width: deviceWidth, height: deviceHeight/5)
                        .ignoresSafeArea()
                    
                    
                    
                    Text("Edit Details")
                    
                        .font(Nunito.bold.font(size: CustomFontsSize.medium))
                        .foregroundColor(.white)
                }
                
                VStack(alignment: .leading, spacing: 16) {
                    
                    ScrollView(showsIndicators: false) {
                        
                        VStack {
                            
                            NavigationLink(
                                destination: BusinessDashBoardView().navigationBarHidden(true).navigationBarBackButtonHidden(true),
                                isActive: $editApiToggle){}
                                .hidden()
                            
                            HStack {
                                
                                
                                TextField("Business Name", text: $businessViewModel.businessName)
                                    .autocapitalization(.none)
                                
                            }
                            .padding()
                            
                            
                            .font(Nunito.regular.font(size: CustomFontsSize.regular))
                            .background(
                                RoundedRectangle(cornerRadius: buttonCornerRadius, style: .continuous)
                                    .stroke(bgColor ,lineWidth: 0.4)

                                            .background(Color.white)
                            )
                            .padding(.top, 40)
                            
                            
                            HStack {
                                
                                CustomTextEditor.init(placeholder: "Enter Description", text: $businessViewModel.businessDesc)

    //                                TextField("Enter Description", text: $businessViewModel.businessDesc)
    //                                    .autocapitalization(.none)
                                
                            }
                            .padding(.leading, 6)

                            
                            .font(Nunito.regular.font(size: CustomFontsSize.regular))
                            .frame(width: .infinity, height: messageTextFiledHeight)
                            .background(
                                RoundedRectangle(cornerRadius: buttonCornerRadius, style: .continuous)
                                    .stroke(bgColor ,lineWidth: 0.4)

                                            .background(Color.white)
                            )
                            .padding(.top)
                            
                            
                            
                            HStack {
                                
                                
                                TextField("Website Link", text: $businessViewModel.businessWebsiteLink)
                                    .autocapitalization(.none)
                                
                            }
                            .padding()
                            
                            .frame(maxWidth: .infinity)
                            
                            .font(Nunito.regular.font(size: CustomFontsSize.regular))
                            .background(
                                RoundedRectangle(cornerRadius: buttonCornerRadius, style: .continuous)
                                    .stroke(bgColor ,lineWidth: 0.4)

                                            .background(Color.white)
                            )
                            .padding(.top)
                            
                            
                            HStack {
                                
                                Text("Upload Images")
                                    .font(Nunito.bold.font(size: CustomFontsSize.regular))

                                    .padding(.top, 30)
                                
                                Spacer()
                            }
                            
                            
                            VStack {
                               
                                
                                Button(action: {
                                    
                                    if businessViewModel.uploadImagesList.count == 5 {
                                        businessViewModel.alertItem = AlertContext.errorMessage(title: "Failed", message: "Max limit of uploaded images is 5")
                                        
                                    } else {
                                        self.showActionSheet.toggle()
                                        withAnimation {
                                        }
                                    }
                                    
                                    
    //                                showSheet.toggle()
                                },
                                       label: {
                                    
                                    ZStack{
                                        Rectangle()
                                        
                                            .stroke(style: StrokeStyle(lineWidth: 2, dash: [5]))
                                            .frame(height: 120)
                                            .foregroundColor(bgColor)
                                        Image("upload")

                                    }
                                })
                                    .actionSheet(isPresented: $showActionSheet) {
                                        ActionSheet(title: Text("Add a photo from"), message: nil, buttons: [
                                            //Button1
                                            
                                            .default(Text("Camera"), action: {
                                                self.showImagePicker = true
                                                self.sourceType = .camera
                                            }),
                                            //Button2
                                            .default(Text("Photo Library"), action: {
                                                self.showImagePicker = true
                                                self.sourceType = .photoLibrary
                                            }),
                                            
                                            //Button3
                                            .cancel()
                                            
                                        ])
                                    }
                                    .sheet(isPresented: $showImagePicker) {
                                        
                                        ImagePicker(avatarImage: $pickerImage, sourceType: self.sourceType)
                                            .onDisappear {
                                                businessViewModel.uploadImagesList.append(pickerImage)
                                                
                                            }
                                    }
                                
                                ImagesViewList()
                                    .padding()
                                
                                
                            }
                            .padding(.top)
                            
                            
                            CustomButton(text: "Update")
                                .onTapGesture {
                                    
                                    businessViewModel.updateBusinessApiRequest(businessId: self.editBuisnessData?.id ?? 0)
                                }
                                .alert(item: $businessViewModel.alertItem) { alertItem in
                                    Alert(title: alertItem.title,
                                          message: alertItem.message,
                                          dismissButton: .default(Text("OK"), action: {
                                        
                                        if businessViewModel.updateBusinessApiRequestToggle {
                                            
                                            editApiToggle.toggle()

//                                            presentationMode.wrappedValue.dismiss()
                                            
                                            
                                        }
                                        
                                    }))
                                    
                                }
                                .padding(.bottom, 40)
                            
                        }
                        .padding([.leading, .trailing])
                        .background(RoundedCorners(color: .white, tl: 24, tr: 46, bl: 0, br: 0))
                        .padding(.bottom, 80)
                        
                    }

                }
                .background(RoundedCorners(color: .white, tl: 24, tr: 46, bl: 0, br: 0))
                .padding(.top, -49)
    //                .onAppear {
    //                    businessViewModel.uploadImagesList.removeAll()
    //
    //                   editJobImages()
    //                }
            }
            
            .ignoresSafeArea()
            .navigationBarTitleDisplayMode(.inline)
            .edgesIgnoringSafeArea(.bottom)
            .onAppear {
                
                businessViewModel.businessName = editBuisnessData?.name ?? ""
                businessViewModel.businessDesc = editBuisnessData?.datumDescription ?? ""
                businessViewModel.businessWebsiteLink = editBuisnessData?.websiteLink ?? ""
                
                businessViewModel.uploadImagesList.removeAll()
                editJobImages()
            }

        }

        
        
    }
    
    // MARK: ImagesViewList
    
    @ViewBuilder
    func ImagesViewList() -> some View {
        
        
        ScrollView(.horizontal, showsIndicators: false) {
            
            HStack {
                
                ForEach(Array(businessViewModel.uploadImagesList.enumerated().reversed()), id: \.offset)  { index, item in
                    
                    ZStack {
                        
                        Image(uiImage: item)
                            .resizable()
                            .frame(width: 100, height: 100)
                            .cornerRadius(8)
                        
                        Button(action: {
                            
                            businessViewModel.uploadImagesList.remove(at: index)
                            
                            
                            
                        }, label: {
                            Image(systemName: "xmark.circle.fill")
                                .resizable()
                                .frame(width: 30, height: 30)
                                .foregroundColor(appColor)
                        })
                        
                    }
                }
                .frame(width: 100, height: 100)
                .padding(.top)
                .padding(.bottom)
            }
            
        }
    }
    
    //MARK: editJobImages
    
    private func editJobImages() {
        
        DispatchQueue.global(qos: .background).async {
            
            do {
                
                
                for imageUrl in editBuisnessData?.images ?? [] {
                      
                    print(imageUrl)
                    
                    let imageData = try Data.init(contentsOf: URL.init(string: imageUrl.imageURL ?? "")!)
                    
                    DispatchQueue.main.async { businessViewModel.uploadImagesList.append(UIImage(data: imageData)!) }
                }
                
                
//                let image1 = try Data.init(contentsOf: URL.init(string: editJobData?.image1 ?? "")!)
//
//                DispatchQueue.main.async { businessViewModel.uploadImagesList.append(UIImage(data: image1)!) }
//
//                if editJobData?.image2 != "" {
//                    let image = try Data.init(contentsOf: URL.init(string: editJobData?.image2 ?? "")!)
//                    DispatchQueue.main.async { customerHomeVM.uploadImagesList.append(UIImage(data: image)!) }
//                }
//
//                if editJobData?.image3 != "" {
//                    let image = try Data.init(contentsOf: URL.init(string: editJobData?.image3 ?? "")!)
//                    DispatchQueue.main.async { customerHomeVM.uploadImagesList.append(UIImage(data: image)!) }
//
//                }
//
//                if editJobData?.image4 != "" {
//                    let image = try Data.init(contentsOf: URL.init(string: editJobData?.image4 ?? "")!)
//                    DispatchQueue.main.async { customerHomeVM.uploadImagesList.append(UIImage(data: image)!) }
//
//                }
//
//                if editJobData?.image5 != "" {
//                    let image = try Data.init(contentsOf: URL.init(string: editJobData?.image5 ?? "")!)
//                    DispatchQueue.main.async { customerHomeVM.uploadImagesList.append(UIImage(data: image)!) }
//
//                }
            } catch {
                // error
                //                return image
                
            }
        }
    }
}

struct EditBusinessView_Previews: PreviewProvider {
    static var previews: some View {
        EditBusinessView()
    }
}
