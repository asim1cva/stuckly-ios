//
//  HomeView.swift
//  StuckApp
//
//  Created by Asim on 16/12/2021.
//

import SwiftUI

struct HomeView: View {
    
    // MARK: Properties ...................
    
    @EnvironmentObject var businessViewModel: BusinessTabsViewModel
    @EnvironmentObject var customerViewModel: CustomerViewModel
    
     private var title = "Browse Businesses"

    // MARK: Body ...................
    
    var body: some View {
        
       
        
        
        VStack {
            
            
            ZStack {
                Image("topBack")
                    .resizable()
                    .frame(width: deviceWidth, height: deviceHeight/5)
                    .ignoresSafeArea()
                
                
                
               
                
                if !businessViewModel.searchingisActive {
                    
                    Text(Global.shared.currentLoginUser.role == UserType.customer ? title.capitalized : "my businesses".capitalized)
                    
                        .font(Nunito.bold.font(size: CustomFontsSize.medium))
                        .foregroundColor(.white)
                    
                    HStack {
                        Spacer()
                        Image(systemName: "magnifyingglass")
                            .resizable()
                            .frame(width: 24, height: 24)
                            
                            .foregroundColor( .white)
                    }
                    .padding(.trailing)
                    
                    .onTapGesture {
                        withAnimation { businessViewModel.searchingisActive = true }

                    }
                    
                }
                
                if businessViewModel.searchingisActive {
                    
                    
                    if businessViewModel.searchingisActive {
                        SearchingView(searchingFor: $businessViewModel.searchingFor,
                                      searchingisActive: $businessViewModel.searchingisActive)
                    }
                    
                    
                }
                
               
            }

            ZStack{
                
            }
            .alert(item: $businessViewModel.alertItem) { alertItem in
                Alert(title: alertItem.title,
                      message: alertItem.message,
                      dismissButton: alertItem.dismissButton)
            }
            
            VStack {
                ScrollView(.vertical, showsIndicators: false) {
                    
                    VStack(alignment: .leading) {
                        
                    
                        
                        ForEach(businessViewModel.filterUserBusinessList, id: \.id) { item in
                            
                           BusinessViewCell(businessData: item)
                               
                        }

                    }
                    .padding([.leading, .trailing])
                    .background(RoundedCorners(color: .white, tl: 24, tr: 46, bl: 0, br: 0))
                    .padding(.bottom, 80)
                    .padding(.top, 40)
                    


                    
                }

            }
            .frame(maxWidth: deviceWidth)

            .background(RoundedCorners(color: .white, tl: 24, tr: 46, bl: 0, br: 0))
            .padding(.top, -49)
          
            
        }
        .onAppear {
            
            
            if Global.shared.currentLoginUser.role == UserType.customer {
                
                businessViewModel.getBusinessListApiRequest()
                
            } else {
                
                businessViewModel.getUsersBusinessListApiRequest()

            }
            

            
            
            
        }
        .ignoresSafeArea()
        .navigationBarTitleDisplayMode(.inline)
        .edgesIgnoringSafeArea(.bottom)
        

        
        
        
    }
    
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
