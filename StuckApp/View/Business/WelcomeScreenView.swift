//
//  WelcomeScreenView.swift
//  StuckApp
//
//  Created by AR Computers on 16/12/2021.
//

import SwiftUI

struct WelcomeScreenView: View {
    
    @State var loginToggle = false
    
    
    //MARK: Body
    var body: some View {
        
        VStack {
            
            ZStack {
                
                Rectangle()
                    .fill(Color.white)
                
                VStack {
                    
                    Group{
                        HStack {
                            
                            Text("Find Businesses")
                                .foregroundColor(Color("primary"))
                                .font(Nunito.bold.font(size: CustomFontsSize.medium))
                            
                            
                            
                            Text("TO")
                                .padding(.leading, -1)
                                .font(Nunito.semiBold.font(size: CustomFontsSize.regular))
                            
                            
                        }
                        .padding(.top, deviceHeight > 700 ? 120 : 120)
                        
                        Text("To Stay Connected")
                            .font(Nunito.bold.font(size: CustomFontsSize.regular))
                        
                    }
                    .textCase(.uppercase)
                    .foregroundColor(Color("textGrayColor"))
                    
                    
                    
                    VStack {
                        
                        Text("Stuckly will instantly notify you of everything the business wants you to be notified of. Stay in the know with Stuckly")
                            .multilineTextAlignment(.center)
                            .lineLimit(5)
                            .font(Nunito.regular.font(size: CustomFontsSize.small))
                            .frame(width: deviceWidth, height: .infinity)
                        
                        
                        // Text("")
                        
                    }
                    .foregroundColor(.gray)
                    .padding(.top, 2)
                    .padding(.horizontal)
                    
                    
                }
            }
            .frame(maxWidth: .infinity, maxHeight: 350)
            
            
            //MARK: Background Image.................
            
            ZStack {
                
                Spacer()
                Image("welcome")
                    .resizable()
                    .scaledToFill()
                    .frame(height: deviceHeight > 700 ? 700: 400)
                // MARK: Blur View
                
                Rectangle()
                    .background(.thinMaterial)
                    .opacity(0.25)
                    .ignoresSafeArea()
                    .frame(height: deviceHeight > 700 ? 700: 400)
                
                VStack {
                    
                    //MARK: Proceed Button........
                    
                    Spacer()
                    
                    HStack {
                        
                        NavigationLink("", destination: SelectionView().navigationBarHidden(true),
                                       isActive: $loginToggle)
                        
                        Button(action: {
                            
                            loginToggle.toggle()
                            
                        }, label: {
                            Image("proceedImage")
                                .resizable()
                                .frame(width: deviceHeight > 700 ? 60 : 50, height: deviceHeight > 700 ? 60 : 50)
                        })
                    }
                    .padding(.bottom, deviceHeight > 700 ? 180: 100)
                    
                }
            }
            
            
        }
        .navigationBarHidden(true)
    }
}

struct WelcomeScreenView_Previews: PreviewProvider {
    static var previews: some View {
        WelcomeScreenView()
    }
}
