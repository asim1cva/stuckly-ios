//
//  ReleaseDateView.swift
//  StuckApp
//
//  Created by Asim on 16/12/2021.
//

import SwiftUI



struct ReleaseDateView: View {
    
    
    
    @EnvironmentObject var businessViewModel: BusinessTabsViewModel
    
    @Environment(\.presentationMode) var presentationMode
    
    @State var showGraphical: Bool = false
    
    var dateFormatter: DateFormatter {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        return formatter
    }
    
    @State var showActionSheet = false
    @State var showImagePicker = false
    
    @State var sourceType : UIImagePickerController.SourceType = .camera
    
    @State var pickerImage =  UIImage()
    
    var body: some View {
        
        LoadingView(isShowing: .constant(businessViewModel.loading)) {
            
            VStack {
                VStack {
                    HStack{
                        Text("Cancel")
                            .font(Nunito.bold.font(size: CustomFontsSize.regular))
                            .foregroundColor(.white)
                            .onTapGesture {
                                presentationMode.wrappedValue.dismiss()
                                
                            }
                        Spacer()
                    }
                    
                    
                    Text(businessViewModel.businessData?.releaseDate != nil ? "Change Event Date" : "Setup Event Date")
                    
                        .font(Nunito.bold.font(size: CustomFontsSize.medium))
                        .foregroundColor(.white)
                }
                
                .padding()
                .background(appColor)
                
                
                VStack(alignment: .leading, spacing: 16) {
                    ScrollView(showsIndicators: false, content: {
                        VStack {
                            
                            ZStack {
                                
                                
                                
                                
                                VStack(alignment: .leading) {
                                    HStack {
                                        
                                        Text("Select Date:")
                                            .font(Nunito.bold.font(size: CustomFontsSize.regular))
                                        Spacer()
                                        
                                    }
                                    .padding(.top)
                                    
                                    
                                    //MARK: WeekDays..............
                                    
                                    HStack {
                                        Image("calender")
                                            .foregroundColor(appColor)
                                        //                                            .padding(.leading)
                                        
                                        Text("\(businessViewModel.releaseDate, formatter: dateFormatter)")
                                        Spacer()
                                        
                                        //                        DatePicker(selection: $customerHomeVM.deliveryDate, in: Date()... ,displayedComponents: .date) {
                                        //                            Text("Delivery Date")
                                        //                        }
                                        //                        .labelsHidden()
                                        //                        .font(.body)
                                        
                                        
                                    }
                                    .onTapGesture {
                                        showGraphical.toggle()
                                        
                                    }
                                    .padding(.vertical)
                                    .font(Nunito.regular.font(size: CustomFontsSize.regular))
                                    .frame(width: .infinity)
                                    .background(
                                        RoundedRectangle(cornerRadius: buttonCornerRadius, style: .continuous)
                                            .stroke(bgColor ,lineWidth: 0.5)
                                        
                                            .background(Color.white)
                                    )
                                    .sheet(isPresented: $showGraphical, content: {
                                        DatePickerSheet(currentDate: $businessViewModel.releaseDate)
                                    })
                                    
                                    //                                    WeekDaysView(selectedDate: $businessViewModel.releaseDate)
                                    
                                    
                                    
                                    
                                    
                                    
                                    //MARK: Write message textfield..........
                                    
                                    
                                    CustomTextEditor.init(placeholder: "Enter Event Name", text: $businessViewModel.message)
                                        .font(Nunito.regular.font(size: CustomFontsSize.regular))
                                        .frame(width: .infinity, height: messageTextFiledHeight)
                                        .background(
                                            RoundedRectangle(cornerRadius: buttonCornerRadius, style: .continuous)
                                                .stroke(bgColor ,lineWidth: 0.5)
                                            
                                                .background(Color.white)
                                        )
                                        .padding(.top)
                                    
                                    
                                    
                                    
                                    //MARK: Details textfield.............
                                    
                                    
                                    
                                    CustomTextEditor.init(placeholder: "Enter Event Details", text: $businessViewModel.detail)
                                        .font(Nunito.regular.font(size: CustomFontsSize.regular))
                                        .frame(width: .infinity, height: messageTextFiledHeight)
                                        .background(
                                            RoundedRectangle(cornerRadius: buttonCornerRadius, style: .continuous)
                                                .stroke(bgColor ,lineWidth: 0.5)
                                            
                                                .background(Color.white)
                                        )
                                        .padding(.top)
                                    
                                    
                                    HStack {
                                        
                                        Text("Upload Event Photos:")
                                            .font(Nunito.bold.font(size: CustomFontsSize.regular))
                                        
                                            .padding(.top)
                                        
                                        Spacer()
                                    }
                                    
                                    VStack {
                                        
                                        
                                        Button(action: {
                                            
                                            if businessViewModel.uploadImagesList.count == 3 {
                                                businessViewModel.alertItem = AlertContext.errorMessage(title: "Failed", message: "Max image limit is 3")
                                                
                                            } else {
                                                self.showActionSheet.toggle()
                                                withAnimation {
                                                }
                                            }
                                            
                                            
                                            //                                showSheet.toggle()
                                        },
                                               label: {
                                            
                                            ZStack{
                                                Rectangle()
                                                
                                                    .stroke(style: StrokeStyle(lineWidth: 2, dash: [5]))
                                                    .frame(height: 120)
                                                    .foregroundColor(bgColor)
                                                Image("upload")
                                                
                                            }
                                        })
                                            .actionSheet(isPresented: $showActionSheet) {
                                                ActionSheet(title: Text("Add a photo from"), message: nil, buttons: [
                                                    //Button1
                                                    
                                                    .default(Text("Camera"), action: {
                                                        self.showImagePicker = true
                                                        self.sourceType = .camera
                                                    }),
                                                    //Button2
                                                    .default(Text("Photo Library"), action: {
                                                        self.showImagePicker = true
                                                        self.sourceType = .photoLibrary
                                                    }),
                                                    
                                                    //Button3
                                                    .cancel()
                                                    
                                                ])
                                            }
                                            .sheet(isPresented: $showImagePicker) {
                                                
                                                ImagePicker(avatarImage: $pickerImage, sourceType: self.sourceType)
                                                    .onDisappear {
                                                        businessViewModel.uploadImagesList.append(pickerImage)
                                                        
                                                    }
                                            }
                                        
                                        ImagesViewList()
                                            .padding()
                                        
                                        
                                    }
                                    .padding(.top)
                                    
                                    
                                    
                                    //MARK: Confirm Button................
                                    
                                    
                                    HStack {
                                        
                                        CustomButton(text: "Confirm")
                                            .onTapGesture {
                                                businessViewModel.setUpReleaseDateApiRequest()
                                            }
                                            .alert(item: $businessViewModel.alertItem) { alertItem in
                                                Alert(title: alertItem.title,
                                                      message: alertItem.message,
                                                      dismissButton: alertItem.dismissButton)
                                            }
                                            .padding(.bottom, 40)
                                        
                                    }
                                    
                                    
                                }
                                .padding(.horizontal)
                                
                            }
                        }
                    })
                    
                }
                
                
            }
            
            
            
            
        }
        
        
        
        
        
        
    }
    
    // MARK: ImagesViewList
    
    @ViewBuilder
    func ImagesViewList() -> some View {
        
        
        ScrollView(.horizontal, showsIndicators: false) {
            
            HStack {
                
                ForEach(Array(businessViewModel.uploadImagesList.enumerated().reversed()), id: \.offset)  { index, item in
                    
                    ZStack {
                        
                        Image(uiImage: item)
                            .resizable()
                            .frame(width: 100, height: 100)
                            .cornerRadius(8)
                        
                        Button(action: {
                            
                            businessViewModel.uploadImagesList.remove(at: index)
                            
                            
                            
                        }, label: {
                            Image(systemName: "xmark.circle.fill")
                                .resizable()
                                .frame(width: 30, height: 30)
                                .foregroundColor(buttonBgColor)
                        })
                        
                    }
                }
                .frame(width: 100, height: 100)
                .padding(.top)
                .padding(.bottom)
            }
            
        }
    }
}

struct MyBackButton: View {
    let label: String
    let closure: () -> ()
    
    var body: some View {
        Button(action: { self.closure() }) {
            HStack {
                Image(systemName: "chevron.left")
                Text(label)
            }
        }
    }
}

struct ReleaseDateView_Previews: PreviewProvider {
    static var previews: some View {
        ReleaseDateView()
    }
}


