//
//  SelectionView.swift
//  StuckApp
//
//  Created by Asim on 17/12/2021.
//

import SwiftUI

struct SelectionView: View {
    
    
    // MARK: Properties

    @State var mainViewToggle: Bool = false
    @State var customerTabViewToggle: Bool = false
    
    @State var userType: UserType = UserType.customer

    @StateObject var customerViewModel = CustomerViewModel()

    @StateObject var businessViewModel = BusinessTabsViewModel()
    
    // MARK: Body
    
    var body: some View {
        
        VStack {
            
            ZStack {
                
                Image("bgImage")
                    .resizable()
                
                // MARK: Image....
                
                VStack {
                    
                    Image("stuck_Image")
                        .resizable()
                        .frame(width: 130, height: 130)
                    Spacer()
                }
                .padding(.top, deviceHeight > 700 ? 50: 10)
                
                
                VStack {
                    
                    VStack {
                        
                        Text("Continue As")
                            .font(Nunito.bold.font(size: CustomFontsSize.medium))
                            .fontWeight(.bold)
                            .padding(.top, deviceHeight > 700 ? 50: 20)
                    }
                    .padding(.bottom, deviceHeight > 700 ? 50: 20)
                    
                    
                    VStack {
                        
                        NavigationLink(
                            destination: LoginView(userType: self.userType)
                                .environmentObject(customerViewModel)
                                .environmentObject(businessViewModel)
                                .navigationBarTitleDisplayMode(.inline),
                            isActive: $mainViewToggle){}
                            .hidden()
                        
                        
//                        NavigationLink("", destination: LoginView().navigationBarTitleDisplayMode(.inline),
//                                       isActive: $mainViewToggle)
                        
                        
                        CustomButton(text: "Business")
                            .onTapGesture {
                                
                                self.userType = UserType.business
                                mainViewToggle.toggle()

                            }

                    }
                    
                    
                    VStack {
                        
                        NavigationLink("", destination: CustomerBottomTabsView()
                                        .environmentObject(customerViewModel)
                                        .environmentObject(businessViewModel)
                                        .navigationBarTitleDisplayMode(.inline),
                                       isActive: $customerTabViewToggle)
                        
                        
                        CustomButton(text: "Customer")
                            .onTapGesture {
                                
                                self.userType = UserType.customer

                                mainViewToggle.toggle()

                            }

                    }
                    
                  
                    
                }
                .padding(.horizontal)
                
            }
            .edgesIgnoringSafeArea(.all)
            
            
        }
        .navigationBarHidden(true)
    }
}

struct SelectionView_Previews: PreviewProvider {
    static var previews: some View {
        SelectionView()
    }
}
