//
//  VerifyEmail.swift
//  StuckApp
//
//  Created by Admin Admin on 2/23/22.
//

import SwiftUI

struct VerifyEmailView: View {
    
    @EnvironmentObject var authVM: AuthViewModel
    @EnvironmentObject var customerViewModel: CustomerViewModel
    
    @EnvironmentObject var businessViewModel: BusinessTabsViewModel

    var userType: UserType
    
    var body: some View {
        
        LoadingView(isShowing: .constant(authVM.loading)) {
            ZStack {
                ScrollView(.vertical, showsIndicators: false) {
                    VStack{
                        
                        ZStack{
                            
                            Image("bgImage")
                                .resizable()
                            
                            // MARK: Image....
                            
                            VStack {
                                
                                Image("stuck_Image")
                                    .resizable()
                                    .frame(width: 130, height: 130)
                                Spacer()
                            }
                            .padding(.top, deviceHeight > 700 ? 50: 40)
                            
                            
                            VStack {
                                
                                VStack {
                                    
                                    VStack {
                                        
                                        Text("Verify your Email")
                                            .font(Nunito.bold.font(size: CustomFontsSize.medium))
                                            .fontWeight(.bold)
                                        
                                        
                                    }
                                    .padding(.top, deviceHeight > 700 ? 150: 100)
                                    
                                    NavigationLink(
                                        destination: BusinessDashBoardView()
                                            .navigationBarHidden(true)
                                            .navigationBarBackButtonHidden(true)
                                            .environmentObject(customerViewModel)
                                            .environmentObject(businessViewModel),
                                        
                                        isActive: $authVM.businessDashboardViewToggle){}
                                        .hidden()
                                    
                                    NavigationLink(
                                        destination: CustomerBottomTabsView()
                                            .navigationBarHidden(true)
                                            .navigationBarBackButtonHidden(true)
                                            .environmentObject(customerViewModel)
                                            .environmentObject(businessViewModel),
                                        isActive: $authVM.customerDashboardViewToggle){}
                                        .hidden()
                                    
                                    
                                    HelperFunctions.highlightText(str: "Code is sent to \(authVM.email)", searched: "\(authVM.email)")
                                        .lineLimit(2)
                                        .padding(.horizontal, 30)
                                        .padding(.top)
                                    
                                    Text("Kindly check your Email or Spam, we have sent a 6 digit code to the following email")
                                        .font(Nunito.semiBold.font(size: CustomFontsSize.small))
                                        .foregroundColor(Color.gray)
                                        .frame(maxWidth: .infinity, alignment: .center)
                                        .padding(.top)
                                    
                                    
                                    CustomTextFieldView(title: "Code", text: $authVM.code, iconName: "", placeHolder: "6 digit code.. .")
                                    
                                    CustomButton(text: "Verify Email")
                                        .onTapGesture {
                                            
                                            if authVM.code.isEmpty {
                                                authVM.alertItem = AlertContext.errorMessage(message: "Please enter 6 OTP.")
                                                
                                            } else if authVM.code.count < 6 {
                                                
                                                authVM.alertItem = AlertContext.errorMessage(message: "Invalid OTP.")
                                                
                                            } else if authVM.verifyEmailCode == HelperFunctions.getIntFromString(str: authVM.code) {
                                                
                                                //                            authVM.alertItem = AlertContext.errorMessage(message: "Code matched")
                                                
                                                authVM.signUpApiRequest(userType: self.userType)
                                                //                                                authVM.newPasswordViewToggle.toggle()
                                            } else {
                                                
                                                authVM.alertItem = AlertContext.errorMessage(message: "Invalid OTP.")
                                                
                                            }
                                            
                                        }
                                        .alert(item: $authVM.alertItem) { alertItem in
                                            Alert(title: alertItem.title, message: alertItem.message, dismissButton: alertItem.dismissButton)
                                        }
                                    
                                    
                                    
                                    
                                }
                                .padding(.horizontal)
                                
                                
                            }
                            .navigationBarTitleDisplayMode(.inline)
                            //                            .navigationBarTitle("Verify Email")
                        }
                    }
                }
                
            }
            .environmentObject(customerViewModel)
        }
        .edgesIgnoringSafeArea(.top)
        
        
        
        
        
    }
}

struct VerifyEmail_Previews: PreviewProvider {
    static var previews: some View {
        VerifyEmailView(userType: UserType.business)
    }
}
