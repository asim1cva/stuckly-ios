//
//  SubscriptionView.swift
//  StuckApp
//
//  Created by Muhammad Salman on 2/14/22.
//

import SwiftUI
import Stripe

struct SubscriptionView: View {
    
    @StateObject var subscriptionVM = SubscriptionVM()
    @State var paymentConfirmationSheetToggle = false
    @State var paymentMethodParams: STPPaymentMethodParams?
    @State var showSheet = false
    
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    
    var body: some View {
        LoadingView(isShowing: .constant(subscriptionVM.loading)) {
        VStack {
            
            
            NavigationLink(
                destination: BusinessDashBoardView()
                    .navigationBarHidden(true)
                    .navigationBarBackButtonHidden(true),
                isActive: $subscriptionVM.businessDashboardViewToggle){}
                .hidden()
            
            
            
            NavigationLink(
                destination: CustomerBottomTabsView()
                    .navigationBarHidden(true)
                    .navigationBarBackButtonHidden(true),
                isActive: $subscriptionVM.customerDashboardViewToggle){}
                .hidden()
            
            
            STPPaymentCardTextField
                .Representable(paymentMethodParams: $paymentMethodParams)
                .padding()
            
            // order placing button and payment sheet....
            VStack {
                if let paymentIntent = subscriptionVM.paymentIntentParams {
                    VStack {
                        
                        CustomButton(text: "Confirm Payment")
                            .onTapGesture {
                                paymentConfirmationSheetToggle = true
                                paymentIntent.paymentMethodParams = self.paymentMethodParams
                                subscriptionVM.loading = true
                            }

//                        Button(action: {
//
//
//
//                        }, label: {
//                            Text("Place Order")
//                        })
//                            .font(.title3)
//                            .foregroundColor(.white)
//                            .frame(width: (deviceWidth-70), height: 20)
//                            .padding()
//                            .background(appColor)
//                            .cornerRadius(10)
                    }
                    .paymentConfirmationSheet(isConfirmingPayment: $paymentConfirmationSheetToggle,
                                              paymentIntentParams: paymentIntent,
                                              onCompletion: subscriptionVM.onCompletion)
                    .onAppear() {
                        subscriptionVM.loading = false
                    }
                    .padding(.horizontal)
                } else {
                    Text("")
                        .onAppear() {
                            subscriptionVM.loading = true
                        }
                }
            }
            
            // cancel button
            VStack {
                
                CustomButton(text: "Cancel")
                    .onTapGesture {
                        mode.wrappedValue.dismiss()
                    }

//                Button(action: {
//                    mode.wrappedValue.dismiss()
//
//                }, label: {
//                    Text("Cancel!")
//                })
//                    .font(.title3)
//                    .foregroundColor(appColor)
//                    .frame(width: (deviceWidth-70), height: 20)
//                    .padding()
//                    .background(Color.yellow)
//                    .cornerRadius(10)
            }
            .padding(.horizontal)

            .padding(.bottom)
            
            
            // payment status checking....
            VStack {
                if let paymentStatus = subscriptionVM.paymentStatus {
                    HStack {
                        switch paymentStatus {
                        case .succeeded:
                            Text("Success")
                                .onAppear() {
                                    paymentConfirmationSheetToggle = false
                                    subscriptionVM.loading = false
//                                    subscriptionVM.alertItem = AlertContext.errorMessage(message: "Payment Success!")
                                    
                                }
                        case .canceled:
                            Text("Calncle")
                                .onAppear() {
                                    paymentConfirmationSheetToggle = false
                                    subscriptionVM.loading = false
                                    subscriptionVM.alertItem = AlertContext.errorMessage(message: "Payment Canceled!")
                                }
                        case .failed:
                            Text("Failed!")
                                .onAppear() {
                                    paymentConfirmationSheetToggle = false
                                    subscriptionVM.loading = false
                                    subscriptionVM.alertItem = AlertContext.errorMessage(message: "Payment failed!")
                                }
                        @unknown default:
                            Text("Unknown")
                                .onAppear() {
                                    paymentConfirmationSheetToggle = false
                                    subscriptionVM.loading = false
                                    subscriptionVM.alertItem = AlertContext.errorMessage(message: "Unknown error occured!")
                                }
                        }
                    }
                }
            }
            .alert(item: $subscriptionVM.alertItem) { item in
                Alert(title: item.message,
                      message: Text(""),
                      dismissButton: .default(Text("Okay")) {
                    if item.message == Text("Payment Success!") {
                        mode.wrappedValue.dismiss()
                    }
                })
            }
            
        }
        .onAppear() {
            subscriptionVM.getPaymentIntent(name: Global.shared.currentLoginUser.userName ?? "",
                                            email: Global.shared.currentLoginUser.email ?? "",
                                            account_owner: Global.shared.currentLoginUser.userName ?? "",
                                            user_id: Global.shared.currentLoginUser.id ?? 0,
                                            amount: "30")
        }
        }
        
    }
    
}

struct SubscriptionView_Previews: PreviewProvider {
    static var previews: some View {
        SubscriptionView()
    }
}
