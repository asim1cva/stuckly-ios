//
//  FollowingTabView.swift
//  StuckApp
//
//  Created by Admin Admin on 2/22/22.
//

import SwiftUI

struct FollowingTabView: View {
    
    
    // MARK: Properties
    
    @EnvironmentObject var customerViewModel: CustomerViewModel
    
    
    // MARK: body

    var body: some View {
        
        VStack {
            
            ZStack {
                Image("topBack")
                    .resizable()
                    .frame(width: deviceWidth, height: deviceHeight/5)
                    .ignoresSafeArea()
                
                
                
                Text("Following")
                
                    .font(Nunito.bold.font(size: CustomFontsSize.medium))
                    .foregroundColor(.white)
                
                
              
                
               
            }

            
           
            VStack {
                ScrollView(.vertical, showsIndicators: false) {
                    
                    VStack(alignment: .leading) {
                        if customerViewModel.followedBusinessList.isEmpty {
                            
                            Text("Find New Businesses To Follow")
                                .foregroundColor(.gray)

                            
                        } else {
                            
                           
                            VStack(alignment: .leading) {
                                
                                ForEach(customerViewModel.followedBusinessList, id: \.id) { item in
                                    
                                    BusinessViewCell(businessData: item)
                                }
                            }
                            

                        }

                    }
                    .padding([.leading, .trailing])
                    .background(RoundedCorners(color: .white, tl: 24, tr: 46, bl: 0, br: 0))
                    .padding(.bottom, 80)
                    .padding(.top, 40)

                   
                    
                }
            }
            .frame(maxWidth: deviceWidth)

            .background(RoundedCorners(color: .white, tl: 24, tr: 46, bl: 0, br: 0))
            .padding(.top, -49)

         
            
        }
        .onAppear {
            
            customerViewModel.getFollowedBusinessListApiRequest()

            
        }
        .ignoresSafeArea()
        .navigationBarTitleDisplayMode(.inline)
        .edgesIgnoringSafeArea(.bottom)
        
    }
}

struct FollowingTabView_Previews: PreviewProvider {
    static var previews: some View {
        FollowingTabView()
    }
}
