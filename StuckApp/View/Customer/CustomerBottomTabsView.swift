//
//  CustomerTabView.swift
//  StuckApp
//
//  Created by Asim on 17/12/2021.
//

import SwiftUI

struct CustomerBottomTabsView: View {
    
    @State private var title = "Browse Businesses"
    @State var selectedIndex: Int = 0
    
    @StateObject var authVM = AuthViewModel()
    
//    @StateObject var customerViewModel = CustomerViewModel()
    
    
    @EnvironmentObject var customerViewModel : CustomerViewModel
    
    @EnvironmentObject var businessViewModel : BusinessTabsViewModel
    
    @ObservedObject var appState = AppState.shared //<-- note this
    @State var navigate = false
    
    @State var searchingFor = ""
    
    
    var pushNavigationBinding : Binding<Bool> {
        .init { () -> Bool in
            appState.pageToNavigationTo != nil
        } set: { (newValue) in
            if !newValue { appState.pageToNavigationTo = nil }
        }
    }
    
    var body: some View {
        
        
        LoadingView(isShowing: .constant(customerViewModel.loading)) {
            
            VStack {
                
              
                
                
                VStack {
                    VStack {
                        
                        
                        ZStack {
                            
                            NavigationLink(destination: SelectionView()
                                            .navigationBarHidden(true)
                                            .navigationBarBackButtonHidden(true),
                                           isActive: $authVM.signOutToggle){}
                                           .hidden()
                            
                            CustomTabView(tabs: CustomerTabType.allCases.map({ $0.tabItem }),
                                          selectedIndex: $selectedIndex) { index in
                                
                                let type = CustomerTabType(rawValue: index) ?? .home
                                
                                getTabView(type: type)
                            }
                        }
                        
                        
                    }
                    
                }
                
                
                
                
            }
            .overlay(NavigationLink(destination: Dest(message: appState.pageToNavigationTo ?? ""),
                                    isActive: pushNavigationBinding) {
                EmptyView()
            })
            .ignoresSafeArea()
            .navigationBarTitleDisplayMode(.inline)
            .edgesIgnoringSafeArea(.bottom)
           
            
            
            
            
        }
        
        
        
        
    }
    
    
    
    @ViewBuilder
    func getTabView(type: CustomerTabType) -> some View {
        
        
        
        if type == .home {
            HomeView()
                .environmentObject(businessViewModel)
                .environmentObject(customerViewModel)
                .onAppear {
                    

                    
                    
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
//
//                        businessViewModel.getNotificationListApiRequest()
//
//                    }
                    
                    
//                    if !customerViewModel.getNotificationListApiRequestToggle {
//                        
//                       
//                    }
                   
                }
                
            
        } else if type == .following {
            FollowingTabView()
                .environmentObject(customerViewModel)
            
        } else if type == .notification {
            NotificationView()
                .environmentObject(customerViewModel)
                
            
        } else if type == .profile {
            ProfileView()
                .environmentObject(authVM)
            
        }
        
        
        //        switch type {
        //        case .home:
        //            HomeView()
        //                .padding(.bottom)
        ////                .onAppear {
        ////                    //                    businessViewModel.getBusinessListApiRequest()
        ////
        ////                    customerViewModel.searchingisActive = false
        ////                    print("HomeView - onAppear")
        ////                    self.title = "Browse Businesses"  // Toolbar Title
        ////
        ////                }
        //            //                .environmentObject(businessViewModel)
        //
        //
        //        case .notification:
        //            NotificationView()
        ////                .onAppear {
        ////                    customerViewModel.searchingisActive = false
        ////
        ////                    print("CreateView - onAppear")
        ////
        ////                    self.title = "Notifications"
        ////
        ////                }
        //                .environmentObject(customerViewModel)
        //
        //        case .profile:
        //            ProfileView()
        ////                .onAppear {
        ////                    customerViewModel.searchingisActive = false
        ////
        ////                    self.title = "Profile"
        ////
        ////                }
        //
        //
        //        case .following:
        //            FollowingTabView()
        ////                .onAppear {
        ////                    customerViewModel.searchingisActive = false
        ////
        ////                    self.title = "Following"
        ////
        ////                }
        //                .environmentObject(customerViewModel)
        //        }
    }
}

struct Dest : View {
    var message : String
    var body: some View {
        Text("\(message)")
    }
}

struct CustomerTabView_Previews: PreviewProvider {
    static var previews: some View {
        CustomerBottomTabsView()
    }
}
