//
//  NotificationView.swift
//  StuckApp
//
//  Created by AR Computers on 17/12/2021.
//

import SwiftUI

struct NotificationView: View {
    
    // MARK: Properties

    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var customerViewModel: BusinessTabsViewModel
    var localTimeZoneIdentifier: String { return TimeZone.current.identifier }

    // MARK: Properties
    @State private var selectedTab: Int = 0

    // MARK: Body

    var body: some View {
        
        VStack {
            
            ZStack {
                Image("topBack")
                    .resizable()
                    .frame(width: deviceWidth, height: deviceHeight/5)
                    .ignoresSafeArea()
                
                
                
                Text(Global.shared.currentLoginUser.role == UserType.customer ?  "Notifications" : "Notifications Scheduled/Sent")
                
                    .font(Nunito.bold.font(size: CustomFontsSize.medium))
                    .foregroundColor(.white)
                
                
               
            }
            
            VStack {
                
                if Global.shared.currentLoginUser.role == UserType.business {
                    
                    TopTabsView
                    
                } else {
                    ScrollView(.vertical, showsIndicators: false) {
                        
                        VStack(alignment: .leading) {
                            
                            if customerViewModel.notificationsList.isEmpty {
                                Text("No Notifications Here")
                                    .foregroundColor(.gray)
                                
                            } else {
                                
                                
                                VStack(alignment: .leading) {
                                    
                                    ForEach(customerViewModel.notificationsList, id: \.id) { item in
                                        
    //                                    "schedule_date": "2022-03-09",
    //                                    "schedule_time": "04:50",

    //                                    "timezone": "Asia/Karachi",


                                //        localTimeZoneIdentifier // "America/Sao_Paulo"

                                        NotificationViewCell(notificationData:  item)
                                            .onAppear {
                                                
                                                if item.isOpen ?? false {
                                                    
                                                    customerViewModel.newNotificationsList.append(customerViewModel.newNotificationsList.count + 1)
                                                
                                                }
                                                print("timezone: \(localTimeZoneIdentifier)")

                                            }

                                       
                                    }
                                }
                                
                            }
                        }
                        .padding([.leading, .trailing])
                        .background(RoundedCorners(color: .white, tl: 24, tr: 46, bl: 0, br: 0))
                        .padding(.bottom, 80)
                        .padding(.top, 40)

                     
                    }
                }
               
               

            }
            .frame(maxWidth: deviceWidth)

            .background(RoundedCorners(color: .white, tl: 24, tr: 46, bl: 0, br: 0))
            .padding(.top, -49)


           
        }
        .onAppear {
            
            customerViewModel.getNotificationListApiRequest()
            
            print("Current Date: \(HelperFunctions.currentDate(dateFormat: "yyyy-MM-dd"))")
            print("Current Time: \(HelperFunctions.currentTime(timeFormat: "HH:mm"))")
            print("Current timeZone: \(localTimeZoneIdentifier)")


        }
        .ignoresSafeArea()
        .navigationBarTitleDisplayMode(.inline)
        .edgesIgnoringSafeArea(.bottom)
        
        
    }
    
    @ViewBuilder
    private var TopTabsView: some View {
        
        let tabs: [Tab] = [
            .init(icon: Image(systemName: ""), title: "Sent"),
            .init(icon: Image(systemName: ""), title: "Scheduled"),
//            .init(icon: Image(systemName: ""), title: "History")
        ]
        
        GeometryReader { geo in
            VStack(spacing: 0) {
                // Tabs
                Tabs(tabs: tabs, geoWidth: geo.size.width, selectedTab: $selectedTab)
                
                
                // Views
                TabView(selection: $selectedTab,
                        content: {
                    
                    PastNotificationView()
                        .environmentObject(customerViewModel)
                    
                        .tag(0)
                    
                    FutureNotificationView()
                        .environmentObject(customerViewModel)
//                    Text("Current")
                    
                        .tag(1)
                    
                })
//                    .overlay(mapsOverlay, alignment: .top)
                
                    .tabViewStyle(PageTabViewStyle(indexDisplayMode: .never))
            }
            
        }
        
        
        
        
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
