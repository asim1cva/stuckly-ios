//
//  CustomerListingView.swift
//  StuckApp
//
//  Created by AR Computers on 17/12/2021.
//

import SwiftUI

struct CustomerListingView: View {
    
    @State var detailViewToggle = false
    
    var body: some View {
        
        VStack {
            ScrollView(.vertical, showsIndicators: false) {
                VStack {
                    ForEach(0..<16, id: \.self) { item in
                        
                        VStack {
                            
//                            NavigationLink("", destination: DetailView().navigationBarHidden(false),
//                                           isActive: $detailViewToggle)
                            
                            Image("image2")
                                .resizable()
                                .scaledToFill()
                                .padding(.top)
                                .padding([.leading, .trailing])
                                .frame(maxHeight: deviceHeight > 700 ? 400: 150)
                            
                            HStack {
                                
                                Text("Real Estate Business")
                                
                                Spacer()
                                
                                Text("2345 Followers")
                                    .foregroundColor(bgColor)
                                
                            }
                            .padding([.leading, .trailing])
                        }
                        .onTapGesture {
                            
                            detailViewToggle.toggle()
                        }
                    }
                }
                
            }
        }
        
    }
}

struct CustomerListingView_Previews: PreviewProvider {
    static var previews: some View {
        CustomerListingView()
    }
}
