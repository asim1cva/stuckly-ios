//
//  AuthResponse.swift
//  StuckApp
//
//  Created by Asim on 26/08/2021.
//


import Foundation

// MARK: - AuthResponse
struct AuthResponse: Codable {
    let status: Int
    let message: String
    let data: DataClass
}

// MARK: - DataClass
struct DataClass: Codable {
    let id: Int?
    let userName, email, emailVerifiedAt, phoneNumber: String?
    let dateOfBirth, fcmToken, createdAt: String?
    let updatedAt: String?
    let role: UserType?
    let isMonthlySubscribed: Bool?
    let islogin : Int?


    enum CodingKeys: String, CodingKey {
        case id
        case userName = "user_name"
        case email
        case emailVerifiedAt = "email_verified_at"
        case phoneNumber = "phone_number"
        case dateOfBirth = "date_of_birth"
        case fcmToken = "fcm_token"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case role
        case isMonthlySubscribed
        case islogin


    }
}

// MARK: - AuthResponse
struct AlreadyLoginResponse: Codable {
    let status: Int
    let islogin: Bool
}



