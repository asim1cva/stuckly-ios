//
//  createPaymentIntentResponse.swift
//  Weat Local App
//
//  Created by Muhammad Salman on 1/19/22.
//

import Foundation

struct CreatePaymentIntentResponse: Codable {
    let data: IntentData
}

// MARK: - DataClass
struct IntentData: Codable {
    let intent, secret, status: String
}
