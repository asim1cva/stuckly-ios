//
//  UserBusinessListApiResponse.swift
//  StuckApp
//
//  Created by Asim on 1/12/22.
//

import Foundation


// MARK: - UserBusinessListApiResponse
struct UserBusinessListApiResponse: Codable {
    let status: Int
    let message: String
    let data: [BusinessListData]
}

// MARK: - Datum
struct BusinessListData: Codable {
    let id: Int
    let name: String
    let datumDescription, websiteLink: String?
    var imageURL, releaseDate, releaseMessage, releaseDetail: String?
    let userID: Int?
    let createdAt, updatedAt: String?
    let followersCount: Int?
    let images: [Images]
    let releaseImages : [String]
    var isfollowed: Bool?

    enum CodingKeys: String, CodingKey {
        case id, name
        case datumDescription = "description"
        case websiteLink = "website_link"
        case imageURL = "image_url"
        case releaseDate = "release_date"
        case releaseMessage = "release_message"
        case releaseDetail = "release_detail"
        case userID = "user_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case followersCount = "followers_count"
        case images
        case releaseImages = "release_image"
        case isfollowed
        
    }
}

// MARK: - Image
struct Images: Codable {
    let businessID, imageID, name: String?
    let imageURL: String?

    enum CodingKeys: String, CodingKey {
        case businessID = "business_id"
        case imageID = "image_id"
        case name
        case imageURL = "image_url"
    }
}
