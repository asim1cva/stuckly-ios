//
//  GetNotificationListApiResponse.swift
//  StuckApp
//
//  Created by Asim on 2/22/22.
//

import Foundation


// MARK: - GetNotificationListAPIResponse
struct GetScheduledNotificationListAPIResponse: Codable {
    let status: Int
    let message: String
    let notification_count: Int?
    let data: ScheduledNotificationListData
    
}

struct ScheduledNotificationListData: Codable {
  
    let past: [NotificationData]
    let future: [NotificationData]
    let today: [NotificationData]

    
}

// MARK: - GetNotificationListAPIResponse
struct GetNotificationListAPIResponse: Codable {
    let status: Int
    let message: String
    let notification_count: Int?
    let data: [NotificationData]
    
    
}

// MARK: - Datum
struct NotificationData: Codable {
    let id: Int
    let text, scheduleDate, scheduleTime, timezone: String?
    let businessID, userID: Int
    let isOpen: Bool?
    let createdAt, updatedAt: String?
    let business: BusinessListData?


    enum CodingKeys: String, CodingKey {
        case id, text
        case scheduleDate = "schedule_date"
        case scheduleTime = "schedule_time"
        case timezone
        case businessID = "business_id"
        case userID = "user_id"
        case isOpen
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case business

    }
}

// MARK: - Business
//struct Business: Codable {
//    let id: Int
//    let name, businessDescription, websiteLink: String
//    let imageURL: String?
//    let releaseDate, releaseMessage, releaseDetail, releaseImage: String
//    let userID: Int
//    let createdAt, updatedAt: String
//    let followersCount: Int
//    let isfollowed: Bool
//    let images: [Image]
//
//    enum CodingKeys: String, CodingKey {
//        case id, name
//        case businessDescription = "description"
//        case websiteLink = "website_link"
//        case imageURL = "image_url"
//        case releaseDate = "release_date"
//        case releaseMessage = "release_message"
//        case releaseDetail = "release_detail"
//        case releaseImage = "release_image"
//        case userID = "user_id"
//        case createdAt = "created_at"
//        case updatedAt = "updated_at"
//        case followersCount = "followers_count"
//        case isfollowed, images
//    }
//}
//
//// MARK: - Image
//struct Image: Codable {
//    let businessID, imageID, name: String
//    let imageURL: String
//
//    enum CodingKeys: String, CodingKey {
//        case businessID = "business_id"
//        case imageID = "image_id"
//        case name
//        case imageURL = "image_url"
//    }
//}
