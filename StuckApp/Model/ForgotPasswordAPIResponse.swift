//
//  ForgotPasswordAPIResponse.swift
//  SIGApp
//
//  Created by Admin Admin on 1/4/22.
//

import Foundation

// MARK: - ForgotPasswordAPIResponse
struct ForgotPasswordAPIResponse: Codable {
    let status: Int
    let message: String
    let data: CodeData
}

// MARK: - DataClass
struct CodeData: Codable {
    let code: Int
}
