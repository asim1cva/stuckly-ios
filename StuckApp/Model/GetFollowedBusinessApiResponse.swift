//
//  GetFollowedBusinessApiResponse.swift
//  StuckApp
//
//  Created by Admin Admin on 2/22/22.
//

import Foundation

// MARK: - GetFollowedBusinessApiResponse
struct GetFollowedBusinessApiResponse: Codable {
    let status: Int
    let message: String
    let data: [BusinessListData]
}


// MARK: - GetFollowedBusinessData
struct GetFollowedBusinessData: Codable {
    let id: Int
    let text: String?
    let businessID, userID: Int?
    let isOpen: Bool?
    let createdAt, updatedAt: String?
    let business: BusinessListData

    enum CodingKeys: String, CodingKey {
        case id, text
        case businessID = "business_id"
        case userID = "user_id"
        case isOpen
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case business
    }
}
