//
//  SimpleResponse.swift
//  StuckApp
//
//  Created by Admin Admin on 1/12/22.
//

import Foundation


// MARK: - SimpleResponse
struct SimpleResponse: Codable {
    let status: Int
    let message: String
}
