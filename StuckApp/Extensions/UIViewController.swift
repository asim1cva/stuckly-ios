//
//  UIViewController.swift
//  Psychnic Reading
//
//  Created by Asim on 09/08/2021.
//

import UIKit
import Foundation

extension UIViewController {
    func saveUserData(userdata: DataClass) {
        /// Encoading Data
        UserDefaults.standard.set(encodable: userdata, forKey: UserDefaultKey.savedUser)
    }
    
    
    func saveUserType(userType: String) {
        /// Encoading Data
        
        
        UserDefaults.standard.set(userType, forKey: UserDefaultKey.savedUserType)
        
        //        UserDefaults.standard.set(encodable: userdata, forKey: UserDefaultKey.saveLoggedInUserType)
    }
    
    
    func removeUserData() {
        /// Encoading Data
        //        UserDefaults.standard.removeObject(forKey: UserDefaultKey.savedUser)
        
        // Write/Set Boolean in User Defaults
        UserDefaults.standard.set(true, forKey: UserDefaultKey.savedUser)
        
        // Before Resetting User Defaults
        print("before", UserDefaults.standard.bool(forKey: UserDefaultKey.savedUser))
        
        // Remove Key-Value Pair
        UserDefaults.standard.removeObject(forKey: UserDefaultKey.savedUser)
        //        UserDefaults.standard.removeObject(forKey: UserDefaultKey.savedBranch)
        UserDefaults.standard.removeObject(forKey: UserDefaultKey.savedUserType)
        
        
        // After Resetting User Defaults
        print("after", UserDefaults.standard.bool(forKey: UserDefaultKey.savedUser))
    }
}

