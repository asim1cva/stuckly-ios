//
//  Services.swift
//  SlideMail
//
//  Created by Asim on 10/03/2021.
//

import Foundation
import Alamofire
import SwiftUI
import Combine

protocol serverResponse {
    func onSuccess(json:JSON, val:String)
    func onFailure(message: String)
}


protocol serverResponseData {
    func onSuccess(data:Data, val:String)
    func onFailure(message: String)
    
}

//@available(iOS 13.0, *)
class MVP : BaseViewConroller {
    
    private var task: Cancellable? = nil
    
    var delegate:serverResponse!
    var dataDelegate : serverResponseData!
    
    let headers: HTTPHeaders = [
        "Accept": "application/json"
    ]
    
    
  
    
    // MARK: requestAndReturnData
    func requestAndReturnData(
        url:String ,
        params:[String:Any] ,
        method:HTTPMethod,
        type:String
    ) {
        
        if CheckInternet.Connection() {
            
            
            print("API :- \(url)")
            print("params:- \(params)")

            
            self.task = AF.request(API.baseURL + url,
                                   method: method,
                                   parameters: params,
                                   encoding: URLEncoding.default,  // JSONEncoding for query Paramters
                                   headers: headers)
                .publishDecodable(type: JSON.self)
                .sink(receiveCompletion: {(completion) in
                    switch completion {
                    case .finished:
                        ()
                    case .failure(let error):
                        
                        print(error.localizedDescription)
                        
                    }
                    
                }, receiveValue: {(response) in
                    switch response.result {
                    
                    case .success(let value):
                        let json = JSON(value)
                        
                        print(json.stringValue)
                        if json["status"] == 200 {
                            DispatchQueue.main.async {
                                if let del = self.dataDelegate {
                                    del.onSuccess(data: response.data!, val: type)
                                }
                            }

                        } else  {
                            DispatchQueue.main.async {
                                if let del = self.dataDelegate {
                                    del.onFailure(message: "\(json["message"])")
                                    
                                }
                            }
                        }
                    case .failure(let error):
                        
                        DispatchQueue.main.async {
                            if let del = self.dataDelegate{
                                del.onFailure(message: "Service Failure : \(error.localizedDescription)")
                                
                            }
                        }
                        
                        break
                        
                    }
                })
            
        } else {
            
            DispatchQueue.main.async {
                if let del = self.dataDelegate{
                    del.onFailure(message: "Please check your internet connectivity.")
                    
                }
            }
            
            print("No Internet")
        }
    }
    
    
}
