//
//  ApisConstants.swift
//  StaffApp
//
//  Created by Asim on 10/18/21.
//

import Foundation

// API
struct API {
    
    static let baseURL = "http://145.14.158.138/stuckly/public/index.php/api/"

    // End Points
    static let login = "userLogin"
    static let register = "userRegister"
    
    static let verifyEmail = "verifyEmail"

    
    static let createBusiness = "createBusiness"
   
    static let updateBusiness = "updateBusiness"

    
    static let userBusinessList = "userBusinessList"
    
    
    static let getFollowedBusiness = "getFollowedBusiness"

    
    
    static let getBusinessList = "getBusinessList"

    static let followBusiness = "followBusiness"

    
    static let editProfile = "editProfile"

    
    static let setReleaseDate = "setReleaseDate"
    
    static let setScheduleNotification = "setScheduleNotification"

    
    
    static let forgotPassword = "forgotPassword"

    static let changePassword = "changePassword"

    
    static let createPaymentIntent = "createPaymentIntent"

    
    static let monthlySubscribed = "monthlySubscribed"

    static let sendInstantNotification = "sendInstantNotification"

    
    static let getNotification = "getNotification"
    
    static let scheduledNotification = "scheduledNotification"

    
    
    static let isOpen = "isOpen"

    static let alreadyLogin = "alreadyLogin"

    

    
}
