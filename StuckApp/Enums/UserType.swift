//
//  UserType.swift
//  StuckApp
//
//  Created by Asim on 1/11/22.
//

import Foundation


enum UserType: String, CaseIterable, Identifiable, Codable {
    case business
    case customer
    var id: String
    { self.rawValue }
}
