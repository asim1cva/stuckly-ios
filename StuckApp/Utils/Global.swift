//
//  Global.swift
//  StuckApp
//
//  Created by Asim on 16/12/2021.
//

import Foundation

import UIKit

class Global {
    
    class var shared : Global {
        
        struct Static{
            static let instance: Global = Global()
        }
        return Static.instance
    }
    var currentLoginUser : DataClass!
    var currentUserFcm: String!
    
   
}


let userDefault = UserDefaults.standard
