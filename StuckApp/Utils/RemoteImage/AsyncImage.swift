//
//  AsyncImage.swift
//  AsyncImage
//
//  Created by Asim on 12/13/21.
//

import SwiftUI

struct AsyncImage<Placeholder: View>: View {
    
    @StateObject private var loader: ImageLoader
    
    private let placeholder: Placeholder
    private let image: (UIImage) -> Image
    private let imageWidth: CGFloat
    private let imageHeight: CGFloat
    private var isRounded: Bool = false
    private var cornerRadius: CGFloat = 0

    
    init(
        url: URL,
        @ViewBuilder placeholder: () -> Placeholder,
        @ViewBuilder image: @escaping (UIImage) -> Image = Image.init(uiImage:),
        imageWidth: CGFloat,
        imageHeight: CGFloat,
        isRounded: Bool,
        cornerRadius: CGFloat
        
    ) {
        self.placeholder = placeholder()
        self.image = image
        self.imageWidth = imageWidth
        self.imageHeight = imageHeight
        self.isRounded = isRounded
        self.cornerRadius = cornerRadius
        
        
        _loader = StateObject(wrappedValue: ImageLoader(url: url, cache: Environment(\.imageCache).wrappedValue))
    }
    
    var body: some View {
        content
            .onAppear(perform: loader.load)
    }
    
    private var content: some View {
        Group {
            if loader.image != nil {
                
                if isRounded {
                    image(loader.image!)
                        .resizable()
                        .scaledToFill()
                        .clipShape(Circle())
                        .shadow(radius: 10)
                        .frame(width: imageWidth, height: imageHeight)
                    
                } else {
                    image(loader.image!)
                        .resizable()
                        .scaledToFill()
                        .frame(width: imageWidth, height: imageHeight)
                        .clipped()
                        .cornerRadius(cornerRadius)
                    
                }
           
            } else {
                placeholder
            }
        }
    }
}
