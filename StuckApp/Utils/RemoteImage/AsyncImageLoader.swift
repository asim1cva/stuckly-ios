//
//  AsyncImageLoader.swift
//  SwiftUiPractice
//
//  Created by Asim on 12/22/21.
//

import Foundation
import SwiftUI

struct AsyncImageLoader: View {
    
    var imageUrl: String = ""
    
    var imageWidth: CGFloat
    var imageHeight: CGFloat
    var isRounded: Bool = false
    var cornerRadius: CGFloat = 0
    
    var isFullView: Bool = false


    var url: URL? = nil
    @State private var fullImageVieToggle = false
    
    
    init(imageUrl: String,
         imageWidth: CGFloat,
         imageHeight: CGFloat,
         isRounded: Bool,
         cornerRadius: CGFloat,
         isFullView : Bool = false
    ) {
        
        self.imageUrl = imageUrl
        self.imageWidth = imageWidth
        self.imageHeight = imageHeight
        self.isRounded = isRounded
        self.cornerRadius = cornerRadius
        self.isFullView = isFullView
        
        if imageUrl != "" {
            guard let parsedURL = URL(string: imageUrl) else {
                //            print("Invalid URL")
                fatalError("Invalid URL: \(imageUrl)")
            }
            
            self.url = parsedURL
        } else {
            return
        }
        
    }
    var body: some View {
        
        ZStack {
            NavigationLink(
                destination: fullScreenImageView,
                isActive: $fullImageVieToggle){}
                .hidden()
            
            
            AsyncImage(
                url: url!,
                placeholder: {
                    ProgressView()
                        .frame(width: imageWidth, height: imageHeight)
                    
                },
                image: {
                    Image(uiImage: $0)
                        .resizable()
                    
                },
                imageWidth: imageWidth,
                imageHeight: imageHeight,
                isRounded: self.isRounded,
                cornerRadius: cornerRadius
            )
                
//                .onTapGesture {
//                    fullImageVieToggle.toggle()
//
//                    if self.isFullView {
//
//                    }
//                }
           
            
        }
        
        
    }
    
    
    // MARK: FullScreen ImageView
    private var fullScreenImageView: some View {
        
        
        
        AsyncImage(
            url: url!,
            placeholder: {
                ProgressView()
                    .frame(width: imageWidth, height: imageHeight)
                
            },
            image: {
                Image(uiImage: $0)
                    .resizable()
                
                
                
            },
            imageWidth: .infinity,
            imageHeight: .infinity,
            isRounded: false, cornerRadius: 12
        )
            .transition(.opacity.combined(with: .scale))
            .navigationBarTitle("Image")
            .navigationBarTitleDisplayMode(.inline)
        
   
    }
    
    
    
}
