//
//  ImagePicker.swift
//  imagePicker
//
//  Created by Asim on 26/08/2021.
//

import Foundation
import SwiftUI
import UIKit


struct ImagePicker: UIViewControllerRepresentable {
    
    // Mark: -  Sean Allen Method
    @Binding var avatarImage: UIImage
    var sourceType: UIImagePickerController.SourceType = .camera
    
    func makeUIViewController(context: Context) -> UIImagePickerController {
        let picker = UIImagePickerController()
        picker.delegate = context.coordinator
        picker.sourceType = sourceType
        picker.allowsEditing = true
        return picker
        
    }
    
    func updateUIViewController(_ uiViewController: UIImagePickerController, context: Context) {}
    
    func makeCoordinator() -> Coordinator {
        return Coordinator(photoPicker: self)
    }
    
    final class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
        
        let photoPicker : ImagePicker
        
        init(photoPicker: ImagePicker) {
            self.photoPicker = photoPicker
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            if let uiimage = info[.editedImage] as? UIImage {
                guard let data = uiimage.jpegData(compressionQuality: 1.0), let compressedImage = UIImage(data: data) else {
                    // Show Error or Alert
                    return
                }
                photoPicker.avatarImage = compressedImage
//                showImagePicker = false
            } else {
                // Return an error or show alert
                print("Image Not Selected")
            }
            
            picker.dismiss(animated: true)
        }
        
    }
    

    
    // By Owner of Project
//
//    @Binding var image: UIImage?
//    @Binding var showImagePicker: Bool
//
//    typealias UIViewControllerType = UIImagePickerController
//    typealias Coordinator = imagePickerCoordinator
//
//    var sourceType:UIImagePickerController.SourceType = .camera
//
//
//    func makeUIViewController(context: UIViewControllerRepresentableContext<imagePicker>) -> UIImagePickerController {
//        let picker = UIImagePickerController()
//        picker.sourceType = sourceType
//        picker.delegate = context.coordinator
//        return picker
//    }
//
//    func makeCoordinator() -> imagePicker.Coordinator {
//        return imagePickerCoordinator(image: $image, showImagePicker: $showImagePicker)
//    }
//
//    func updateUIViewController(_ uiViewController: UIImagePickerController, context: UIViewControllerRepresentableContext<imagePicker>) {}
//
//
//}
//
//
//
//class imagePickerCoordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
//        @Binding var image: UIImage?
//        @Binding var showImagePicker: Bool
//
//    init(image:Binding<UIImage?>, showImagePicker: Binding<Bool>) {
//            _image = image
//            _showImagePicker = showImagePicker
//    }
//
//
//
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//        if let uiimage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
//            image = uiimage
//            showImagePicker = false
//        }
//    }
//
//    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
//        showImagePicker = false
//    }


}
