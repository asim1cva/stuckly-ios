//
//  ImagePickerManager.swift
//  FitnessApp
//
//  Created by Asim on 28/08/2021.
//


import Foundation
import UIKit

class ImagePickerManager: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    var picker = UIImagePickerController();
    var alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
    var viewController: UIViewController?
    var pickImageCallback : ((UIImage) -> ())?;
    var videoURL: NSURL?

    override init(){
        super.init()
    }

    // Pick Image from Camera and gallery menu
    func pickImage( viewController: UIViewController,  callback: @escaping ((UIImage) -> ())) {
        pickImageCallback = callback;
        self.viewController = viewController;

        let cameraAction = UIAlertAction(title: "Take Image", style: .default){
            UIAlertAction in
            self.openCamera()
        }
        let galleryAction = UIAlertAction(title: "Get image from gallery", style: .default){
            UIAlertAction in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel){
            UIAlertAction in
        }

        // Add the actions
        picker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(cancelAction)
        alert.popoverPresentationController?.sourceView = self.viewController!.view
        viewController.present(alert, animated: true, completion: nil)
    }
    
    func openCamera(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.alert.dismiss(animated: true, completion: nil)
        if(UIImagePickerController .isSourceTypeAvailable(.camera)){
            self.picker.sourceType = .camera
            self.picker.allowsEditing = true
            self.viewController!.present(self.picker, animated: true, completion: nil)
        } else {
            let alertWarning = UIAlertView(title:"Warning", message: "You don't have camera", delegate:nil, cancelButtonTitle:"OK", otherButtonTitles:"")
            alertWarning.show()
        }
            
        }
    }
    func openGallery(){
        alert.dismiss(animated: true, completion: nil)
        picker.sourceType = .photoLibrary
        picker.allowsEditing = true
        self.viewController!.present(picker, animated: true, completion: nil)
    }


    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK: Pick image from gallery
    func pickImageFromGallery( viewController: UIViewController,  callback: @escaping ((UIImage) -> ())) {
        pickImageCallback = callback;
        self.viewController = viewController;

        picker.sourceType = .photoLibrary
        picker.allowsEditing = false
        
        self.viewController!.present(picker, animated: true, completion: nil)
        picker.delegate = self


//        viewController.present(picker, animated: true, completion: nil)
    }
    
    //MARK: Pick image from gallery
    func pickImageFromCamera( viewController: UIViewController,  callback: @escaping ((UIImage) -> ())) {
        pickImageCallback = callback;
        self.viewController = viewController;


        picker.delegate = self
        picker.sourceType = .camera
        picker.allowsEditing = false
        
        viewController.present(picker, animated: true, completion: nil)
    }
    
    

    //MARK: Pick image from Camera
    func pickImageFromCamera(vc: UIViewController) {
        picker.sourceType = .camera
        picker.allowsEditing = false
        vc.present(picker, animated: true, completion: nil)
    }
    
    // For Swift 4.2+
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        let compressedImage = image.compressImage(compressionRate: 1.0)
        pickImageCallback?(compressedImage.resizeWithWidth(width: 700)!)

    }



    @objc func imagePickerController(_ picker: UIImagePickerController, pickedImage: UIImage?) {
    }
    
   

}


extension UIImage {
    func compressImage(compressionRate: CGFloat) -> UIImage {
        
        return UIImage(data:self.jpegData(compressionQuality: compressionRate)!,scale:1.0)!
    }
    
    func resizeWithWidth(width: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
        imageView.contentMode = .scaleAspectFill
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
}
