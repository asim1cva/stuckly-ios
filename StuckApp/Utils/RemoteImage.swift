//
//  RemoteImage.swift
//  FitnessApp
//
//  Created by Asim on 27/08/2021.
//

import SwiftUI

struct RemoteImage: View {
    private enum LoadState {
        case loading, success, failure
    }
    
    private class Loader: ObservableObject {
        var data = Data()
        var state = LoadState.loading
        
        init(url: String) {
            guard let parsedURL = URL(string: url) else {
                fatalError("Invalid URL: \(url)")
                
            }
            
            URLSession.shared.dataTask(with: parsedURL) { data, response, error in
                if let data = data, data.count > 0 {
                    self.data = data
                    self.state = .success
                } else {
                    self.state = .failure
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.objectWillChange.send()
                }
            }.resume()
        }
    }
    
    @StateObject private var loader: Loader
    var loading: Image
    var failure: Image
    
//    var user: User
    var imageWidth: CGFloat
    var imageHeight: CGFloat
    var isRounded: Bool = false
    
    
    var body: some View {
        ZStack {
            
            if isRounded {
                selectImage()
                    .resizable()
//                    .scaledToFit()
                    .clipShape(Circle())
                    .shadow(radius: 10)
                    .frame(width: imageWidth, height: imageHeight)

            } else {
                selectImage()
                    .resizable()
//                    .scaledToFit()
                    .frame(width: imageWidth, height: imageHeight)
            }
            
            
            
        }
        
        
    }
    
    
    init(
         url: String,
         loading: Image = Image(systemName: "photo.fill"),
         failure: Image = Image(systemName: "arrow.down.doc.fill"),
         imageWidth: CGFloat,
         imageHeight: CGFloat,
         isRounded: Bool
         
    ) {
        _loader = StateObject(wrappedValue: Loader(url: url))
        self.loading = loading
        self.failure = failure
//        self.user = user
        self.imageWidth = imageWidth
        self.imageHeight = imageHeight
        self.isRounded = isRounded
    }
    
    private func selectImage() -> Image {
        switch loader.state {
        case .loading:
            return loading
        case .failure:
            return failure
        default:
            if let image = UIImage(data: loader.data) {
                return Image(uiImage: image)
            } else {
                return failure
            }
        }
    }
}
