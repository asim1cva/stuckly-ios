//
//  HelperFunctions.swift
//  FitnessApp
//
//  Created by Asim on 30/08/2021.
//

import SwiftUI
import UIKit
import Foundation
import CoreLocation
import MapKit




// MARK: Helper methods

class HelperFunctions: NSObject {
    
    
    // MARK: stringToDate

    class func stringToDate(strDate: String) -> Date {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: strDate)!
        
        return date
    }
    
    // MARK: daysBetween
    func daysBetween(start: Date, end: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: start, to: end).day!
    }
    
    func split(name: String) -> (firstName: String, lastName: String) {
        let split = name.components(separatedBy: " ")
        return (split[0], split[1])
    }
    
    
    
    
    
    class func getUiImageFromUrl(imageUrl: String) ->  UIImage {
        var image: UIImage = UIImage()
        
        //        let url = URL(string: imageUrl)
        //           if let data = try? Data(contentsOf: url!) {
        //               return UIImage(data: data)!
        //           }
        //
        //
        //        return UIImage()
        
        
        DispatchQueue.global(qos: .background).async {
            do {
                let data = try Data.init(contentsOf: URL.init(string: imageUrl)!)
                DispatchQueue.main.async {
                    image = UIImage(data: data)!
                    //                    return image
                    
                }
            } catch {
                // error
                //                return image
                
            }
        }
        return image
        
        
    }
    
    
    // MARK: dialNumber
    class func dialNumber(number : String) {
        
        if let url = URL(string: "tel://\(number)"),
           UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            
            print("Error")
            // add error message here
        }
    }
    
    
    
    // MARK: Number Formatter
    class func numberFormatter() -> NumberFormatter  {
        let nf = NumberFormatter()
        nf.usesGroupingSeparator = false
        return nf
    }
    
    // MARK: Number Formatter
    
    class func yearString(at index: Int, years: Array<Any>) -> String {
        //        let years = Array(1990...2021)
        
        let selectedYear = years[index]
        return numberFormatter().string(for: selectedYear) ?? (selectedYear as AnyObject).description
    }
    
    // MARK: MapsAppWithDirections
    
    class func openMapsAppWithDirections(to coordinate: CLLocationCoordinate2D, destinationName name: String) {
        let options = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = name // Provide the name of the destination in the To: field
        mapItem.openInMaps(launchOptions: options)
    }
    
    
    // MARK: HighLight Text
    class func highlightText(str: String, searched: String) -> Text {
        guard !str.isEmpty && !searched.isEmpty else { return Text(str) }
        
        var result: Text!
        let parts = str.components(separatedBy: searched)
        for i in parts.indices {
            result = (result == nil ? Text(parts[i]) : result + Text(parts[i]))
            if i != parts.count - 1 {
                result = result + Text(searched).bold()
            }
        }
        return result ?? Text(str)
    }
    
    
    // MARK: get Double From String
    
    class func getDoubleFromString(str: String) -> Double {
        
        return (str as NSString).doubleValue
    }
    
    
    // MARK: getIntFromString
    
    class func getIntFromString(str: String) -> Int {
        
        return (str as NSString).integerValue
    }
    
    
    // MARK: Highlight Specific text in Text
    
    class func hilightedText(str: String, searched: String) -> Text {
        guard !str.isEmpty && !searched.isEmpty else { return Text(str) }
        
        var result: Text!
        let parts = str.components(separatedBy: searched)
        for i in parts.indices {
            result = (result == nil ? Text(parts[i]) : result + Text(parts[i]))
            if i != parts.count - 1 {
                result = result + Text(searched).bold()
            }
        }
        return result ?? Text(str)
    }
    
    
    //  dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    
    
    // MARK: currentDate
    
    // get current date from formate "yyyy-MM-dd HH:mm:ss"
    class func currentDate(dateFormat: String) -> String {
        let dateFormatter : DateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = dateFormat
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        //        let interval = date.timeIntervalSince1970
        
        return dateString
        
    }
    
    
    // MARK: currentTime
    
    // get current date from formate "yyyy-MM-dd HH:mm:ss"
    class func currentTime(timeFormat: String) -> String {
        let dateFormatter : DateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = timeFormat
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        //        let interval = date.timeIntervalSince1970
        
        return dateString
        
    }
    
    
    // MARK: getLastFileNameFromUrl
    
    // get last segmant from url
    class func getLastFileNameFromUrl(url: URL) -> String {
        
        let theURL = URL(string: "\(url)")  //use your URL
        let fileNameWithExt = theURL?.lastPathComponent //somePDF.pdf
        //        let fileNameLessExt = theURL?.deletingPathExtension().lastPathComponent //somePDF
        
        return fileNameWithExt!
        
    }
    
    
    // MARK: pdfFileTobase64
    
    // Convert Pdf File to Base64
    class func pdfFileTobase64(url: URL) -> String {
        
        let fileData = try! Data.init(contentsOf: url)
        let base64String: String = fileData.base64EncodedString(options: NSData.Base64EncodingOptions.init(rawValue: 0))
        
        return "data:application/pdf;base64,\(base64String)"
        
    }
    
    
    // MARK: isValidEmailAddress
    
    class func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    // MARK: imageTobase64
    
    class func imageTobase64(withCompression: CGFloat = 1.0, image: UIImage) -> String{
        
//        let image = compressImage(image: image)
        return imgConverTobase64(image: image)
        
    }
    
    
    // MARK: imageTobase64
    
    private class  func imgConverTobase64(image: UIImage) -> String {
//
//        let imageData:NSData = image.pngData()! as NSData
//        let imageStr =  imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
//        let strBase64Image = "data:image/png;base64,\(imageStr)"
        
        
        let strBase64Image = "data:image/png;base64,\(image.pngData()!.base64EncodedString())"

        return strBase64Image
        
    }
    
    
    // MARK: dateConverter
    
    class func dateConverter(date: Date, dateFormat: String) -> String {
        
        // Create Date Formatter
        let dateFormatter = DateFormatter()
        
        // Set Date Format
        dateFormatter.dateFormat = dateFormat
        
        
        //        "y, M d"                // 2020, 10 29
        //        "YY, MMM d"             // 20, Oct 29
        //        "YY, MMM d, hh:mm"      // 20, Oct 29, 02:18
        //        "YY, MMM d, HH:mm:ss"   // 20, Oct 29, 14:18:31
        
        // Convert Date to String
        dateFormatter.string(from: date)
        return dateFormatter.string(from: date)
        
    }
    
    
    // MARK: timeConvert
    
    class func timeConvert(time unixtimeInterval: Int) -> String{
        let date = Date(timeIntervalSince1970: TimeInterval(unixtimeInterval))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    
    
    // MARK: UTCtoLocal
    
    class func UTCtoLocal(date: String) -> String? {
        let utcTime = date
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let date = dateFormatter.date(from: utcTime)
        let dateString = "\(date!)"
        let array = dateString.components(separatedBy: "+")
        return array.first
        //println("utc: \(utcTime), date: \(date)")
    }
    
    
    // MARK: compressImage
    
    class func compressImage(image : UIImage) -> UIImage {
        
        var _actualImageHeight : CGFloat = image.size.height
        var _actualImageWidth : CGFloat = image.size.width
        let _maxHeight : CGFloat = 300.0
        let _maxWidth : CGFloat = 400.0
        var _imageRatio : CGFloat = _actualImageWidth / _actualImageHeight
        let _maxRatio: CGFloat = _maxWidth / _maxHeight
        let _imageCompressionQuality : CGFloat = 1.0 // makes the image get compressed to 50% of its actual size
        
        if _actualImageHeight > _maxHeight || _actualImageWidth > _maxWidth
        {
            if _imageRatio < _maxRatio
            {
                // Adjust thw width according to the _maxHeight
                _imageRatio = _maxHeight / _actualImageHeight
                _actualImageWidth = _imageRatio * _actualImageWidth
                _actualImageHeight = _maxHeight
            }
            else
            {
                if _imageRatio > _maxRatio
                {
                    // Adjust height according to _maxWidth
                    _imageRatio = _maxWidth / _actualImageWidth
                    _actualImageHeight = _imageRatio * _actualImageHeight
                    _actualImageWidth = _maxWidth
                }
                else
                {
                    _actualImageHeight = _maxHeight
                    _actualImageWidth = _maxWidth
                }
                
            }
        }
        let _compressedImage : CGRect = CGRect(x: 0.0 , y: 0.0 , width: _actualImageWidth , height: _actualImageHeight)
        UIGraphicsBeginImageContext(_compressedImage.size)
        image.draw(in: _compressedImage)
        let img: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        let imageData: NSData = img.jpegData(compressionQuality: _imageCompressionQuality)! as NSData
        UIGraphicsEndImageContext()
        return UIImage(data: imageData as Data)!
    }
    
}

