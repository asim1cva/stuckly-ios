//
//  StuckAppApp.swift
//  StuckApp
//
//  Created by Asim on 16/12/2021.
//

import SwiftUI
import Firebase
import UserNotifications
import FirebaseMessaging
import StripeCore


var serverRequest = MVP()


@main
struct StuckAppApp: App {
    
    
    // MARK: Properties
    var userType = UserDefaults.standard.string(forKey: UserDefaultKey.savedUserType)
    
    
    var user = UserDefaults.standard.value(DataClass.self, forKey: UserDefaultKey.savedUser)
    
    // MARK: For AppDelegate to initialize
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    
    init() {
        StripeAPI.defaultPublishableKey = StripeConstants.publishableKey
    }
    
    @StateObject var customerViewModel = CustomerViewModel()

    @StateObject var businessViewModel = BusinessTabsViewModel()

    // MARK: body

    var body: some Scene {
        
        WindowGroup {
            NavigationView {
                
                switch userType {
                    
                case UserType.business.rawValue:

                    BusinessDashBoardView()
                        .onAppear {
                            saveGlobal()
                        }
                        .onAppear(perform: UIApplication.shared.addTapGestureRecognizer)
                        .navigationBarTitleDisplayMode(.inline)
                        .environmentObject(customerViewModel)
                        .environmentObject(businessViewModel)
                    
                        .navigationBarTitle("")
                        .navigationBarHidden(true)
                        .navigationBarBackButtonHidden(true)
                       
                    
                case UserType.customer.rawValue:
                    

                    CustomerBottomTabsView()
                        .onAppear {
                            saveGlobal()
                        }
                        .onAppear(perform: UIApplication.shared.addTapGestureRecognizer)
                                            .navigationBarTitleDisplayMode(.inline)
                       
                        .environmentObject(customerViewModel)
                        .environmentObject(businessViewModel)
                        .navigationBarTitle("")
                        .navigationBarHidden(true)
                        .navigationBarBackButtonHidden(true)
                     
                    
                default:
                    
                    WelcomeScreenView()
                        .onAppear(perform: UIApplication.shared.addTapGestureRecognizer)
                        .environmentObject(customerViewModel)
                        .environmentObject(businessViewModel)
                    
                    
                }
                
                
                
            }
            .onAppear {
                print("onAppear: - StuckAppApp")

                if user != nil {
                    saveGlobal()

                    
                    businessViewModel.getNotificationListApiRequest()
                }
                
            }
            
        }
    }
    
 
    
    private func saveGlobal() {
        Global.shared.currentLoginUser = user
        
    }
}


// MARK: AppDelegate

class AppDelegate: NSObject, UIApplicationDelegate {
    let gcmMessageIDKey = "gcm.message_id"
    
    
    
    
//    class AppDelegate: NSObject, UIApplicationDelegate, UNUserNotificationCenterDelegate {
//
//        func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
//
//            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
//                print("Dispatch")
//                AppState.shared.pageToNavigationTo = "test"
//            }
//
//            return true
//        }
//    }
    
   
    
    
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        
        
        
        // Mark: - Firebase configure
        FirebaseApp.configure()
        
        
        // [START set_messaging_delegate]
        Messaging.messaging().delegate = self
        
        // [END set_messaging_delegate]
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        // [START register_for_notifications]
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
            UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
//            print("Dispatch")
//            AppState.shared.pageToNavigationTo = "Notification Clicked"
//        }
        
        return true
    }
    
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            print("Dispatch")
            AppState.shared.pageToNavigationTo = "Notification Clicked"
        }
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            print("Dispatch")
            AppState.shared.pageToNavigationTo = "test1"
        }
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    // [END receive_message]
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the FCM registration token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        
        // With swizzling disabled you must set the APNs token here.
        Messaging.messaging().apnsToken = deviceToken
        
        
    }
    
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
}



// MARK: AppDelegate extension functions


extension AppDelegate : MessagingDelegate {
    
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        
        print("Firebase registration token: \(fcmToken ?? "")")
        
        let dataDict:[String: String] = ["token": fcmToken ?? ""]
        
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    
    //    func application(application: UIApplication,
    //                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    //      Messaging.messaging().apnsToken = deviceToken
    //    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingDelegate) {
        print("Received data message: \(remoteMessage.description)")
    }
    
    // Old way
    //  func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage?) {
    //    print("Received data message: \(remoteMessage.appData)")
    //  }
    // [END ios_10_data_message]
}


// for In app Notification
extension AppDelegate : UNUserNotificationCenterDelegate {

    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
      withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
      
        let userInfo = notification.request.content.userInfo

        // Do Something with MSG Data..

      Messaging.messaging().appDidReceiveMessage(userInfo)

        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        print(userInfo)
      // Change this to your preferred presentation option
        completionHandler([[.banner, .badge, .sound]])
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
      let userInfo = response.notification.request.content.userInfo

        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
      Messaging.messaging().appDidReceiveMessage(userInfo)

      completionHandler()
    }


}

struct StripeConstants {
    
    static let publishableKey = "pk_live_51Kh2UuETz6GyvrICtd8m8EbMakas8dOonPrYjaxfMCzevrhQlZlJQgdby2PXcme6nsMv5Nh3I3YNNPesoDWtfXTj00E6YmDjWC"

//    static let publishableKey = "pk_test_51HyHLOJafjVru5XaN4jujtsQXBSfUGvJ5kWbbPKu5CJ7jhZ6x3RJlbUnHAHMJDnN8V0Vt6JkvAbt5yPCrzU5EGaf00kQWP18Pf"
    
}
