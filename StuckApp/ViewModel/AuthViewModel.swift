//
//  AuthViewModel.swift
//  SIGApp
//
//  Created by Asim on 1/4/22.
//

import SwiftUI
import Foundation
import Combine
import UIKit
import Alamofire
import FirebaseMessaging


final class AuthViewModel: UIViewController, ObservableObject, serverResponseData {
    
    
    //MARK: Properties
    
    @Published var authApiResponse: AuthResponse? = nil
    
    @Published var user = UserDefaults.standard.value(DataClass.self, forKey: UserDefaultKey.savedUser)
    
    
    //    @State var driver = UserDefaults.standard.value(DataClass.self, forKey: UserDefaultKey.savedUser)
    
    private var task: Cancellable? = nil
    @Published var email: String = ""
    @Published var password: String = ""
    @Published var cPassword: String = ""  // Confirm Password
//    @Published var userName: String = ""
  
    @Published var showUpdateButton = false

    @Published var profileUpdateAlertToggle = false

    
    
    
    @Published var dateOfBirth = Date()
    
    
    @Published var loginSignUpTopImage: String = ""
    
    @Published var oldPassword: String = ""
    @Published var newPassword: String = ""
    @Published var confirmPassword: String = ""

    @Published var signUpToggle = false

    
    @Published var phone: String = ""
    @Published var name: String = ""
    @Published var forgotPasswordSheet = false
    @Published var isForgotPasswordRequest = false
    @Published var isChangePasswordRequest = false
    
    @Published var forgotPasswordApiResponse: ForgotPasswordAPIResponse? = nil
    
    
    @Published var code: String = ""
    @Published var isCodeVerify: Bool = false
    
    //    var username: String = ""
    
    @Published var alertItem : AlertItem?
    @Published var loading = false
    @Published var isAuthenticated: Bool = false
    
    //    var fcmToken: String = UserDefaults.standard.string(forKey: UserDefaultKey.savedToken) ?? ""
    
    @Published var signOutToggle = false
    
    
    
    // MARK: loginn API
    
    @Published var businessDashboardViewToggle = false
    @Published var customerDashboardViewToggle = false

    
    func loginApiRequest(userType: UserType) {
        
        
        loading = true
        
        
        
        Messaging.messaging().token { (token, error) in
            if let error = error {
                
                print("Messaging.messaging().token \(error.localizedDescription)")

            } else if let token = token {
                
                print("FCM Token: \(token)")

                
                
                let params: [String: Any] = ["email": self.email,
                                             "password" : self.password,
                                             "role": userType.rawValue,
                                             "fcm_token": token
                                             
                                             
                                             
                ]
                
                serverRequest.dataDelegate = self
                
                serverRequest.requestAndReturnData(url: API.login,
                                                   params: params,
                                                   method: .post,
                                                   type: "loginApiRequest"
                )
            }
        }
        
       
        
        
        
    }
    
    
    // MARK: signUp API
    
    func signUpApiRequest(userType: UserType) {
        
        
        if self.name == "" {
            return self.alertItem = AlertContext.errorMessage(message: "Name is required")
            
        }
        
        if self.email == "" {
            return self.alertItem = AlertContext.errorMessage(message: "Email is required")
            
        }
        
        
//        if self.phone == "" {
//            return self.alertItem = AlertContext.errorMessage(message: "Phone number is required")
//
//        }
        
        if self.password == "" {
            return self.alertItem = AlertContext.errorMessage(message: "Password is required")
            
        }
        
        
        loading = true
        
        
        
        Messaging.messaging().token { (token, error) in
            if let error = error {
                
                print("Messaging.messaging().token \(error.localizedDescription)")

            } else if let token = token {
                
                print("FCM Token: \(token)")

                
                
                let params: [String: Any] = ["email": self.email,
                                             "user_name": self.name,
                                             "password" : self.password,
                                             "role" : userType.rawValue,
                                             "phone_number" : self.phone,
                                             "fcm_token": token
                                             
                                             
                                             
                ]
                
                print(params)
                
                serverRequest.dataDelegate = self
                
                serverRequest.requestAndReturnData(url: API.register,
                                                   params: params,
                                                   method: .post,
                                                   type: "signUpApiRequest"
                )
                
            }
        }
        
        
        
       
        
    }
    
    
    // MARK: verifyEmailApiRequest
    
    @Published var verifyEmailApiRequestToggle = false

    @Published var verifyEmailCode: Int = 0

    
    func verifyEmailApiRequest() {
        
        
        if self.name == "" {
            return self.alertItem = AlertContext.errorMessage(message: "Name is required")
            
        }
        
        if self.email == "" {
            
            return self.alertItem = AlertContext.errorMessage(message: "Email is required")
            
        }
        
        
        if !HelperFunctions.isValidEmailAddress(emailAddressString: self.email) {
            
            return self.alertItem = AlertContext.errorMessage(message: "Email is not valid")

        }
        
        
//        if self.phone == "" {
//            return self.alertItem = AlertContext.errorMessage(message: "Phone number is required")
//
//        }
        
        if self.password == "" {
            return self.alertItem = AlertContext.errorMessage(message: "Password is required")
            
        }
        
        if self.password.count < 6 {
            
            return self.alertItem = AlertContext.errorMessage(message: "Password length should not be less than 6")

        }
        
        
        loading = true
        
        
        let params: [String: Any] = ["email": self.email,
                                    
                                     
                                     
                                     
        ]
        
        print(params)
        
        serverRequest.dataDelegate = self
        
        serverRequest.requestAndReturnData(url: API.verifyEmail,
                                           params: params,
                                           method: .post,
                                           type: "verifyEmailApiRequest"
        )
        
    }
    
    //    MARK: API Authentication...
    func editProfileApiRequest() {
        
        loading = true
        
        let params: [String: Any] = ["user_id"        : Global.shared.currentLoginUser.id ?? 0,
                                     "user_name"     : self.name,
                                     "email"          : self.email,
                                     "phone_number"   : self.phone
        ]
        
        serverRequest.dataDelegate = self
        
        serverRequest.requestAndReturnData(url: API.editProfile,
                                           params: params,
                                           method: .post,
                                           type: "editProfileApiRequest"
        )
    }
    
    
    
    // MARK: alertView with Text Field and Button
    
    func updateProfileDialog(title: String = "", isUpdate: Bool = true, updateText: String = "") -> UIAlertController {
        
        
        
        
        let alert = UIAlertController(title: title,
                                      message: "Update \(title)",
                                      preferredStyle: .alert)
        
        alert.addTextField { (txt) in
            if isUpdate {
                txt.text = updateText
            } else {
                txt.placeholder = "Type here"
                
            }
            
        }
        
        
        let doneAction = UIAlertAction(title: "Done",
                                       style: .default){ (_) in
            
            // MARK: Checking for Empty Click.. .
            
            let user = alert.textFields![0].text ?? updateText
            
            print("Update Text: \(updateText)")
            
            
            
            
            
            
            
            // MARK: reprompting AlertView
            
            //            UIApplication.shared.windows.first?.rootViewController?.present(alert, animated: true)
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive){ (_) in
            
            // MARK: Checking for Empty Click.. .
            // MARK: reprompting AlertView
            
            //            UIApplication.shared.windows.first?.rootViewController?.present(alert, animated: true)
            
        }
        
        alert.addAction(doneAction)
        alert.addAction(cancelAction)
        
        return alert
    }
    
    
    // MARK: updatePassword API
    
    @Published var dob: String = ""
    @Published var aboutMe: String = ""
    @Published var selfDescription: String = ""
    @Published var selectedGender: String = ""
    
    
    func updateProfileApiRequest() {
        
        print("updateProfileApiRequest")

        loading = true
        
        let params: [String: Any] = ["email": user?.email ?? "",
                                     "gender" : self.selectedGender,
                                     "user_name" : self.name,
                                     "about" : self.aboutMe,
                                     "self_description" : self.selfDescription,
                                     "user_id" : user?.id ?? 0,
                                    
        ]
        
        
        print(params)
        
        serverRequest.dataDelegate = self
        
        serverRequest.requestAndReturnData(url: API.editProfile,
                                           params: params,
                                           method: .post,
                                           type: "updateProfileApiRequest"
        )
        
        
    }
    
    
    
    // MARK: updatePassword API
    
    func updatePasswordApiRequest() {
        
        loading = true
        let params: [String: Any] = ["user_id": user?.id  ?? "",
                                     "old_password" : oldPassword,
                                     "new_password" : newPassword,
                                     
        ]
        
        
    }
    
    
    // MARK: ForgotPassword Request
    
    @Published var otpViewToggle  = false
    @Published var newPasswordViewToggle = false
    @Published var passChanged    = false

    
    func forgotPasswordApiRequest() {
        
        loading = true
        let params: [String: Any] = [
            "email" : self.email
        ]
        
        serverRequest.dataDelegate = self
        serverRequest.requestAndReturnData(url: API.forgotPassword,
                                           params: params,
                                           method: .post,
                                           type: "forgotPasswordApiRequest")
        
        
        
    }
    
    // MARK: changePasswordApiRequest
    
    func changePasswordApiRequest() {
        
        
        if self.newPassword == "" {
            
            return self.alertItem = AlertContext.errorMessage(message: "New Password is required")

        }
        
        if self.confirmPassword == "" {
            
            return self.alertItem = AlertContext.errorMessage(message: "Confirm Password is required")

        }
        
        
        if self.newPassword != self.confirmPassword {
            
            return self.alertItem = AlertContext.errorMessage(message: "Password not matches")

        }
        
        loading = true
        let params: [String: Any] = [
            "email" : self.email,
            "new_password" : self.newPassword,
            
            
        ]
        
        
        
        serverRequest.dataDelegate = self
        serverRequest.requestAndReturnData(url: API.changePassword,
                                           params: params,
                                           method: .post,
                                           type: "changePasswordApiRequest")
        
        
        
    }
    
    // MARK: signout
    
    func signout() {
        
        
        print("signout")

        loading = true
        
        let params: [String: Any] = ["fcm_token": "No",
                                     "user_id" : user?.id ?? 0,
                                    
        ]
        
        
        print(params)
        
        serverRequest.dataDelegate = self
        
        serverRequest.requestAndReturnData(url: API.editProfile,
                                           params: params,
                                           method: .post,
                                           type: "signout"
        )
        
       
        
        
    }
    
    
    // MARK: onFailure
    func onFailure(message: String) {
        
        loading = false
        
        print("OnFailure")
        
        self.alertItem = AlertContext.errorMessage(title: "Failed", message: message)
        print(message)
        
        
    }
    
    
    
    // MARK: onSuccess
    
    func onSuccess(data: Data, val: String) {
        loading = false
        
        switch val {
            
        case "signout":
            
            do {
                
                self.authApiResponse = try JSONDecoder().decode(AuthResponse.self, from: data)
                
//                self.saveUserData(userdata: (self.authApiResponse?.data)!)
                
                
                self.removeUserData()
                
                DispatchQueue.main.async {
                    
                    self.signOutToggle.toggle()
                    
                }
                
            
                
            } catch let error{
                print(error)
            }
            break
            
        case "verifyEmailApiRequest":
            
            do {
                
                let apiResponse = try JSONDecoder().decode(ForgotPasswordAPIResponse.self, from: data)
               
                
                self.code = ""
                
                print(apiResponse)
                
                self.verifyEmailCode = apiResponse.data.code
                
                DispatchQueue.main.async {

                    self.verifyEmailApiRequestToggle.toggle()

//                    self.alertItem = AlertContext.successMessage(title: "Success", message: self.forgotPasswordApiResponse?.message ?? "")
                    
                    
                }
                
            } catch let error{
                print(error)
            }
            break
            
        case "loginApiRequest":
            do {
                self.authApiResponse = try JSONDecoder().decode(AuthResponse.self, from: data)
                
                self.saveUserData(userdata: (self.authApiResponse?.data)!)
                
                Global.shared.currentLoginUser = (self.authApiResponse?.data)!

                
                print("Logged in Data: \(String(describing: self.authApiResponse?.data))")
                

                DispatchQueue.main.async {
                    
                    if self.authApiResponse?.data.role == UserType.business {
                        
                        self.saveUserType(userType: UserType.business.rawValue)
                        
                        self.businessDashboardViewToggle.toggle()

                        
                    } else if self.authApiResponse?.data.role == UserType.customer {
                        
                        self.saveUserType(userType: UserType.customer.rawValue)
                        
                        self.customerDashboardViewToggle = true

                    }

                }
                
                
                
                
            } catch let error {
                print(error)
                self.alertItem = AlertContext.errorMessage(title: "Failed", message: error.localizedDescription)
                
            }
            
            break
            
        case "signUpApiRequest":
            do {
                
                self.authApiResponse = try JSONDecoder().decode(AuthResponse.self, from: data)
                
                self.saveUserData(userdata: (self.authApiResponse?.data)!)
                
                Global.shared.currentLoginUser = (self.authApiResponse?.data)!

                
                DispatchQueue.main.async {
                    
                    if self.authApiResponse?.data.role == UserType.business {
                        
                        self.saveUserType(userType: UserType.business.rawValue)
                        
                        self.businessDashboardViewToggle.toggle()

                        
                    } else if self.authApiResponse?.data.role == UserType.customer {
                        
                        self.saveUserType(userType: UserType.customer.rawValue)
                        
                        self.customerDashboardViewToggle = true

                    }

                }
                
                
                
                
            } catch let error {
                print(error)
                self.alertItem = AlertContext.errorMessage(title: "Failed", message: error.localizedDescription)
                
            }
            
            break
            
        case "updateProfileApiRequest":
            do {
                let authApiResponse = try JSONDecoder().decode(AuthResponse.self, from: data)
                
                self.saveUserData(userdata: authApiResponse.data)
                
                print(authApiResponse.message )
                
                self.alertItem = AlertContext.successMessage(title: "Success", message: "Profile Updated Successfully")

                DispatchQueue.main.async {
//                    self.homeViewToggle.toggle()

                }
                
                
                
                
            } catch let error {
                print(error)
                self.alertItem = AlertContext.errorMessage(title: "Failed", message: error.localizedDescription)
                
            }
            
            break
            
        case "forgotPasswordApiRequest":
            do {
                self.forgotPasswordApiResponse = try JSONDecoder().decode(ForgotPasswordAPIResponse.self, from: data)
               
                
                print(self.forgotPasswordApiResponse!)
                
                DispatchQueue.main.async {

//                    self.isForgotPasswordRequest = true
                    self.otpViewToggle.toggle()

//                    self.alertItem = AlertContext.successMessage(title: "Success", message: self.forgotPasswordApiResponse?.message ?? "")
                    
                    
                }
                
            } catch let error{
                print(error)
            }
            break
            
        case "changePasswordApiRequest":
            
            do {
                self.authApiResponse = try JSONDecoder().decode(AuthResponse.self, from: data)
                
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
                    
                    self.isChangePasswordRequest = true
                    
                    self.alertItem = AlertContext.successMessage(title: "Success", message: self.authApiResponse?.message ?? "")
                    
                    
                }
                
            } catch let error{
                print(error)
            }
            break
            
        case "editProfileApiRequest":
            do {
                self.authApiResponse = try JSONDecoder().decode(AuthResponse.self, from: data)
                
                self.saveUserData(userdata: (self.authApiResponse?.data)!)
                
                Global.shared.currentLoginUser = (self.authApiResponse?.data)!
                
                profileUpdateAlertToggle.toggle()
                
                
                DispatchQueue.main.async {
                    
                    
                    
                    self.name = Global.shared.currentLoginUser.userName ?? ""
                    self.email = Global.shared.currentLoginUser.email ?? ""
                    self.phone = Global.shared.currentLoginUser.phoneNumber ?? ""
                    
                }
            } catch {
                self.alertItem = AlertContext.errorMessage(message: error.localizedDescription)
            }
            break
            
        default:
            self.alertItem = AlertContext.errorMessage(title: "Failed", message: "Something went wrong")
            
        }
        
        
    }
    
}

