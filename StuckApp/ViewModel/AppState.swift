//
//  AppState.swift
//  StuckApp
//
//  Created by Admin Admin on 2/23/22.
//

import Foundation
import SwiftUI


class AppState: ObservableObject {
    static let shared = AppState()
    @Published var pageToNavigationTo : String?
}
