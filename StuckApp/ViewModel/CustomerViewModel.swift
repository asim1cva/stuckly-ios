//
//  CustomerViewModel.swift
//  StuckApp
//
//  Created by Asim on 2/22/22.
//


import SwiftUI
import Foundation
import Combine
import UIKit
import Alamofire



final class CustomerViewModel: UIViewController, ObservableObject, serverResponseData {
    
    
    // MARK: Properties
    
    @Published var alertItem : AlertItem?
    @Published var loading = false
    


    
    
    @Published var businessData: BusinessListData?
    
    
    // MARK: Create Business
    
    @Published var businessName : String = ""
    @Published var businessDesc : String = ""
    @Published var businessWebsiteLink : String = ""


    
    
//    // MARK: getNotificationListApiRequest
//
//    @Published var notificationsListResponse : GetNotificationListAPIResponse? = nil
//
//    @Published var notificationsList : [NotificationData] = []
//
//    @Published var newNotificationsList : [Int] = []
//
//    @Published var getNotificationListApiRequestToggle : Bool = false
//
//
//    func getNotificationListApiRequest() {
//
//
//
////        loading = getNotificationListApiRequestToggle ? false : true
//
//
//
//        newNotificationsList = []
//
//
//
//        let params: [String: Any] = ["user_id": Global.shared.currentLoginUser.id ?? 0,
//
//
//        ]
//
//        serverRequest.dataDelegate = self
//
//        serverRequest.requestAndReturnData(url: API.getNotification,
//                                           params: params,
//                                           method: .post,
//                                           type: "getNotificationListApiRequest"
//        )
//    }
    
   
    
    
   


    
    
    // MARK: followBusiness
    
    @Published var followBusinessApiRequestToggle : Bool = false
    
    
    
    func followBusinessApiRequest() {
        
        
        loading =  true
        
        
        let params: [String: Any] = ["user_id": Global.shared.currentLoginUser.id ?? 0 ,
                                     "business_id" : self.businessData?.id,
                                     
                                     
        ]
        
        serverRequest.dataDelegate = self
        
        serverRequest.requestAndReturnData(url: API.followBusiness,
                                           params: params,
                                           method: .post,
                                           type: "followBusinessApiRequest"
        )
    }
    
    
    
    // MARK: getFollowedBusinessListApiRequest
    
    @Published var followedBusinessList : [BusinessListData] = []
    @Published var getFollowedBusinessListToggle : Bool = false
    
    
    func getFollowedBusinessListApiRequest() {
        
        
        
//        loading = getFollowedBusinessListToggle ? false : true
        
        
        
        let params: [String: Any] = ["user_id": Global.shared.currentLoginUser.id ?? 0,
                                     
                                     
        ]
        
        serverRequest.dataDelegate = self
        
        serverRequest.requestAndReturnData(url: API.getFollowedBusiness,
                                           params: params,
                                           method: .post,
                                           type: "getFollowedBusinessListApiRequest"
        )
    }
    
    // MARK: onSuccess
    
    func onSuccess(data: Data, val: String) {
        
        loading = false
        
        switch val {
            
            
        case "getFollowedBusinessListApiRequest":
            
            do {
                
                
                let apiResponse = try JSONDecoder().decode(GetFollowedBusinessApiResponse.self, from: data)
                
                print("getFollowedBusinessListApiRequest: \(apiResponse.message)")
                
                
              
                if !apiResponse.data.isEmpty {
                    
                    self.followedBusinessList = apiResponse.data
                    
                }
                
                DispatchQueue.main.async {
                    
                  
                    self.getFollowedBusinessListToggle = true
                    
                }
                
                
            } catch let error {
                
                print(error)
                self.alertItem = AlertContext.errorMessage(title: "Failed", message: error.localizedDescription)
                
            }
            
            break
      
            
    
            
      
            
       
            
        case "followBusinessApiRequest":
            
            do {
                
                
                let apiResponse = try JSONDecoder().decode(SimpleResponse.self, from: data)
                
                
                print("followBusinessApiRequest: \(apiResponse.message)")
                
                
                //                self.alertItem = AlertContext.successMessage(title: "Success", message: apiResponse.message)
                
                
                DispatchQueue.main.async {
                    
                    self.followBusinessApiRequestToggle = true
                    
                }
                
                
                
            } catch let error {
                
                print(error)
                self.alertItem = AlertContext.errorMessage(title: "Failed", message: error.localizedDescription)
                
            }
            
            break
            
            
        default:
            
            self.alertItem = AlertContext.errorMessage(title: "Failed", message: "Something went wrong")
            
        }
        
        
    }
    
    
    // MARK: onFailure
    
    func onFailure(message: String) {
        
        loading = false
        
        print("OnFailure")
        
        self.alertItem = AlertContext.errorMessage(title: "Failed", message: message)
        print(message)
        
        
    }
    
    
    
    @Published var signOutToggle = false
    
    func signout() {
        
        
        self.removeUserData()
        
        DispatchQueue.main.async {
            self.signOutToggle.toggle()
        }
        
        
    }
    
}
