//
//  SubscriptionView.swift
//  StuckApp
//
//  Created by Muhammad Salman on 2/14/22.
//

import SwiftUI
import Alamofire
import Stripe

class SubscriptionVM: UIViewController, ObservableObject , serverResponseData{
  
    
    
    
    @Published var firstMonthPlanAvailabe = true
    
    
    // Payment
    @Published var createPaymentIntentResponse: CreatePaymentIntentResponse? = nil
    @Published var alertItem : AlertItem?
    @Published var loading = false
    
    @Published var paymentStatus: STPPaymentHandlerActionStatus?
    @Published var paymentIntentParams: STPPaymentIntentParams?
    @Published var lastPaymentError: NSError?
    
    
    @Published var businessDashboardViewToggle = false
    @Published var customerDashboardViewToggle = false
    
    var currentLoggedInUser = UserDefaults.standard.value(DataClass.self, forKey: UserDefaultKey.savedUser)

    // starting and ending dates for subscription plan....
//    @Published var startDate = ""
//    @Published var endDate = ""
//    @Published var planStatus = ""
    // userDefaultsKeys....
    let startingDateKey = "startingDate"
    
    
    
    // Get payment intent....
    func getPaymentIntent(name: String, email: String, account_owner: String, user_id: Int ,amount: String) {
        
        let params: [String: Any] = ["name":            name,
                                     "email":           email,
                                     "account_owner":   account_owner,
                                     "user_id":         user_id,
                                     "amount":          amount]
        
        
        
        
        serverRequest.dataDelegate = self
        
        serverRequest.requestAndReturnData(url: API.createPaymentIntent,
                                           params: params,
                                           method: .post,
                                           type: "getPaymentIntent"
        )
        
//
//        AF.request(API.baseURL + API.createPaymentIntent ,
//                   method: .post,
//                   parameters: params).responseJSON { response in
//            switch response.result {
//            case .success:
//
//                do {
//                    let resp = try JSONDecoder().decode(CreatePaymentIntentResponse.self, from: response.data!)
//
//                    let clientSecret = resp.data.secret
//                    self.paymentIntentParams = STPPaymentIntentParams(clientSecret: clientSecret)
//
//                }
//                catch let error {
//                    print(error.localizedDescription)
//                }
//
//
//                break
//
//            case .failure(let error):
//                print("\nError...\(error.localizedDescription)\n\n")
//
//                break
//
//            }
//        }
    }
    
    
    
    
    
    func onCompletion(status: STPPaymentHandlerActionStatus, pi: STPPaymentIntent?, error: NSError?) {
        
        self.paymentStatus = status
        self.lastPaymentError = error
        self.loading = false

        if status == .succeeded {
            self.paymentIntentParams = nil
            print("success.............\n\n")
            
            monthlySubscribedApiRequest()
            
            // A PaymentIntent can't be reused after a successful payment. Prepare a new one for the demo.
//            self.paymentIntentParams = nil
//            preparePaymentIntent(paymentMethodType: self.paymentMethodType!, currency: self.currency!)
        }
    }
    
    
    
    // MARK: Get payment intent....
    
    func monthlySubscribedApiRequest() {
        
        let params: [String: Any] = [
            
            "user_id":         Global.shared.currentLoginUser.id ?? 0,
        ]
        
        
        
        
        serverRequest.dataDelegate = self
        
        serverRequest.requestAndReturnData(url: API.monthlySubscribed,
                                           params: params,
                                           method: .post,
                                           type: "monthlySubscribedApiRequest"
        )
        

    }
    
    
    
    // Date validator....
    func validSubscription() {
        
        print("validSubscription - \(String(describing: currentLoggedInUser))")
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"

        
        let current = Date()
        formatter.dateFormat = "yyyy-MM-dd"
        
        
        
        let currentString = formatter.string(from: current)
        let currentDate = formatter.date(from: currentString)
        
        let createdDate = currentLoggedInUser?.createdAt?.toDate(withFormat: "yyyy-MM-dd")
        
//            let createdDate = "2022-01-19".toDate(withFormat: "yyyy-MM-dd")

        
        print("Cuurent Date: \(String(describing: createdDate)) Create Date: \(String(describing: createdDate))")
        
        print("Days Between: \(HelperFunctions().daysBetween(start: createdDate ?? Date(), end: currentDate ?? Date()))")

        if !(currentLoggedInUser?.isMonthlySubscribed)! {
            
            
            if HelperFunctions().daysBetween(start: createdDate ?? Date(), end: currentDate ?? Date()) <= 30 {
                
                self.firstMonthPlanAvailabe = true
                
            } else {
                
                self.firstMonthPlanAvailabe = false


            }
            

        }
        

        
        
    }
    
    
    
    
    // Subsribe new plan....
    func subscribeNow() {
        UserDefaults.standard.set(nil, forKey: startingDateKey)
        self.validSubscription() //MARK: IMPORTANT
    }
    
    
    
    
    func onSuccess(data: Data, val: String) {
        
        loading = false
        switch val {
            
        case "getPaymentIntent":
            
            do {
                
                let resp = try JSONDecoder().decode(CreatePaymentIntentResponse.self, from: data)
                
                let clientSecret = resp.data.secret
                self.paymentIntentParams = STPPaymentIntentParams(clientSecret: clientSecret)
                
                
                
            } catch let error {
                print(error)
                self.alertItem = AlertContext.errorMessage(title: "Failed", message: error.localizedDescription)
                
            }
            
            break
        case "monthlySubscribedApiRequest":
            
            do {
                let authApiResponse = try JSONDecoder().decode(AuthResponse.self, from: data)
                
                self.saveUserData(userdata: (authApiResponse.data))
                
                Global.shared.currentLoginUser = (authApiResponse.data)

                
                print("Logged in Data: \(String(describing: authApiResponse.data))")
                

                DispatchQueue.main.async {
                    
                    if authApiResponse.data.role == UserType.business {
                        
                        self.saveUserType(userType: UserType.business.rawValue)
                        
                        self.businessDashboardViewToggle.toggle()

                        
                    } else if authApiResponse.data.role == UserType.customer {
                        
                        self.saveUserType(userType: UserType.customer.rawValue)
                        
                        self.customerDashboardViewToggle = true

                    }

                }
                
                
                
                
            } catch let error {
                print(error)
                self.alertItem = AlertContext.errorMessage(title: "Failed", message: error.localizedDescription)
                
            }
            
            break
     
            
  
            
      
            
        default:
            self.alertItem = AlertContext.errorMessage(title: "Failed", message: "Something went wrong")
            
        }
    }
    
    func onFailure(message: String) {
        
        loading = false
        
        print("OnFailure")
        
        self.alertItem = AlertContext.errorMessage(title: "Failed", message: message)
        print(message)
    }
    
}
