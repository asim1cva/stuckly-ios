//
//  BusinessTabsViewModel.swift
//  StuckApp
//
//  Created by Asim on 1/12/22.
//


import SwiftUI
import Foundation
import Combine
import UIKit
import Alamofire



final class BusinessTabsViewModel: UIViewController, ObservableObject, serverResponseData {
    
    
    // MARK: Properties
    
    @Published var alertItem : AlertItem?
    @Published var loading = false
    
    
    
    @Published var businessData: BusinessListData?
    
    
    // MARK: Create Business
    
    @Published var businessName : String = ""
    @Published var businessDesc : String = ""
    @Published var businessWebsiteLink : String = ""
    
    @Published var imagesArray = Array<String>()
    
    @Published var uploadImagesList : [UIImage] = []
    var localTimeZoneIdentifier: String { return TimeZone.current.identifier }

    
    @Published var isImageConverted = false
    
    func createBusinessAPiRequest() {
        
        
        // Send Data in Raw JSON formate
        
        if self.businessName == "" {
            return self.alertItem = AlertContext.errorMessage(message: "Business name is required!")
            
        }
        
        if self.businessDesc == "" {
            return self.alertItem = AlertContext.errorMessage(message: "Business description is required!")
            
        }
        
        if self.uploadImagesList.count < 1 {
            return self.alertItem = AlertContext.errorMessage(message: "Business images is required!")
            
        }
        
        loading = true
        
        
        imagesArray.removeAll()
        
        
        // set up once performed background task
        DispatchQueue.global(qos: .background).async {
            
            
            for uiImage in self.uploadImagesList {
                
                self.imagesArray.append(HelperFunctions.imageTobase64(withCompression: 1.0, image: uiImage))
                
            }
            
            DispatchQueue.main.async { [weak self] in
                
                let params: [String: Any] = [
                    
                    "user_id": "\(Global.shared.currentLoginUser.id ?? 0)",
                    "name" : self?.businessName ?? "",
                    "description" : self?.businessDesc ?? "",
                    "website_link" : self?.businessWebsiteLink ?? "",
                    "image": self?.imagesArray ?? [],
                    
                ]
                
                

                print(params)
                
                serverRequest.dataDelegate = self
                
                serverRequest.requestAndReturnData(url: API.createBusiness,
                                                   params: params,
                                                   method: .post,
                                                   type: "createBusinessAPiRequest"
                )
            }
            
            
        }
        
       
        
      
      

        
        
      
    }
    
    
    // MARK: Update Business
    
    @Published var updateBusinessApiRequestToggle : Bool = false

    
    func updateBusinessApiRequest(businessId: Int) {
        
        
        // Send Data in Raw JSON formate
        
        if self.businessName == "" {
            return self.alertItem = AlertContext.errorMessage(message: "Business name is required!")
            
        }
        
        if self.businessDesc == "" {
            return self.alertItem = AlertContext.errorMessage(message: "Business description is required!")
            
        }
        
        if self.uploadImagesList.count < 1 {
            return self.alertItem = AlertContext.errorMessage(message: "Business images is required!")
            
        }
        
        loading = true
        
        
        imagesArray.removeAll()
        
        
        // set up once performed background task
        DispatchQueue.global(qos: .background).async {
            
            
            for uiImage in self.uploadImagesList {
                
                self.imagesArray.append(HelperFunctions.imageTobase64(withCompression: 1.0, image: uiImage))
                
            }
            
            DispatchQueue.main.async { [weak self] in
                
                let params: [String: Any] = [
                    "business_id": "\(businessId)",
                    "name" : self?.businessName ?? "",
                    "description" : self?.businessDesc ?? "",
                    "website_link" : self?.businessWebsiteLink ?? "",
                    "image": self?.imagesArray ?? [],
                ]
                
                
                
                print(params)
                
                serverRequest.dataDelegate = self
                
                serverRequest.requestAndReturnData(url: API.updateBusiness,
                                                   params: params,
                                                   method: .post,
                                                   type: "updateBusinessApiRequest"
                )
            }
            
            
        }
    }
    
    
    // MARK: getUsersBusinessListApiRequest
    
    @Published var usersBusinessList : [BusinessListData] = []
    @Published var usersBusinessListToggle : Bool = false
    
    var filterUserBusinessList: [BusinessListData] {
        
        if searchingFor.isEmpty {
            return usersBusinessList
        } else {
            return (usersBusinessList.filter {
                $0.name.localizedCaseInsensitiveContains(searchingFor)
            })
        }
    }
    
    
    func getUsersBusinessListApiRequest() {
        
        
        
        loading = usersBusinessListToggle ? false : true
        
        
        
        let params: [String: Any] = ["user_id": Global.shared.currentLoginUser.id ?? 0,
                                     
                                     
        ]
        
        serverRequest.dataDelegate = self
        
        serverRequest.requestAndReturnData(url: API.userBusinessList,
                                           params: params,
                                           method: .post,
                                           type: "getUsersBusinessListApiRequest"
        )
    }
    
    
    var user = UserDefaults.standard.value(DataClass.self, forKey: UserDefaultKey.savedUser)

    @Published var searchingFor = ""

    @Published var searchingisActive = false
    
    func getBusinessListApiRequest() {
        
        
        
        loading = usersBusinessListToggle ? false : true
        
        
        
        let params: [String: Any] = ["user_id": Global.shared.currentLoginUser.id ?? 0,
                                     
                                     
        ]
        
        serverRequest.dataDelegate = self
        
        serverRequest.requestAndReturnData(url: API.getBusinessList,
                                           params: params,
                                           method: .post,
                                           type: "getBusinessListApiRequest"
        )
    }
    
    
    // MARK: setUpReleaseDateApiRequest
    
    @Published var message : String = ""
    @Published var detail : String = ""
    
    @Published var releaseDate = Date()
    
//    @Published var deliveryDate = Date()

    @Published var releaseImagesArray = Array<String>()

    func setUpReleaseDateApiRequest() {
        
//        yyyy-MM-dd
        
//        if self.uploadImagesList.count < 1 {
//            return self.alertItem = AlertContext.errorMessage(message: "Release Date image is required!")
//
//        }
        
        loading =  true
        
        
        releaseImagesArray.removeAll()

        // set up once performed background task
        DispatchQueue.global(qos: .background).async {
            
            for uiImage in self.uploadImagesList {
                
                self.releaseImagesArray.append(HelperFunctions.imageTobase64(image: uiImage))
                
            }
            
            DispatchQueue.main.async { [weak self] in
                
                let params: [String: Any] = ["business_id": self?.businessData?.id ?? 0 ,
                                             "message" : self?.message ?? "",
                                             "detail" : self?.detail ?? "",
                                             "date" : HelperFunctions.dateConverter(date: self?.releaseDate ?? Date(), dateFormat: "yyyy-MM-dd"),
                                             "release_image" : self?.releaseImagesArray ?? [],

                                             
                ]
                
                serverRequest.dataDelegate = self
                
                serverRequest.requestAndReturnData(url: API.setReleaseDate,
                                                   params: params,
                                                   method: .post,
                                                   type: "setUpReleaseDateApiRequest"
                )
                
            }

           
        }
        
      
        
       
    }
    
    
    
    // MARK: setScheduleNotification
    
    @Published var scheduleMessage : String = ""
    
    @Published var scheduleDate = Date()
    

    @Published var scheduleTime = Date()

    
    @Published var setScheduleNotificationApiRequestToggle: Bool = false

    func setScheduleNotificationApiRequest() {
        
        

//        localTimeZoneIdentifier // "America/Sao_Paulo"

        
        print("timezone: \(localTimeZoneIdentifier)")
        
        if self.scheduleMessage == "" {
            return self.alertItem = AlertContext.errorMessage(message: "Message is required!")
            
        }
        
     
        
        
        loading =  true
        
        
        let params: [String: Any] = ["business_id": self.businessData?.id ?? 0 ,
                                     "message" : self.scheduleMessage,
                                     "schedule_time" : HelperFunctions.dateConverter(date: self.scheduleTime, dateFormat: "HH:mm"),
                                     "timezone": localTimeZoneIdentifier,

                                     "schedule_date" : HelperFunctions.dateConverter(date: self.scheduleDate, dateFormat: "yyyy-MM-dd")
                                     
        ]
        
        serverRequest.dataDelegate = self
        
        serverRequest.requestAndReturnData(url: API.setScheduleNotification,
                                           params: params,
                                           method: .post,
                                           type: "setScheduleNotificationApiRequest"
        )
    }
    
    
    
    
    // MARK: followBusiness
    
    @Published var followBusinessApiRequestToggle : Bool = false
    
    
    
    func followBusinessApiRequest() {
        
        
        loading =  true
        
        
        let params: [String: Any] = ["user_id": Global.shared.currentLoginUser.id ?? 0 ,
                                     "business_id" : self.businessData?.id,
                                     
                                     
        ]
        
        serverRequest.dataDelegate = self
        
        serverRequest.requestAndReturnData(url: API.followBusiness,
                                           params: params,
                                           method: .post,
                                           type: "followBusinessApiRequest"
        )
    }
    
    
    // MARK: sendInstantNotificationApiRequest
    
    
    @Published var instantNotificationMessage : String = ""
    
    @Published var isInstantToggle: Bool = false


    
    func sendInstantNotificationApiRequest() {
        
        
        
        
        let params: [String: Any] = ["message": instantNotificationMessage ,
                                     "business_id" : self.businessData?.id ?? 0,
                                     "timezone": localTimeZoneIdentifier,

                                     
        ]
        
        if instantNotificationMessage == "" {
            
            self.alertItem = AlertContext.errorMessage(message: "Message is required")
            
            return

        }
        
        loading =  true

        serverRequest.dataDelegate = self
        
        serverRequest.requestAndReturnData(url: API.sendInstantNotification,
                                           params: params,
                                           method: .post,
                                           type: "sendInstantNotificationApiRequest"
        )
    }
    
    
    // MARK: getNotificationListApiRequest
    
    @Published var isOpenApiRequestToggle : Bool = false
    
    
    func isOpenApiRequest(notificationId: Int) {
        
        
        let params: [String: Any] = ["notification_id": notificationId,
                                     
                                     
        ]
        
        serverRequest.dataDelegate = self
        
        serverRequest.requestAndReturnData(url: API.isOpen,
                                           params: params,
                                           method: .post,
                                           type: "isOpenApiRequest"
        )
    }
    
    
    // MARK: getNotificationListApiRequest
    
    @Published var notificationsListResponse : GetNotificationListAPIResponse? = nil

    @Published var notificationsList : [NotificationData] = []
    @Published var pastNotificationsList : [NotificationData] = []
    @Published var futureNotificationsList : [NotificationData] = []
    @Published var todayNotificationsList : [NotificationData] = []

    @Published var newNotificationsList : [Int] = []

    @Published var getNotificationListApiRequestToggle : Bool = false
    
    
    func getNotificationListApiRequest() {
        
        
        
//        loading = getNotificationListApiRequestToggle ? false : true
        
        

        newNotificationsList = []

        
        
        let params: [String: Any] = ["user_id": 12,
                                     
                                     
        ]
        
        serverRequest.dataDelegate = self
        
        if Global.shared.currentLoginUser.role == UserType.customer {
            
            serverRequest.requestAndReturnData(url: API.getNotification,
                                               params: params,
                                               method: .post,
                                               type: "getNotificationListApiRequest"
            )
            
        } else if Global.shared.currentLoginUser.role == UserType.business {
           
            
            serverRequest.requestAndReturnData(url: API.scheduledNotification,
                                               params: params,
                                               method: .post,
                                               type: "getNotificationListApiRequest"
            )
        }
        
       
    }
    
    
    // MARK: alreadyLoginApiRequest
    
    @Published var alreadyLoginApiRequestToggle : Bool = false
    @Published var alreadyLoginApiResponse : AlreadyLoginResponse? = nil

    
    func alreadyLoginApiRequest() {

        
//        loading = true
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
//        DispatchQueue.main.async { [weak self] in
            
            let params: [String: Any] = [
                "user_id": Global.shared.currentLoginUser.id ?? 0,
                "islogin" : Global.shared.currentLoginUser.islogin ?? 0,
                
            ]
            
            
            
            print(params)
            
            serverRequest.dataDelegate = self
            
            serverRequest.requestAndReturnData(url: API.alreadyLogin,
                                               params: params,
                                               method: .post,
                                               type: "alreadyLoginApiRequest"
            )
        }
    }
    
    
    
    
    

    // MARK: onSuccess
    
    func onSuccess(data: Data, val: String) {
        
        loading = false
        
        switch val {
            
        case "alreadyLoginApiRequest":
            
            do {
                
                
                alreadyLoginApiResponse = try JSONDecoder().decode(AlreadyLoginResponse.self, from: data)
                
                print("alreadyLoginApiResponse: ")

                
                if !(alreadyLoginApiResponse?.islogin ?? false) {
                    
//                    self.alertItem = AlertContext.errorMessage(title: "Failed", message: "You Logged in second device")
                    self.signout()

                }
                
                
                DispatchQueue.main.async {
                    self.alreadyLoginApiRequestToggle = true
                }
                
                
                
                
            } catch let error {
                
                print(error)
                self.alertItem = AlertContext.errorMessage(title: "Failed", message: error.localizedDescription)
                
            }
            
            break
            
        case "getNotificationListApiRequest":
            
            do {
                
                
                if Global.shared.currentLoginUser.role == UserType.customer {
                    
                   
                    print("getNotificationListApiRequest - customer ")
                    
                    notificationsListResponse = try JSONDecoder().decode(GetNotificationListAPIResponse.self, from: data)
                    
                    print("getNotificationListApiRequest: \(notificationsListResponse?.data)")
                    
                    
                    if !((notificationsListResponse?.data.isEmpty)!) {
                        
                        self.notificationsList = notificationsListResponse!.data
                    
                        
                    }
                    
                } else if Global.shared.currentLoginUser.role == UserType.business {
                   
                    print("getNotificationListApiRequest - business ")
                    
                    
                    var response = try JSONDecoder().decode(GetScheduledNotificationListAPIResponse.self, from: data)
                    
                    print("getNotificationListApiRequest: \(response.data)")
                    
                    
                    if !((response.data.past.isEmpty)) {
                        
                        self.pastNotificationsList = response.data.past
                    }
                    
                    if !((response.data.future.isEmpty)) {
                        
                        self.futureNotificationsList = response.data.future
                    }
                    
                    if !((response.data.today.isEmpty)) {
                        
                        self.todayNotificationsList = response.data.today
                    }

                    
                }
                
              
                
                
                DispatchQueue.main.async {
                    self.getNotificationListApiRequestToggle = true
                    
                }
                
                
                
                
            } catch let error {
                
                print(error)
                self.alertItem = AlertContext.errorMessage(title: "Failed", message: error.localizedDescription)
                
            }
            
            break
            
        case "isOpenApiRequest":
            
            do {
                
                
                let apiResponse = try JSONDecoder().decode(SimpleResponse.self, from: data)
                
                print("isOpenApiRequest: \(apiResponse.message)")
                
                
                
                DispatchQueue.main.async {
                    
                    self.isOpenApiRequestToggle = true
                    
                }

                
            } catch let error {
                
                print(error)
                self.alertItem = AlertContext.errorMessage(title: "Failed", message: error.localizedDescription)
                
            }
            
            break
            
            
            
            
        case "setScheduleNotificationApiRequest":
            
            do {
                
                
                let apiResponse = try JSONDecoder().decode(SimpleResponse.self, from: data)
                
                print("setScheduleNotificationApiRequest: \(apiResponse.message)")
                
                
                DispatchQueue.main.async {
                    
                    self.alertItem = AlertContext.successMessage(title: "Success", message: apiResponse.message)
                    
                    self.scheduleMessage = ""
                    self.scheduleTime = Date()
                    self.scheduleDate = Date()
                    self.setScheduleNotificationApiRequestToggle = true
                    
                    
                }
                
                
                
                
            } catch let error {
                
                print(error)
                self.alertItem = AlertContext.errorMessage(title: "Failed", message: error.localizedDescription)
                
            }
            
            break
            
        case "sendInstantNotificationApiRequest":
            
            do {
                
                
                let apiResponse = try JSONDecoder().decode(SimpleResponse.self, from: data)
                
                print("sendInstantNotificationApiRequest: \(apiResponse.message)")
                
                
                DispatchQueue.main.async {
                    
                    self.alertItem = AlertContext.successMessage(title: "Success", message: apiResponse.message)

                    self.instantNotificationMessage = ""
                    self.isInstantToggle = false
                    
                    
                }
                
                
                
                
            } catch let error {
                
                print(error)
                self.alertItem = AlertContext.errorMessage(title: "Failed", message: error.localizedDescription)
                
            }
            
            break
            
            
        case "createBusinessAPiRequest":
            
            do {
                
                
                let apiResponse = try JSONDecoder().decode(SimpleResponse.self, from: data)
                
                print("createBusinessAPiRequest: \(apiResponse.message)")
                
                self.businessName = ""
                self.businessDesc = ""
                self.businessWebsiteLink = ""
                self.uploadImagesList.removeAll()
                
                self.alertItem = AlertContext.successMessage(title: "Success", message: apiResponse.message)
                
                
                
                
            } catch let error {
                
                print(error)
                self.alertItem = AlertContext.errorMessage(title: "Failed", message: error.localizedDescription)
                
            }
            
            break
            
        case "getUsersBusinessListApiRequest":
            
            do {
                
                
                let apiResponse = try JSONDecoder().decode(UserBusinessListApiResponse.self, from: data)
                
                print("getUsersBusinessListApiRequest: \(apiResponse.message)")
                
                
                if !apiResponse.data.isEmpty {
                    self.usersBusinessList = apiResponse.data
                    
                }
                
                
                DispatchQueue.main.async {
                    self.usersBusinessListToggle = true
                    self.alreadyLoginApiRequest()

                }
                
                
                
                
            } catch let error {
                
                print(error)
                self.alertItem = AlertContext.errorMessage(title: "Failed", message: error.localizedDescription)
                
            }
            
            break
            
        case "getBusinessListApiRequest":
            
            do {
                
                
                let apiResponse = try JSONDecoder().decode(UserBusinessListApiResponse.self, from: data)
                
                print("getUsersBusinessListApiRequest: \(apiResponse.message)")
                
                
                

                if !apiResponse.data.isEmpty {
                    self.usersBusinessList = apiResponse.data
                    
                }
                
                
                if !self.getNotificationListApiRequestToggle {
                    self.getNotificationListApiRequest()

                }

                
                DispatchQueue.main.async {
                    self.usersBusinessListToggle = true
                    
                    self.alreadyLoginApiRequest()
//                    self.getNotificationListApiRequest()

                }
                
                
                
                
            } catch let error {
                
                print(error)
                self.alertItem = AlertContext.errorMessage(title: "Failed", message: error.localizedDescription)
                
            }
            
            break
            
        case "setUpReleaseDateApiRequest":
            
            do {
                
                
                let apiResponse = try JSONDecoder().decode(SimpleResponse.self, from: data)
                
                
                print("createBusinessAPiRequest: \(apiResponse.message)")
                
                
                
                
                self.alertItem = AlertContext.successMessage(title: "Success", message: apiResponse.message)
                
                
                DispatchQueue.main.async {
                    
//                    self.releaseDate
                    
                    self.businessData?.releaseDate = HelperFunctions.dateConverter(date: self.releaseDate, dateFormat: "yyyy-MM-dd")
                    self.businessData?.releaseMessage = self.message
                    self.businessData?.releaseDetail = self.detail
                    
                    self.message = ""
                    self.detail = ""
                    self.releaseImagesArray = []
                    self.uploadImagesList = []
                    
                    //
                    //                    if  self.businessData!.isfollowed ?? false {
                    //
                    //                        self.businessData!.isfollowed = false
                    //
                    //                    } else {
                    //
                    //                        self.businessData!.isfollowed = true
                    //                    }
                    
                    //                    self.usersBusinessListToggle = true
                    
                }
                
                
                
            } catch let error {
                
                print(error)
                self.alertItem = AlertContext.errorMessage(title: "Failed", message: error.localizedDescription)
                
            }
            
            break
            
        case "followBusinessApiRequest":
            
            do {
                
                
                let apiResponse = try JSONDecoder().decode(SimpleResponse.self, from: data)
                
                
                print("followBusinessApiRequest: \(apiResponse.message)")
                
                
                //                self.alertItem = AlertContext.successMessage(title: "Success", message: apiResponse.message)
                
                
                DispatchQueue.main.async {
                    
                    self.followBusinessApiRequestToggle = true
                    
                }
                
                
                
            } catch let error {
                
                print(error)
                self.alertItem = AlertContext.errorMessage(title: "Failed", message: error.localizedDescription)
                
            }
            
            break
            
        case "updateBusinessApiRequest":
            
            do {
                
                
                let apiResponse = try JSONDecoder().decode(SimpleResponse.self, from: data)
                
                print("createBusinessAPiRequest: \(apiResponse.message)")
                
                self.businessName = ""
                self.businessDesc = ""
                self.businessWebsiteLink = ""
                self.uploadImagesList.removeAll()
                
                self.alertItem = AlertContext.successMessage(title: "Success", message: apiResponse.message)
                
                DispatchQueue.main.async {
                    
                    self.updateBusinessApiRequestToggle = true
                    
                }
                
            } catch let error {
                
                print(error)
                self.alertItem = AlertContext.errorMessage(title: "Failed", message: error.localizedDescription)
                
            }
            
            break
            
        default:
            
            self.alertItem = AlertContext.errorMessage(title: "Failed", message: "Something went wrong")
            
        }
        
        
    }
    
    
    // MARK: onFailure
    
    func onFailure(message: String) {
        
        loading = false
        
        print("OnFailure")
        
        self.alertItem = AlertContext.errorMessage(title: "Failed", message: message)
        print(message)
        
        
    }
    
    
    
    @Published var signOutToggle = false
    
    func signout() {
        
        
        self.removeUserData()
        
        DispatchQueue.main.async {
            self.signOutToggle.toggle()
        }
        
        
    }
    
}
